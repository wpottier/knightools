from django.http import JsonResponse
from django.shortcuts import render
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import PasswordChangeView
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse_lazy
from django.utils.text import slugify
from django.db.models import Q

from gettext import gettext
import json

from .models import (FavoriteList, FavoriteArmour, FavoriteEffect,
    FavoriteModule, FavoriteWeapon, FavoriteEnhancement,
    FavoriteMechaArmour, CustomUser, Feature)
from gm.models.weaponry import Effect, Module, Weapon, Enhancement
from gm.models.character import Armour, Trauma, MechaArmour

from .forms import CustomUserProfileForm

# Create your views here.
class ListFavoritesView(LoginRequiredMixin, generic.TemplateView):
    template_name = 'users/fav_list.html'
    
    def get_context_data(self, **kwargs):
        context = super(ListFavoritesView, self).get_context_data(**kwargs)
        fav_lists = FavoriteList.objects.filter(user=self.request.user).prefetch_related(
            'favoritearmour_set', 'favoritearmour_set__armour',
            'favoriteeffect_set', 'favoriteeffect_set__effect',
            'favoriteenhancement_set', 'favoriteenhancement_set__enhancement',
            'favoritemodule_set', 'favoritemodule_set__module',
            'favoritemechaarmour_set', 'favoritemechaarmour_set__mecha',
            'favoriteweapon_set', 'favoriteweapon_set__weapon',
        ).order_by("order", "name")
        
        favorites_weapon = (FavoriteWeapon
          .objects
          .filter(
            ~Q(lists__in=fav_lists.values_list('pk', flat=True)),
            user=self.request.user
          )
          .select_related('weapon')
          .order_by('weapon__name')
        )
        
        favorites_module = (FavoriteModule
          .objects
          .filter(
            ~Q(lists__in=fav_lists.values_list('pk', flat=True)),
            user=self.request.user
          )
          .select_related('module')
          .order_by('module__name')
        )
        
        favorites_effect = (FavoriteEffect
          .objects
          .filter(
            ~Q(lists__in=fav_lists.values_list('pk', flat=True)),
            user=self.request.user
          )
          .select_related('effect')
          .order_by('effect__name')
        )
        
        favorites_armour = (FavoriteArmour
          .objects
          .filter(
            ~Q(lists__in=fav_lists.values_list('pk', flat=True)),
            user=self.request.user
          )
          .select_related('armour')
          .order_by('armour__name')
        )
        
        favorites_enhancement = (FavoriteEnhancement
          .objects
          .filter(
            ~Q(lists__in=fav_lists.values_list('pk', flat=True)),
            user=self.request.user
          )
          .select_related('enhancement')
          .order_by('enhancement__name')
        )
        
        favorites_mecha = (FavoriteMechaArmour
          .objects
          .filter(
            ~Q(lists__in=fav_lists.values_list('pk', flat=True)),
            user=self.request.user
          )
          .select_related('mecha')
          .order_by('mecha__name')
        )
        
        no_fav_list = {}
        
        no_fav_list['id'] = 0
        no_fav_list['favoriteweapon_set'] = favorites_weapon.all()
        no_fav_list['favoritemodule_set'] = favorites_module.all()
        no_fav_list['favoriteeffect_set'] = favorites_effect.all()
        no_fav_list['favoritearmour_set'] = favorites_armour.all()
        no_fav_list['favoriteenhancement_set'] = favorites_enhancement.all()
        no_fav_list['favoritemechaarmour_set'] = favorites_mecha.all()
        
        final_fav_list = list(fav_lists)
        final_fav_list.append(no_fav_list)
        context['favorite_lists'] = final_fav_list
        # import pdb; pdb.set_trace()
        return context

class ProfileView(LoginRequiredMixin, generic.FormView):
    template_name = 'users/profile.html'
    form_class = CustomUserProfileForm
    success_url = reverse_lazy('users:profile')
    
    def get_form(self):
        """
        Populate form with user informations
        """
        try:
            return self.form_class(instance=self.request.user, **self.get_form_kwargs())
        except Exception:
            return self.form_class(**self.get_form_kwargs())
    
    def form_valid(self, form):
        if form.instance != self.request.user:
            raise PermissionError(gettext("Vous ne pouvez pas modifier cet utilisateur !"), "tried_to_modify_another_user")
        
        # import pdb; pdb.set_trace()
        user = CustomUser.objects.get(pk=form.instance.pk)
        if not user.game_master and form.cleaned_data['game_master']:
            question = user.get_next_game_master_question()
            if form.cleaned_data['game_master_question']:
                answer = slugify(form.cleaned_data['game_master_question'])
                
                if answer == question.answer:
                    form.instance.game_master = True
                else:
                    form.instance.game_master = False
                    form.instance.asked_game_master_questions.add(question)
            else:
                form.instance.game_master = False
        
        if form.cleaned_data['activation_code'] != '':
            codes = form.cleaned_data['activation_code'].splitlines()
            for code in codes:
                unlocked_armour = Armour.objects.filter(password=code).first()
                if unlocked_armour:
                    form.instance.unlocked_armours.add(unlocked_armour)
                
                unlocked_trauma = Trauma.objects.filter(password=code).first()
                if unlocked_trauma:
                    form.instance.unlocked_traumas.add(unlocked_trauma)
                
                unlocked_mecha = MechaArmour.objects.filter(password=code).first()
                if unlocked_mecha:
                    form.instance.unlocked_mechas.add(unlocked_mecha)
                
                try:
                    feature = Feature.objects.get(key=code)
                    form.instance.unlocked_features.add(feature)
                except ObjectDoesNotExist:
                    pass
        
        form.save()
        
        return super(ProfileView, self).form_valid(form)

class DeleteUserView(LoginRequiredMixin, generic.DeleteView):
    model = CustomUser
    template_name = 'users/delete_account.html'
    success_url = reverse_lazy('gm:index')
    queryset = CustomUser.objects.all()
    
    def get_object(self, queryset=None):
        queryset = super().get_queryset()
        return queryset.get(pk=self.request.user.pk)
    
class PasswordChangeView(LoginRequiredMixin, PasswordChangeView):
    template_name = 'users/change_password.html'
    success_url = reverse_lazy('users:profile')
    

class AddToFavView(LoginRequiredMixin, generic.View):
    
    def post(self, request, *args, **kwargs):
        data = {
            'updated': False,
        }
        try:
            fav_type = request.POST.get('type', None)
            fav_id = int(request.POST.get('id', 0))
            fav_note = request.POST.get('note', '')
            new_lists = request.POST.get('new_lists', '[]')
            bind_to_lists = request.POST.get('bind_to_lists', '[]')
            
            if fav_type is None:
                raise Exception('Unknown type of favorite', 'fav_type_unknown')
            
            if fav_id == 0:
                raise Exception('Unable to retrieve the favorite id', 'fav_id_unknown')
            
            new_lists = json.loads(new_lists)
            bind_to_lists = json.loads(bind_to_lists)
            
            for fl in new_lists:
                fav_list = FavoriteList()
                fav_list.user = request.user
                fav_list.name = fl['name']
                fav_list.define_default_name()
                fav_list.set_last_order()
                fav_list.save()
                
                if fl['bind']:
                    bind_to_lists.append(fav_list.pk)
            
            if fav_type == 'weapon':
                if FavoriteWeapon.objects.filter(user=request.user, weapon=fav_id).count() > 0:
                    fav = FavoriteWeapon.objects.get(user=request.user, weapon=fav_id)
                else:
                    fav = FavoriteWeapon()
                    target = Weapon.objects.get(pk=fav_id)
                    fav.user = request.user
                    fav.weapon = target
            elif fav_type == 'module':
                if FavoriteModule.objects.filter(user=request.user, module=fav_id).count() > 0:
                    fav = FavoriteModule.objects.get(user=request.user, module=fav_id)
                else:
                    fav = FavoriteModule()
                    target = Module.objects.get(pk=fav_id)
                    fav.user = request.user
                    fav.module = target
            elif fav_type == 'effect':
                if FavoriteEffect.objects.filter(user=request.user, effect=fav_id).count() > 0:
                    fav = FavoriteEffect.objects.get(user=request.user, effect=fav_id)
                else:
                    fav = FavoriteEffect()
                    target = Effect.objects.get(pk=fav_id)
                    fav.user = request.user
                    fav.effect = target
            elif fav_type == 'armour':
                if FavoriteArmour.objects.filter(user=request.user, armour=fav_id).count() > 0:
                    fav = FavoriteArmour.objects.get(user=request.user, armour=fav_id)
                else:
                    fav = FavoriteArmour()
                    target = Armour.objects.get(pk=fav_id)
                    fav.user = request.user
                    fav.armour = target
            elif fav_type == 'mecha':
                if FavoriteMechaArmour.objects.filter(user=request.user, mecha=fav_id).count() > 0:
                    fav = FavoriteMechaArmour.objects.get(user=request.user, mecha=fav_id)
                else:
                    fav = FavoriteMechaArmour()
                    target = MechaArmour.objects.get(pk=fav_id)
                    fav.user = request.user
                    fav.mecha = target
            elif fav_type == 'enhancement':
                if FavoriteEnhancement.objects.filter(user=request.user, enhancement=fav_id).count() > 0:
                    fav = FavoriteEnhancement.objects.get(user=request.user, enhancement=fav_id)
                else:
                    fav = FavoriteEnhancement()
                    target = Enhancement.objects.get(pk=fav_id)
                    fav.user = request.user
                    fav.enhancement = target
            else:
                raise Exception('Invalid type of favorite', 'fav_type_invalid')
            
            fav.note = fav_note
            fav.save()
            
            if len(bind_to_lists) > 0:
                fav.lists.set(bind_to_lists)
            
            fav.save()
            
            data = {
                'updated': True,
            }
        except Exception as e:
            data = {
                'error': str(e)
            }
        finally:
            return JsonResponse(data)
    
class EditItemToFavView(LoginRequiredMixin, generic.View):
    
    def post(self, request, *args, **kwargs):
        data = {
            'updated': False,
        }
        try:
            fav_type = request.POST.get('type', None)
            fav_id = int(request.POST.get('id', 0))
            fav_note = request.POST.get('note', '')
            new_lists = request.POST.get('new_lists', '[]')
            bind_to_lists = request.POST.get('bind_to_lists', '[]')
            
            if fav_type is None:
                raise Exception('Unknown type of favorite', 'fav_type_unknown')
            
            if fav_id == 0:
                raise Exception('Unable to retrieve the favorite id', 'fav_id_unknown')
            
            new_lists = json.loads(new_lists)
            bind_to_lists = json.loads(bind_to_lists)
            
            for fl in new_lists:
                fav_list = FavoriteList()
                fav_list.name = fl['name']
                fav_list.define_default_name()
                fav_list.save()
                
                if fl['bind']:
                    bind_to_lists.append(fav_list.pk)
            
            if fav_type == 'weapon':
                if FavoriteWeapon.objects.filter(user=request.user, weapon=fav_id).count() > 0:
                    fav = FavoriteWeapon.objects.get(user=request.user, weapon=fav_id)
                else:
                    fav = FavoriteWeapon()
                    target = Weapon.objects.get(pk=fav_id)
                    fav.user = request.user
                    fav.weapon = target
            elif fav_type == 'module':
                if FavoriteModule.objects.filter(user=request.user, module=fav_id).count() > 0:
                    fav = FavoriteModule.objects.get(user=request.user, module=fav_id)
                else:
                    fav = FavoriteModule()
                    target = Module.objects.get(pk=fav_id)
                    fav.user = request.user
                    fav.module = target
            elif fav_type == 'effect':
                if FavoriteEffect.objects.filter(user=request.user, effect=fav_id).count() > 0:
                    fav = FavoriteEffect.objects.get(user=request.user, effect=fav_id)
                else:
                    fav = FavoriteEffect()
                    target = Effect.objects.get(pk=fav_id)
                    fav.user = request.user
                    fav.effect = target
            elif fav_type == 'armour':
                if FavoriteArmour.objects.filter(user=request.user, armour=fav_id).count() > 0:
                    fav = FavoriteArmour.objects.get(user=request.user, armour=fav_id)
                else:
                    fav = FavoriteArmour()
                    target = Armour.objects.get(pk=fav_id)
                    fav.user = request.user
                    fav.armour = target
            elif fav_type == 'mecha':
                if FavoriteMechaArmour.objects.filter(user=request.user, mecha=fav_id).count() > 0:
                    fav = FavoriteMechaArmour.objects.get(user=request.user, mecha=fav_id)
                else:
                    fav = FavoriteMechaArmour()
                    target = MechaArmour.objects.get(pk=fav_id)
                    fav.user = request.user
                    fav.mecha = target
            elif fav_type == 'enhancement':
                if FavoriteEnhancement.objects.filter(user=request.user, enhancement=fav_id).count() > 0:
                    fav = FavoriteEnhancement.objects.get(user=request.user, enhancement=fav_id)
                else:
                    fav = FavoriteEnhancement()
                    target = Enhancement.objects.get(pk=fav_id)
                    fav.user = request.user
                    fav.enhancement = target
            else:
                raise Exception('Invalid type of favorite', 'fav_type_invalid')
            
            fav.note = fav_note
            
            fav.lists.set(bind_to_lists)
            
            fav.save()
            
            data = {
                'updated': True,
            }
        except Exception as e:
            data = {
                'error': str(e)
            }
        finally:
            return JsonResponse(data)
    

class DelFromFavView(LoginRequiredMixin, generic.View):
    
    def post(self, request, *args, **kwargs):
        data = {
            'updated': False,
        }
        try:
            fav_type = request.POST.get('type', None)
            fav_id = int(request.POST.get('id', 0))
            
            if fav_type is None:
                raise Exception('Unknown type of favorite', 'fav_type_unknown')
            
            if fav_id == 0:
                raise Exception('Unable to retrieve the favorite id', 'fav_id_unknown')
            
            if fav_type == 'weapon':
                fav = FavoriteWeapon.objects.get(user=request.user, weapon=fav_id)
                fav.delete()
            elif fav_type == 'module':
                fav = FavoriteModule.objects.get(user=request.user, module=fav_id)
                fav.delete()
            elif fav_type == 'effect':
                fav = FavoriteEffect.objects.get(user=request.user, effect=fav_id)
                fav.delete()
            elif fav_type == 'armour':
                fav = FavoriteArmour.objects.get(user=request.user, armour=fav_id)
                fav.delete()
            elif fav_type == 'mecha':
                fav = FavoriteMechaArmour.objects.get(user=request.user, mecha=fav_id)
                fav.delete()
            elif fav_type == 'enhancement':
                fav = FavoriteEnhancement.objects.get(user=request.user, enhancement=fav_id)
                fav.delete()
            else:
                raise Exception('Invalid type of favorite', 'fav_type_invalid')
            
            data = {
                'updated': True,
            }
        except e:
            data = {
                'error': str(e)
            }
        finally:
            return JsonResponse(data)
    

class DelFavItemView(LoginRequiredMixin, generic.View):
    
    def post(self, request, *args, **kwargs):
        data = {
            'updated': False,
        }
        try:
            fav_type = request.POST.get('type', None)
            fav_id = int(request.POST.get('id', 0))
            fav_list = int(request.POST.get('list', 0))
            
            if fav_type is None:
                raise Exception('Unknown type of favorite', 'fav_type_unknown')
            
            if fav_id == 0:
                raise Exception('Unable to retrieve the favorite id', 'fav_id_unknown')
            
            if fav_type == 'weapon':
                fav = FavoriteWeapon.objects.get(user=request.user, weapon=fav_id)
            elif fav_type == 'module':
                fav = FavoriteModule.objects.get(user=request.user, module=fav_id)
            elif fav_type == 'effect':
                fav = FavoriteEffect.objects.get(user=request.user, effect=fav_id)
            elif fav_type == 'armour':
                fav = FavoriteArmour.objects.get(user=request.user, armour=fav_id)
            elif fav_type == 'mecha':
                fav = FavoriteMechaArmour.objects.get(user=request.user, mecha=fav_id)
            elif fav_type == 'enhancement':
                fav = FavoriteEnhancement.objects.get(user=request.user, enhancement=fav_id)
            else:
                raise Exception('Invalid type of favorite', 'fav_type_invalid')
            
            if fav_list == 0:
                fav.delete()
            else:
                fav.lists.remove(fav_list)
                fav.save()
            
            data = {
                'updated': True,
            }
        except e:
            data = {
                'error': str(e)
            }
        finally:
            return JsonResponse(data)
    
class MoveFavItemView(LoginRequiredMixin, generic.View):
    def post(self, request, *args, **kwargs):
        data = {
            'updated': False,
        }
        
        try:
            fav_type = request.POST.get('type', None)
            fav_id = int(request.POST.get('id', 0))
            add_to_list = request.POST.get('add_to_list', 0)
            remove_from_list = request.POST.get('remove_from_list', 0)
            
            if fav_type is None:
                raise Exception('Unknown type of favorite', 'fav_type_unknown')
            
            if fav_id == 0:
                raise Exception('Unknown item id', 'fav_id_unknown')
            
            if add_to_list == 0:
                raise Exception('Unknown fav list to add', 'fav_list_unknown')
            
            if fav_type == 'weapon':
                try:
                    fav = FavoriteWeapon.objects.get(user=request.user, weapon=fav_id)
                except ObjectDoesNotExist:
                    pass
                except Exception as e:
                    raise e
            elif fav_type == 'module':
                try:
                    fav = FavoriteModule.objects.get(user=request.user, module=fav_id)
                except ObjectDoesNotExist:
                    pass
                except Exception as e:
                    raise e
            elif fav_type == 'effect':
                try:
                    fav = FavoriteEffect.objects.get(user=request.user, effect=fav_id)
                except ObjectDoesNotExist:
                    pass
                except Exception as e:
                    raise e
            elif fav_type == 'armour':
                try:
                    fav = FavoriteArmour.objects.get(user=request.user, armour=fav_id)
                except ObjectDoesNotExist:
                    pass
                except Exception as e:
                    raise e
            elif fav_type == 'mecha':
                try:
                    fav = FavoriteMechaArmour.objects.get(user=request.user, mecha=fav_id)
                except ObjectDoesNotExist:
                    pass
                except Exception as e:
                    raise e
            elif fav_type == 'enhancement':
                try:
                    fav = FavoriteEnhancement.objects.get(user=request.user, enhancement=fav_id)
                except ObjectDoesNotExist:
                    pass
                except Exception as e:
                    raise e
            else:
                raise Exception('Invalid type of favorite', 'fav_type_invalid')
            
            fav.lists.add(add_to_list)
            fav.lists.remove(remove_from_list)
            fav.save()
            
            data = {
                'updated': True,
            }
        except e:
            data = {
                'error': str(e)
            }
        finally:
            return JsonResponse(data)

class OrderFavListView(LoginRequiredMixin, generic.View):
    def post(self, request, *args, **kwargs):
        data = {
            'updated': False,
        }
        
        try:
            new_order = request.POST.get('new_order', '')
            
            if new_order == '':
                raise Exception('Unable to retrieve the new order', 'new_order_unknown')
            
            try:
                new_order = json.loads(new_order)
            except Exception as e:
                print(str(e))
            
            for dict_order in new_order:
                try:
                    fav = FavoriteList.objects.get(user=request.user, pk=dict_order['key'])
                    fav.order = dict_order['order']
                    fav.save()
                except ObjectDoesNotExist:
                    pass
                except Exception as e:
                    raise e
                    
            data = {
                'updated': True,
            }
        except e:
            data = {
                'error': str(e)
            }
        finally:
            return JsonResponse(data)

class AddFavListView(LoginRequiredMixin, generic.View):
    pass
    
class DelFavListView(LoginRequiredMixin, generic.View):
    
    def post(self, request, *args, **kwargs):
        data = {
            'updated': False,
        }
        try:
            fav_list_id = int(request.POST.get('id', 0))
            fav_list_action = request.POST.get('action', 'keep')
            
            if fav_list_id == 0:
                raise Exception('Unable to retrieve the favorite list id', 'fav_list_id_unknown')
            
            if fav_list_action != 'keep' and fav_list_action != 'remove-orphan' and fav_list_action != 'remove-completely':
                raise Exception('Unknown action about what to do of favorite items after deletion', 'fav_unknown_del_list_action')
            
            fav_list = FavoriteList.objects.get(user=request.user, pk=fav_list_id)
            
            for fav in fav_list.favoriteweapon_set.all():
                fav.lists.remove(fav_list)
                
                if fav_list_action == 'remove-completely' or (fav_list_action == 'remove-orphan' and len(fav.lists.all()) == 0):
                    fav.delete()
            for fav in fav_list.favoritemodule_set.all():
                fav.lists.remove(fav_list)
                
                if fav_list_action == 'remove-completely' or (fav_list_action == 'remove-orphan' and len(fav.lists.all()) == 0):
                    fav.delete()
            for fav in fav_list.favoriteenhancement_set.all():
                fav.lists.remove(fav_list)
                
                if fav_list_action == 'remove-completely' or (fav_list_action == 'remove-orphan' and len(fav.lists.all()) == 0):
                    fav.delete()
            for fav in fav_list.favoriteeffect_set.all():
                fav.lists.remove(fav_list)
                
                if fav_list_action == 'remove-completely' or (fav_list_action == 'remove-orphan' and len(fav.lists.all()) == 0):
                    fav.delete()
            for fav in fav_list.favoritearmour_set.all():
                fav.lists.remove(fav_list)
                
                if fav_list_action == 'remove-completely' or (fav_list_action == 'remove-orphan' and len(fav.lists.all()) == 0):
                    fav.delete()
            for fav in fav_list.favoritemechaarmour_set.all():
                fav.lists.remove(fav_list)
                
                if fav_list_action == 'remove-completely' or (fav_list_action == 'remove-orphan' and len(fav.lists.all()) == 0):
                    fav.delete()
            
            fav_list.delete()
            
            data = {
                'updated': True,
            }
        except Exception as e:
            data = {
                'error': str(e)
            }
        finally:
            return JsonResponse(data)

    
class EditFavListView(LoginRequiredMixin, generic.View):
    
    def post(self, request, *args, **kwargs):
        data = {
            'updated': False,
        }
        try:
            fav_list_id = int(request.POST.get('id', 0))
            fav_list_name = request.POST.get('name', '')
            
            if fav_list_id == 0:
                fav_list = FavoriteList()
                fav_list.user = request.user
                fav_list.set_last_order()
            else:
                fav_list = FavoriteList.objects.get(user=request.user, pk=fav_list_id)
            
            fav_list.name = fav_list_name
            fav_list.define_default_name()
            fav_list.save()
            
            data = {
                'updated': True,
            }
        except Exception as e:
            data = {
                'error': str(e)
            }
        finally:
            return JsonResponse(data)
