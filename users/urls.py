from django.urls import include, path
from django.views.decorators.cache import cache_page

from . import views

app_name = 'users'
urlpatterns = [
    path('favorites/', cache_page(0)(views.ListFavoritesView.as_view()), name='fav_list'),
    
    # Profile part
    path('profile/', cache_page(0)(views.ProfileView.as_view()), name='profile'),
    path('del_user/', cache_page(0)(views.DeleteUserView.as_view()), name='del_user'),
    path('password/', cache_page(0)(views.PasswordChangeView.as_view()), name='change_password'),
    
    
    # Fav from detail part
    path('fav_add/', cache_page(0)(views.AddToFavView.as_view()), name='fav_add'),
    path('fav_del/', cache_page(0)(views.DelFromFavView.as_view()), name='fav_del'),
    
    # Item management part
    path('fav_copy_item/', cache_page(0)(views.MoveFavItemView.as_view()), name='fav_copy_item'),
    path('fav_del_item/', cache_page(0)(views.DelFavItemView.as_view()), name='fav_del_item'),
    path('fav_edit_item/', cache_page(0)(views.EditItemToFavView.as_view()), name='fav_edit_item'),
    path('fav_move_item/', cache_page(0)(views.MoveFavItemView.as_view()), name='fav_move_item'),
    
    # List management part
    path('fav_add_list/', cache_page(0)(views.AddFavListView.as_view()), name='fav_add_list'),
    path('fav_del_list/', cache_page(0)(views.DelFavListView.as_view()), name='fav_del_list'),
    path('fav_edit_list/', cache_page(0)(views.EditFavListView.as_view()), name='fav_edit_list'),
    path('fav_order_list/', cache_page(0)(views.OrderFavListView.as_view()), name='fav_order_list'),
]
