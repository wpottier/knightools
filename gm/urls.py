from django.urls import path
from django.views.decorators.cache import cache_page

from .views import misc, calculators, searches, lists, details, prints, faq

app_name = 'gm'
urlpatterns = [
    path('', misc.IndexView.as_view(), name='index'),
    path('about/', misc.AboutView.as_view(), name='about'),
    path('ai_generator/', cache_page(0)(misc.generateAI), name='ai_generator'),
    path('ai_generator/<codename>/', misc.generateAI, name='ai_informations'),
    path('ajax/autocomplete.json', misc.AutocompleteView.as_view(), name='autocomplete'),
    path('ajax/theme', misc.SetTheme.as_view(), name='set_theme'),
    
    path('weapon/', lists.WeaponListView.as_view(), name='weapon_list'),
    path('module/', lists.ModuleListView.as_view(), name='module_list'),
    path('effect/', lists.EffectListView.as_view(), name='effect_list'),
    path('overdrive/', lists.OverdriveListView.as_view(), name='overdrive_list'),
    path('enhancement/', lists.EnhancementListView.as_view(), name='enhancement_list'),
    path('armour/', lists.ArmourListView.as_view(), name='armour_list'),
    path('division/', lists.DivisionListView.as_view(), name='division_list'),
    path('arcana/', lists.ArcanaListView.as_view(), name='arcana_list'),
    path('crest/', lists.CrestListView.as_view(), name='crest_list'),
    path('archetype/', lists.ArchetypeListView.as_view(), name='archetype_list'),
    path('great_deed/', lists.GreatDeedListView.as_view(), name='great_deed_list'),
    path('skill/', lists.HeroicSkillListView.as_view(), name='skill_list'),
    path('vehicle/', lists.VehicleListView.as_view(), name='vehicle_list'),
    path('stance/', lists.StanceListView.as_view(), name='stance_list'),
    path('trauma/', lists.TraumaListView.as_view(), name='trauma_list'),
    path('mecha/', lists.MechaArmourListView.as_view(), name='mecha_list'),
    path('npc_capacity/', lists.NonPlayerCharacterCapacityListView.as_view(), name='npc_capacity_list'),
    path('twin_ability/', lists.ArmourTwinAbilityListView.as_view(), name='twin_list'),
    path('ultimate_aptitude/', lists.UltimateAptitudeListView.as_view(), name='ultimate_list'),
    
    path('weapon/<int:pk>/', details.WeaponDetailView.as_view(), name='weapon_detail'),
    path('module/<int:pk>/', details.ModuleDetailView.as_view(), name='module_detail'),
    path('effect/<int:pk>/', details.EffectDetailView.as_view(), name='effect_detail'),
    path('overdrive/<int:pk>/', details.OverdriveDetailView.as_view(), name='overdrive_detail'),
    path('enhancement/<int:pk>/', details.EnhancementDetailView.as_view(), name='enhancement_detail'),
    path('armour/<int:pk>/', details.ArmourDetailView.as_view(), name='armour_detail'),
    path('division/<int:pk>/', details.DivisionDetailView.as_view(), name='division_detail'),
    path('arcana/<int:pk>/', details.ArcanaDetailView.as_view(), name='arcana_detail'),
    path('great_deed/<int:pk>/', details.GreatDeedDetailView.as_view(), name='great_deed_detail'),
    path('skill/<int:pk>/', details.HeroicSkillDetailView.as_view(), name='skill_detail'),
    path('vehicle/<int:pk>/', details.VehicleDetailView.as_view(), name='vehicle_detail'),
    path('trauma/<int:pk>/', details.TraumaDetailView.as_view(), name='trauma_detail'),
    path('ultimate_aptitude/<int:pk>/', details.UltimateAptitudeDetailView.as_view(), name='ultimate_detail'),
    
    path('weapon/<slug:slug>/', details.WeaponDetailView.as_view(), name='weapon_detail'),
    path('module/<slug:slug>/', details.ModuleDetailView.as_view(), name='module_detail'),
    path('effect/<slug:slug>/', details.EffectDetailView.as_view(), name='effect_detail'),
    path('overdrive/<slug:slug>/', details.OverdriveDetailView.as_view(), name='overdrive_detail'),
    path('enhancement/<slug:slug>/', details.EnhancementDetailView.as_view(), name='enhancement_detail'),
    path('armour/classified', details.ArmourClassifiedView.as_view(), name='armour_unknown'),
    path('armour/<slug:slug>/', details.ArmourDetailView.as_view(), name='armour_detail'),
    path('division/<slug:slug>/', details.DivisionDetailView.as_view(), name='division_detail'),
    path('arcana/<slug:slug>/', details.ArcanaDetailView.as_view(), name='arcana_detail'),
    path('great_deed/<slug:slug>/', details.GreatDeedDetailView.as_view(), name='great_deed_detail'),
    path('skill/<slug:slug>/', details.HeroicSkillDetailView.as_view(), name='skill_detail'),
    path('archetype/<slug:slug>/', details.ArchetypeDetailView.as_view(), name='archetype_detail'),
    path('crest/<slug:slug>/', details.CrestDetailView.as_view(), name='crest_detail'),
    path('vehicle/<slug:slug>/', details.VehicleDetailView.as_view(), name='vehicle_detail'),
    path('stance/<slug:slug>/', details.StanceDetailView.as_view(), name='stance_detail'),
    path('trauma/<slug:slug>/', details.TraumaDetailView.as_view(), name='trauma_detail'),
    path('mecha/classified', details.MechaArmourClassifiedView.as_view(), name='mecha_unknown'),
    path('mecha/<slug:slug>/', details.MechaArmourDetailView.as_view(), name='mecha_detail'),
    path('npc_capacity/<slug:slug>/', details.NonPlayerCharacterCapacityDetailView.as_view(), name='npc_capacity_detail'),
    path('ultimate_aptitude/<slug:slug>/', details.UltimateAptitudeDetailView.as_view(), name='ultimate_detail'),
    
    path('longbow_calculator/', calculators.LongbowCalculatorView.as_view(), name='longbow_calculator'),
    
    path('faq/v1/', faq.QuestionV1ListView.as_view(), name='faqv1'),
    path('faq/v1.5/', faq.QuestionV1Dot5ListView.as_view(), name='faqv1.5'),
    
    path('search/', searches.search, name='search'),
    
    path('print/weapon/', prints.WeaponPrintListView.as_view(), name='print_weapon'),
    path('print/module/', prints.ModulePrintListView.as_view(), name='print_module'),
    path('print/overdrive/', prints.OverdrivePrintListView.as_view(), name='print_overdrive'),
    path('print/enhancement/', prints.EnhancementPrintListView.as_view(), name='print_enhancement'),
    path('print/trauma/', prints.TraumaPrintListView.as_view(), name='print_trauma'),

]
