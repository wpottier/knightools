from django.db import models
from django.urls import reverse
from mptt.models import MPTTModel, TreeForeignKey, TreeManyToManyField

from martor.models import MartorField
from gettext import gettext
import bleach
from markdown import markdown

class Category(MPTTModel):
    name = models.CharField("nom", max_length=128)
    css_classes = models.CharField("classes CSS", max_length=256, blank=True)
    parent = TreeForeignKey(
        'self',
        on_delete=models.CASCADE,
        verbose_name="Catégorie parent",
        related_name="children",
        blank=True,
        null=True
    )
    
    class MPTTMeta:
        order_insertion_by = ['name']
    
    def __str__(self):
        return self.name
    

class Question(models.Model):
    SRC_DISCORD = 'discord'
    SRC_FACEBOOK = 'facebook'
    SRC_FORUM = 'forum'
    SRC_MAIL = 'mail'
    SRC_PDF = 'pdf'
    source_type_choices = (
        (SRC_PDF, 'FAQ PDF'),
        (SRC_FORUM, 'Forum'),
        (SRC_DISCORD, 'Discord'),
        (SRC_FACEBOOK, 'Facebook'),
        (SRC_MAIL, 'Mail'),
    )
    
    question = models.CharField("question", max_length=512)
    answer = MartorField("réponse", blank=True, null=True)
    gm = models.BooleanField('filtre MJ', blank=True, default=False)
    v1 = models.BooleanField('compatible v1', blank=True, default=True)
    v1_5 = models.BooleanField('compatible v1.5', blank=True, default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    category = TreeManyToManyField(
        Category,
        verbose_name="catégorie(s)",
        related_name="questions"
    )
    source_type = models.CharField('type de source', max_length=8, choices=source_type_choices, blank=True, null=True)
    
    def __str__(self):
        if self.gm:
            return ('[MJ] %s: %s' % (self.get_full_category(), self.question))
        return ('%s: %s' % (self.get_full_category(), self.question))
    
    def get_full_category(self):
        extended_fullpathname = []
        
        for categ in self.category.all():
            fullpathname = []
            
            for cat in categ.get_family():
                fullpathname.append(cat.name)
            
            extended_fullpathname.append(' > '.join(fullpathname))
            
        return " ; ".join(extended_fullpathname)
    
    def get_fontawesome_by_source_type(self):
        if self.source_type == self.SRC_DISCORD:
            return 'fab fa-discord'
        if self.source_type == self.SRC_FACEBOOK:
            return 'fab fa-facebook'
        if self.source_type == self.SRC_FORUM:
            return 'fas fa-university'
        if self.source_type == self.SRC_MAIL:
            return 'fas fa-envelope'
        if self.source_type == self.SRC_PDF:
            return 'fas fa-file-pdf'
        
        return ''
    
    def question_text_only(self):
        return bleach.clean(markdown(self.question), tags=[], strip=True)
    
    def answer_text_only(self):
        if self.answer:
            return bleach.clean(markdown(self.answer), tags=[], strip=True)
        else:
            return ''
    
