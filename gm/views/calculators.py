from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from django.db.models import Q, Min

from ..models.weaponry import Weapon, Reach

class LongbowCalculatorView(generic.View):
    def get(self, request):
        return render(request, 'gm/calculators/longbow.html', {
            "longbow": Weapon.objects.get(slug='fusil-longbow'),
        })
    