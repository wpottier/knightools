from django.views import generic
from django.db.models import Q, Count, Prefetch
from django.contrib.auth.mixins import LoginRequiredMixin

from ..models.faq import Category, Question

class QuestionV1ListView(generic.ListView):
    template_name = 'gm/faq/list.html'
    context_object_name = 'faq'
    
    def get_context_data(self, **kwargs):
        context = super(QuestionV1ListView, self).get_context_data(**kwargs)
        
        prefetch = Prefetch(
            'questions',
            queryset=Question.objects.filter(v1=True, gm=False).order_by('-created')
        )
        
        if self.request.user.is_authenticated and self.request.user.game_master:
            prefetch = Prefetch(
                'questions',
                queryset=Question.objects.filter(v1=True).order_by('-created')
            )
        
        categories = Category.objects.prefetch_related(prefetch).all()
        displayed_category = []
        index_category = {}
        
        for category in categories:
            index_category[category.id] = len(displayed_category)
            displayed_category.append((category, category.is_leaf_node() and len(category.questions.all()) > 0))
            
            if category.is_leaf_node():
                if len(category.questions.all()) > 0:
                    for ancestor in category.get_ancestors():
                        displayed_category[index_category[ancestor.id]] = (ancestor, True)
        
        context['displayed_categories'] = displayed_category
        
        return context
        
    def get_queryset(self):
        """Return list of weapon."""
        return (Question
            .objects
            .filter(
                v1=True
            )
            .order_by('category', '-created'))

class QuestionV1Dot5ListView(generic.ListView):
    template_name = 'gm/faq/list.html'
    context_object_name = 'faq'
    
    def get_context_data(self, **kwargs):
        context = super(QuestionV1Dot5ListView, self).get_context_data(**kwargs)
        
        prefetch = Prefetch(
            'questions',
            queryset=Question.objects.filter(v1_5=True, gm=False).order_by('-created')
        )
        
        if self.request.user.is_authenticated and self.request.user.game_master:
            prefetch = Prefetch(
                'questions',
                queryset=Question.objects.filter(v1_5=True).order_by('-created')
            )
        
        categories = Category.objects.prefetch_related(prefetch).all()
        displayed_category = []
        index_category = {}
        
        for category in categories:
            index_category[category.id] = len(displayed_category)
            displayed_category.append((category, category.is_leaf_node() and len(category.questions.all()) > 0))
            
            if category.is_leaf_node():
                if len(category.questions.all()) > 0:
                    for ancestor in category.get_ancestors():
                        displayed_category[index_category[ancestor.id]] = (ancestor, True)
        
        context['displayed_categories'] = displayed_category
        
        return context
        
    def get_queryset(self):
        """Return list of weapon."""
        return (Question
            .objects
            .filter(
                v1_5=True
            )
            .order_by('category', '-created'))
