# Generated by Django 2.2.2 on 2022-08-19 13:01

from django.db import migrations, models
import django.db.models.deletion
import martor.models


class Migration(migrations.Migration):

    dependencies = [
        ('gm', '0090_armourtwinability'),
    ]

    operations = [
        migrations.CreateModel(
            name='UltimateAptitudeType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=16, verbose_name='nom')),
            ],
        ),
        migrations.CreateModel(
            name='UltimateAptitude',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128, verbose_name='nom')),
                ('restriction', models.CharField(blank=True, max_length=512, null=True)),
                ('quote', models.CharField(max_length=512)),
                ('description', martor.models.MartorField()),
                ('cost', models.CharField(blank=True, max_length=128, null=True, verbose_name='coût')),
                ('activation', models.CharField(blank=True, max_length=256, null=True, verbose_name='activation')),
                ('duration', models.CharField(blank=True, max_length=128, null=True, verbose_name='durée')),
                ('armour', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='gm.Armour', verbose_name='armure')),
                ('crest', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='gm.Crest', verbose_name='blason')),
                ('reach', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='gm.Reach', verbose_name='portée')),
                ('type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='gm.UltimateAptitudeType')),
            ],
        ),
    ]
