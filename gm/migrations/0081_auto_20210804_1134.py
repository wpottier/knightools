# Generated by Django 2.2.2 on 2021-08-04 11:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gm', '0080_faction_factioneffect'),
    ]

    operations = [
        migrations.AlterField(
            model_name='factioneffect',
            name='effect_description',
            field=models.TextField(blank=True, verbose_name='description capacité ou malus'),
        ),
        migrations.AlterField(
            model_name='factioneffect',
            name='effect_name',
            field=models.CharField(blank=True, max_length=128, verbose_name='nom capacité ou malus'),
        ),
        migrations.CreateModel(
            name='FactionDefaultEdge',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('level', models.SmallIntegerField(default=0, verbose_name='niveau de relation')),
                ('node_source', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='default_sources', to='gm.Faction')),
                ('node_target', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='default_targets', to='gm.Faction')),
            ],
        ),
    ]
