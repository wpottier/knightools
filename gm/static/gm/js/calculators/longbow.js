var longbow_calculator = {
	
	change_boost: function() {
		if ($('#boost-damage').val() < 0) {
			$('#boost-damage').val('');
		}
		
		if ($('#boost-violence').val() < 0) {
			$('#boost-violence').val('');
		}
		
		var boost_damage = parseInt($('#boost-damage').val(), 10) || 0;
		if (boost_damage > parseInt($('#boost-damage').attr('max'), 10)) {
			$('#boost-damage').addClass('bg-danger');
		}
		else {
			$('#boost-damage').removeClass('bg-danger');
		}
		
		var boost_violence = parseInt($('#boost-violence').val(), 10) || 0;
		if (boost_violence > parseInt($('#boost-violence').attr('max'), 10)) {
			$('#boost-violence').addClass('bg-danger');
		}
		else {
			$('#boost-violence').removeClass('bg-danger');
		}
		
		var damage_sum = parseInt($('#damage span[data-default-value]').attr('data-raw-value'), 10) + boost_damage;
		var violence_sum = parseInt($('#violence span[data-default-value]').attr('data-raw-value'), 10) + boost_violence;
		$('#damage span[data-default-value]').text(damage_sum);
		$('#violence span[data-default-value]').text(violence_sum);
		$('#damage span[data-avg-value]').text(damage_sum * 3);
		$('#violence span[data-avg-value]').text(violence_sum * 3);
		
		longbow_calculator.refresh_effect(true);
	},
	
	change_evolution: function() {
		switch ($(this).val()) {
			case 'upgrade-base':
				var span_damage = $('#damage').find('span[data-default-value]');
				var span_violence = $('#violence').find('span[data-default-value]');
				if ($(this).prop('checked')) {
					span_damage.attr('data-raw-value', parseInt(span_damage.attr('data-default-value'), 10) + 2);
					span_violence.attr('data-raw-value', parseInt(span_violence.attr('data-default-value'), 10) + 2);
					$('#boost-damage').attr('max', 9);
					$('#boost-violence').attr('max', 9);
				}
				else {
					span_damage.attr('data-raw-value', span_damage.attr('data-default-value'));
					span_violence.attr('data-raw-value', span_violence.attr('data-default-value'));
					$('#boost-damage').attr('max', 6);
					$('#boost-violence').attr('max', 6);
					if ($('#boost-damage').val() > 6) {
						$('#boost-damage').val(6);
					}
					if ($('#boost-violence').val() > 6) {
						$('#boost-violence').val(6);
					}
				}
				localStorage.setItem("longbow/upgrade-base", $(this).prop('checked'));
				longbow_calculator.change_boost();
			break;
			
			case 'unlock-list':
				if ($(this).prop('checked')) {
					$('#unlockable-list :checkbox').prop('disabled', false);
				}
				else {
					$('#unlockable-list :checkbox').prop('disabled', true).prop('checked', false);
					longbow_calculator.refresh_effect(false);
				}
				localStorage.setItem("longbow/unlock-list", $(this).prop('checked'));
			break;
			
			case 'reduce-cost':
				if ($(this).prop('checked')) {
					$('#effect-lists span[data-default-value]').each(function() {
						var new_cost = parseInt($(this).attr('data-default-value'), 10) - 2;
						if (new_cost <= 0) {
							new_cost = 1;
						}
						$(this).text(new_cost);
					});
				}
				else {
					$('#effect-lists span[data-default-value]').each(function() {
						$(this).text($(this).attr('data-default-value'));
					});
				}
				localStorage.setItem("longbow/reduce-cost", $(this).prop('checked'));
				longbow_calculator.refresh_effect(true);
			break;
			
			case 'drop-heavy':
				if ($(this).prop('checked')) {
					$('#heavy-effect').hide();
				}
				else {
					$('#heavy-effect').show();
				}
				localStorage.setItem("longbow/drop-heavy", $(this).prop('checked'));
			break;
		}
	},
	
	change_effect: function() {
		var checked = $(this).prop('checked');
		if ($(this).is(':checkbox') && $(this).attr('name')) {
			$('#effect-lists :checkbox[name=' + $(this).attr('name') + ']').prop('checked', false);
			$(this).prop('checked', checked);
		}
		
		longbow_calculator.refresh_effect(false);
	},
	
	change_reach: function() {
		$('#final-reach').text($('#reach>option:selected').attr('data-reach-label'));
		longbow_calculator.refresh_effect(true);
	},
	
	refresh_effect: function(compute_only_cost) {
		var compute_only_cost = compute_only_cost || false;
		var final_cost = 0;
		
		if (!compute_only_cost) {
			$('#final-effect-list li:not(.unremovable)').remove();
		}
		
		$('div.effect-list').each(function() {
			var current_cost = parseInt($(this).find('span[data-default-value]').text(), 10);
			
			if (!compute_only_cost) {
				if ($(this).find(':checkbox:checked').length > 3) {
					$(this).find('div.state').removeClass('p-success').addClass('p-danger');
				}
				else {
					$(this).find('div.state').removeClass('p-danger').addClass('p-success');
				}
			}
			
			$(this).find(':checkbox:checked').each(function() {
				final_cost += current_cost;
				if (!compute_only_cost) {
					$('#final-effect-list').append('<li><a href="/effect/' + $(this).val() + '">' + $(this).parent().find('label').text() + '</a></li>');
				}
			});
		});
		
		if (!compute_only_cost) {
			$('#final-effect-list a[href*=effect]').each(knightools.process_effect_link);
		}
		
		final_cost += parseInt($('#boost-damage').val(), 10) || 0;
		final_cost += parseInt($('#boost-violence').val(), 10) || 0;
		final_cost += parseInt($('#reach').val(), 10);
		
		$('#final-cost').text(final_cost);
	},
	
	reset_form: function() {
		$('#effect-lists :checkbox').prop('checked', false);
		$('#boost-damage').val('');
		$('#boost-violence').val('');
		$('#reach').val(0);
		
		longbow_calculator.change_boost();
		longbow_calculator.change_reach();
		longbow_calculator.refresh_effect(false);
	},
};

$(function() {
	$('#evolutions-check :checkbox').on('change', longbow_calculator.change_evolution);
	$('#effect-lists :checkbox').on('change', longbow_calculator.change_effect);
	$('#boost-damage').on('change', longbow_calculator.change_boost);
	$('#boost-violence').on('change', longbow_calculator.change_boost);
	$('#reach').on('change', longbow_calculator.change_reach);
	
	$('#reset-form').on('click', longbow_calculator.reset_form);
	
	$('#evolutions-check :checkbox').each(function() {
		$(this).prop('checked', JSON.parse(localStorage.getItem('longbow/' + $(this).attr('value') ) ) );
		$(this).trigger('change');
	});
});