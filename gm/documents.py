from django_elasticsearch_dsl import fields
from django_elasticsearch_dsl import Document
from elasticsearch_dsl import analyzer, tokenizer
from django_elasticsearch_dsl.registries import registry

from django.db.models import Q

from gm.models.weaponry import ArmourAbilityVariant, Effect, Enhancement, HeroicSkill, Module, Overdrive, Weapon
from gm.models.character import GreatDeed, Archetype, Armour, ArmourAbility, ArmourEvolution, Crest, MajorArcana, Division, Trauma, MechaArmour, NonPlayerCharacterCapacity, Stance
from gm.models.faq import Question, Category
# New import line just for related_models, for readability
from gm.models.weaponry import WeaponAttack, WeaponAttackEffect, ModuleLevel, ModuleLevelEffect, EnhancementEffect, ArmourAbilityVariant, MechaArmourActionCommon, MechaArmourActionConfiguration
from gm.models.character import Characteristic, Aspect, ArmourAbility, ArmourEvolution, ArmourOverdrive, MechaArmourConfiguration

ascii_folding = analyzer(
    'ascii_folding', 
    tokenizer=tokenizer('standard'), 
    filter=['lowercase', 'asciifolding', "word_delimiter"] 
)

@registry.register_document
class WeaponDocument(Document):
    class Index:
        name = "weapons"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description_text_only", analyzer=ascii_folding)
    nicknames = fields.TextField(attr="nicknames", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    attacks = fields.NestedField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
        "effects": fields.NestedField(properties={
            "name": fields.TextField(attr="get_name", analyzer=ascii_folding),
        }),
    })
    
    class Django:
        model = Weapon
        related_models = [WeaponAttack, WeaponAttackEffect, Effect]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, WeaponAttack):
            return related_instance.weapon
        elif isinstance(related_instance, WeaponAttackEffect):
            return related_instance.weapon_attack.weapon
        elif isinstance(related_instance, Effect):
            return Weapon.objects.filter(attacks__effects__effect__pk=related_instance.pk).all() # don't know if there is a better way...

@registry.register_document
class ModuleDocument(Document):
    class Index:
        name = "modules"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    levels = fields.NestedField(properties={
        "description": fields.TextField(attr="description_text_only", analyzer=ascii_folding),
        "effects": fields.NestedField(properties={
            "name": fields.TextField(attr="get_title", analyzer=ascii_folding),
        }),
    })
    
    class Django:
        model = Module
        related_models = [ModuleLevel, ModuleLevelEffect, Effect]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, ModuleLevel):
            return related_instance.module
        elif isinstance(related_instance, ModuleLevelEffect):
            return related_instance.module_level.module
        elif isinstance(related_instance, Effect):
            return Module.objects.filter(levels__effects__effect__pk=related_instance.pk).all() # don't know if there is a better way...

@registry.register_document
class EffectDocument(Document):
    class Index:
        name = "effects"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description_text_only", analyzer=ascii_folding)
    short_description = fields.TextField(attr="short_description", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    
    class Django:
        model = Effect

@registry.register_document
class EnhancementDocument(Document):
    class Index:
        name = "enhancements"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
        
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description_text_only", analyzer=ascii_folding)
    restriction = fields.TextField(attr="restriction", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    effects = fields.NestedField(properties={
        "name": fields.TextField(attr="get_name", analyzer=ascii_folding),
    })
    
    class Django:
        model = Enhancement
        related_models = [EnhancementEffect, Effect]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, EnhancementEffect):
            return related_instance.enhancement
        elif isinstance(related_instance, Effect):
            return Enhancement.objects.filter(effects__effect__pk=related_instance.pk).all() # don't know if there is a better way...

@registry.register_document
class OverdriveDocument(Document):
    class Index:
        name = "overdrives"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
        
    name = fields.TextField(attr="full_name", analyzer=ascii_folding)
    description = fields.TextField(attr="description_text_only", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    
    characteristic = fields.ObjectField(properties={
        "overdrive_global_description": fields.TextField(attr="overdrive_global_description", analyzer=ascii_folding),
        "aspect": fields.ObjectField(properties={
            "name": fields.TextField(attr="name", analyzer=ascii_folding),
        }),
    })
    
    class Django:
        model = Overdrive
        related_models = [Characteristic, Aspect]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Characteristic):
            return related_instance.overdrives.all()
        elif isinstance(related_instance, Aspect):
            return Overdrive.objects.filter(characteristic__aspect__pk=related_instance.pk).all() # don't know if there is a better way...

@registry.register_document
class ArchetypeDocument(Document):
    class Index:
        name = "archetypes"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
        
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    characteristic_bonus_1 = fields.ObjectField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
    })
    characteristic_bonus_2 = fields.ObjectField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
    })
    characteristic_bonus_3 = fields.ObjectField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
    })
    
    class Django:
        model = Archetype
        related_models = [Characteristic]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Characteristic):
            return Archetype.objects.filter(Q(characteristic_bonus_1__pk=related_instance.pk) | Q(characteristic_bonus_2__pk=related_instance.pk) | Q(characteristic_bonus_3__pk=related_instance.pk)).all(),

@registry.register_document
class MajorArcanaDocument(Document):
    class Index:
        name = "arcanas"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    
    name = fields.TextField(attr='name', analyzer=ascii_folding)
    description = fields.TextField(attr='description', analyzer=ascii_folding)
    past = fields.TextField(attr='past', analyzer=ascii_folding)
    advantage_name = fields.TextField(attr='advantage_name', analyzer=ascii_folding)
    disadvantage_name = fields.TextField(attr='disadvantage_name', analyzer=ascii_folding)
    ai_description = fields.TextField(attr='ai_description', analyzer=ascii_folding)
    ai_advantage_name = fields.TextField(attr='ai_advantage_name', analyzer=ascii_folding)
    ai_disadvantage_name = fields.TextField(attr='ai_disadvantage_name', analyzer=ascii_folding)
    ai_behavior = fields.TextField(attr='ai_behavior', analyzer=ascii_folding)
    slug = fields.TextField(attr='slug', analyzer=ascii_folding)
    destiny_effect = fields.TextField(attr="destiny_effect_text_only", analyzer=ascii_folding)
    advantage = fields.TextField(attr="advantage_text_only", analyzer=ascii_folding)
    disadvantage = fields.TextField(attr="disadvantage_text_only", analyzer=ascii_folding)
    ai_advantage = fields.TextField(attr="ai_advantage_text_only", analyzer=ascii_folding)
    ai_disadvantage = fields.TextField(attr="ai_disadvantage_text_only", analyzer=ascii_folding)
    forced_aspect = fields.ObjectField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
    })
    
    class Django:
        model = MajorArcana
        related_models = [Aspect]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Aspect):
            return related_instance.majorarcana_set.all()

@registry.register_document
class GreatDeedDocument(Document):
    class Index:
        name = "great_deeds"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    restriction = fields.TextField(attr="restriction_text_only", analyzer=ascii_folding)
    aspect_1 = fields.ObjectField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
    })
    aspect_2 = fields.ObjectField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
    })
    
    class Django:
        model = GreatDeed
        related_models = [Aspect]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Characteristic):
            return GreatDeed.objects.filter(Q(aspect_1__pk=related_instance.pk) | Q(aspect_2__pk=related_instance.pk)).all(),

@registry.register_document
class CrestDocument(Document):
    class Index:
        name = "crests"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description", analyzer=ascii_folding)
    vow = fields.TextField(attr="vow", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    
    class Django:
        model = Crest

@registry.register_document
class ArmourDocument(Document):
    class Index:
        name = "armours"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    id = fields.IntegerField(attr="id") # used to filter allowed armours
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    background_description = fields.TextField(attr="background_description", analyzer=ascii_folding)
    technical_description = fields.TextField(attr="technical_description", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    additional_notes = fields.TextField(attr="additional_notes_text_only", analyzer=ascii_folding)
    overdrives = fields.NestedField(properties={
        "characteristic": fields.ObjectField(properties={
            "name": fields.TextField(attr="name", analyzer=ascii_folding),
        }),
    })
    evolutions = fields.NestedField(properties={
        "description": fields.TextField(attr="description_text_only", analyzer=ascii_folding),
    })
    abilities = fields.NestedField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
        "description": fields.TextField(attr="description_text_only", analyzer=ascii_folding),
        "variants" : fields.NestedField(properties={
            "name": fields.TextField(attr="name", analyzer=ascii_folding),
            "description": fields.TextField(attr="description_text_only", analyzer=ascii_folding),
        }),
    })
    
    class Django:
        model = Armour
        related_models = [ArmourAbility, ArmourAbilityVariant, ArmourEvolution, ArmourOverdrive, Characteristic]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, ArmourAbility):
            return related_instance.armour
        if isinstance(related_instance, ArmourAbilityVariant):
            return related_instance.armour_ability.armour
        if isinstance(related_instance, ArmourEvolution):
            return related_instance.armour
        if isinstance(related_instance, ArmourOverdrive):
            return related_instance.armour
        if isinstance(related_instance, Characteristic):
            return Armour.objects.filter(overdrives__characteristic__pk=related_instance.pk).all()

@registry.register_document
class DivisionDocument(Document):
    class Index:
        name = "divisions"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description", analyzer=ascii_folding)
    disadvantage_name = fields.TextField(attr="disadvantage_name", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    aspect_bonus = fields.ObjectField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
    })
    disadvantage = fields.TextField(attr="disadvantage_text_only", analyzer=ascii_folding)
    
    class Django:
        model = Division
        related_models = [Aspect]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Aspect):
            return related_instance.division_set.all()

@registry.register_document
class HeroicSkillDocument(Document):
    class Index:
        name = "heroic_skills"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description_text_only", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    
    class Django:
        model = HeroicSkill

@registry.register_document
class TraumaDocument(Document):
    class Index:
        name = "traumas"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    id = fields.IntegerField(attr="id") # used to filter allowed traumas
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description_text_only", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    
    class Django:
        model = Trauma

@registry.register_document
class MechaArmourDocument(Document):
    class Index:
        name = "mecha_armours"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    id = fields.IntegerField(attr="id") # used to filter allowed MechaArmours
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description_text_only", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    actions = fields.NestedField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
        "description": fields.TextField(attr="description_text_only", analyzer=ascii_folding),
        "effects": fields.TextField(attr="effects_text_only", analyzer=ascii_folding),
    })
    configurations = fields.NestedField(properties={
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
        "description": fields.TextField(attr="description_text_only", analyzer=ascii_folding),
        "actions" : fields.NestedField(properties={
            "name": fields.TextField(attr="name", analyzer=ascii_folding),
            "description": fields.TextField(attr="description_text_only", analyzer=ascii_folding),
            "effects": fields.TextField(attr="effects_text_only", analyzer=ascii_folding),
        }),
    })
    
    class Django:
        model = MechaArmour
        related_models = [MechaArmourConfiguration, MechaArmourActionCommon, MechaArmourActionConfiguration]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, MechaArmourConfiguration):
            return related_instance.mecha
        if isinstance(related_instance, MechaArmourActionCommon):
            return related_instance.mecha
        if isinstance(related_instance, MechaArmourActionConfiguration):
            return related_instance.configuration.mecha

@registry.register_document
class NonPlayerCharacterCapacityDocument(Document):
    class Index:
        name = "npc_capacities"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    id = fields.IntegerField(attr="id") # used to filter allowed capacities
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description_text_only", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    
    class Django:
        model = NonPlayerCharacterCapacity

@registry.register_document
class StanceDocument(Document):
    class Index:
        name = "stances"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    name = fields.TextField(attr="name", analyzer=ascii_folding)
    description = fields.TextField(attr="description_text_only", analyzer=ascii_folding)
    bonus = fields.TextField(attr="bonus_text_only", analyzer=ascii_folding)
    malus = fields.TextField(attr="malus_text_only", analyzer=ascii_folding)
    slug = fields.TextField(attr="slug", analyzer=ascii_folding)
    
    class Django:
        model = Stance

@registry.register_document
class QuestionDocument(Document):
    class Index:
        name = "faq"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    id = fields.IntegerField(attr="id") # used to filter allowed capacities
    v1_5 = fields.BooleanField(attr="v1_5")
    v1 = fields.BooleanField(attr="v1")
    gm = fields.BooleanField(attr="gm")
    updated = fields.DateField(attr="updated")
    question = fields.TextField(attr="question_text_only", analyzer=ascii_folding)
    answer = fields.TextField(attr="answer_text_only", analyzer=ascii_folding)
    category = fields.NestedField(properties={
        "id": fields.IntegerField(),
        "name": fields.TextField(attr="name", analyzer=ascii_folding),
        "get_family": fields.NestedField(properties={
            "id": fields.IntegerField(),
            "name": fields.TextField(attr="name", analyzer=ascii_folding),
        }),
    })
    
    class Django:
        model = Question
        # related_models = [Category,]
