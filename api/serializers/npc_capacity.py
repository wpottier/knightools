from html5lib import serialize
from rest_framework import serializers
from gm.models.character import NonPlayerCharacterCapacity
from .common import NameRelatedSerializer

class DetailNonPlayerCharacterCapacitySerializer(serializers.ModelSerializer):
    """Serializer to map the Model NonPlayerCharacterCapacity instance into JSON format from a NonPlayerCharacterCapacity Detail view."""
    
    min_difficulty = NameRelatedSerializer(read_only=True)
    description = serializers.CharField(source="description_text_only")
    
    class Meta:
        model = NonPlayerCharacterCapacity
        fields = (
            'id',
            'name',
            'description',
            'min_difficulty',
            'common',
            'penalty',
            'elite',
            'human_allowed',
            'anathema_allowed',
            'ether_allowed',
            'void_allowed',
            'precision',
            'slug',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'min_difficulty',
            'common',
            'penalty',
            'elite',
            'human_allowed',
            'anathema_allowed',
            'ether_allowed',
            'void_allowed',
            'precision',
            'slug',
        )

class ListNonPlayerCharacterCapacitySerializer(serializers.ModelSerializer):
    """Serializer to map the Model NonPlayerCharacterCapacity instance into JSON format from a NonPlayerCharacterCapacity List view."""
    
    min_difficulty = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = NonPlayerCharacterCapacity
        fields = (
            'id',
            'name',
            'min_difficulty',
            'common',
            'penalty',
            'elite',
            'human_allowed',
            'anathema_allowed',
            'ether_allowed',
            'void_allowed',
            'precision',
            'slug',
        )
        read_only_fields = (
            'id',
            'name',
            'min_difficulty',
            'common',
            'penalty',
            'elite',
            'human_allowed',
            'anathema_allowed',
            'ether_allowed',
            'void_allowed',
            'precision',
            'slug',
        )