from rest_framework import serializers
from gm.models.character import TraumaCategory, Trauma

class ListTraumaCategoryTraumaSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Trauma instance into JSON format from a Trauma Category Detail view."""
    
    description = serializers.CharField(source="description_text_only")
    
    class Meta:
        model = Trauma
        fields = (
            'id',
            'name',
            'description',
            'die_value',
            'slug',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'die_value',
            'slug',
        )

class DetailTraumaCategorySerializer(serializers.ModelSerializer):
    """Serializer to map the Model TraumaCategory instance into JSON format from a trauma Detail view."""
    
    traumas = ListTraumaCategoryTraumaSerializer(many=True, read_only=True)
    
    class Meta:
        model = TraumaCategory
        fields = (
            'id',
            'name',
            'description',
            'hop_recovered',
            'traumas',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'hop_recovered',
            'traumas',
        )

class ListTraumaCategorySerializer(serializers.ModelSerializer):
    """Serializer to map the Model TraumaCategory instance into JSON format from a trauma Detail view."""
    
    class Meta:
        model = TraumaCategory
        fields = (
            'id',
            'name',
            'description',
            'hop_recovered',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'hop_recovered',
        )

class SummaryTraumaCategorySerializer(serializers.ModelSerializer):
    """Serializer to map the Model HeroicSkill instance into JSON format from a heroic skill Detail view."""
    
    class Meta:
        model = TraumaCategory
        fields = (
            'id',
            'name',
        )
        read_only_fields = (
            'id',
            'name',
        )

class DetailTraumaSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Trauma instance into JSON format from a Trauma Category Detail view."""
    
    description = serializers.CharField(source="description_text_only")
    category = SummaryTraumaCategorySerializer(read_only=True)
    
    class Meta:
        model = Trauma
        fields = (
            'id',
            'name',
            'description',
            'die_value',
            'category',
            'slug',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'die_value',
            'category',
            'slug',
        )
