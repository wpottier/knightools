from rest_framework import serializers
from gm.models.character import Armour, ArmourOverdrive, ArmourAbility, ArmourEvolution
from gm.models.weaponry import ArmourAbilityVariant, NonPlayerCharacterWeapon, NonPlayerCharacterModuleLevel
from .common import NameRelatedSerializer, CharacteristicSerializer, CommonModuleLevelSerializer, CommonNonPlayerCharacterSerializer
from .weapons import WLWeaponSerializer

class DetailArmourOverdriveSerializer(serializers.ModelSerializer):
    """Serializer to map the Model ArmourOverdrive instance into JSON format from an Armour Detail view."""
    characteristic = CharacteristicSerializer(read_only=True)
    
    class Meta:
        model = ArmourOverdrive
        fields = (
            'characteristic',
        )
        read_only_fields = (
            'characteristic',
        )
    
class DetailNonPlayerCharacterWeaponSerializer(serializers.ModelSerializer):
    """Serializer to map the Model NonPlayerCharacterWeapon instance into JSON format from an Armour Detail view."""
    weapon = WLWeaponSerializer(read_only=True)
    
    class Meta:
        model = NonPlayerCharacterWeapon
        fields = (
            'weapon',
            'quantity',
        )
        read_only_fields = (
            'weapon',
            'quantity',
        )
    
class DetailNonPlayerCharacterModuleLevelSerializer(serializers.ModelSerializer):
    """Serializer to map the Model NonPlayerCharacterModuleLevel instance into JSON format from an Armour Detail view."""
    module_level = CommonModuleLevelSerializer(read_only=True)
    
    class Meta:
        model = NonPlayerCharacterModuleLevel
        fields = (
            'module_level',
        )
        read_only_fields = (
            'module_level',
        )
    
class DetailArmourAbilityVariantSerializer(serializers.ModelSerializer):
    """Serializer to map the Model ArmourAbilityVariant instance into JSON format from an Armour Detail view."""
    reach = NameRelatedSerializer(read_only=True)
    npc = CommonNonPlayerCharacterSerializer(read_only=True)
    
    class Meta:
        model = ArmourAbilityVariant
        fields = (
            'name',
            'description',
            'energy',
            'activation',
            'duration',
            'damage_dice',
            'damage_bonus',
            'violence_dice',
            'violence_bonus',
            'reach',
            'npc',
        )
        read_only_fields = (
            'name',
            'description',
            'energy',
            'activation',
            'duration',
            'damage_dice',
            'damage_bonus',
            'violence_dice',
            'violence_bonus',
            'reach',
            'npc',
        )
    
class DetailArmourAbilitySerializer(serializers.ModelSerializer):
    """Serializer to map the Model ArmourAbility instance into JSON format from an Armour Detail view."""
    variants = DetailArmourAbilityVariantSerializer(read_only=True, many=True)
    
    class Meta:
        model = ArmourAbility
        fields = (
            'name',
            'description',
            'default_max_variant',
            'energy',
            'activation',
            'duration',
            'variants',
        )
        read_only_fields = (
            'name',
            'description',
            'default_max_variant',
            'energy',
            'activation',
            'duration',
            'variants',
        )
    
class DetailArmourEvolutionSerializer(serializers.ModelSerializer):
    """Serializer to map the Model ArmourEvolution instance into JSON format from an Armour Detail view."""
    
    class Meta:
        model = ArmourEvolution
        fields = (
            'level',
            'unlock_at',
            'description',
        )
        read_only_fields = (
            'level',
            'unlock_at',
            'description',
        )
    
class DetailArmourSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Armour instance into JSON format from an Armour Detail view."""
    overdrives = DetailArmourOverdriveSerializer(read_only=True, many=True)
    abilities = DetailArmourAbilitySerializer(read_only=True, many=True)
    evolutions = DetailArmourEvolutionSerializer(read_only=True, many=True)
    
    class Meta:
        model = Armour
        fields = (
            'id',
            'name',
            'background_description',
            'technical_description',
            'additional_notes',
            'generation',
            'energy_points',
            'armour_points',
            'force_field',
            'slot_head',
            'slot_left_arm',
            'slot_right_arm',
            'slot_torso',
            'slot_left_leg',
            'slot_right_leg',
            'slug',
            'overdrives',
            'abilities',
            'evolutions',
        )
        read_only_fields = (
            'id',
            'name',
            'background_description',
            'technical_description',
            'additional_notes',
            'generation',
            'energy_points',
            'armour_points',
            'force_field',
            'slot_head',
            'slot_left_arm',
            'slot_right_arm',
            'slot_torso',
            'slot_left_leg',
            'slot_right_leg',
            'slug',
            'overdrives',
            'abilities',
            'evolutions',
        )

class ListArmourSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Armour instance into JSON format from an Armour List view."""
    overdrives = DetailArmourOverdriveSerializer(read_only=True, many=True)
    
    class Meta:
        model = Armour
        fields = (
            'id',
            'name',
            'background_description',
            'technical_description',
            'additional_notes',
            'generation',
            'energy_points',
            'armour_points',
            'force_field',
            'slot_head',
            'slot_left_arm',
            'slot_right_arm',
            'slot_torso',
            'slot_left_leg',
            'slot_right_leg',
            'slug',
            'overdrives',
        )
        read_only_fields = (
            'id',
            'name',
            'background_description',
            'technical_description',
            'additional_notes',
            'generation',
            'energy_points',
            'armour_points',
            'force_field',
            'slot_head',
            'slot_left_arm',
            'slot_right_arm',
            'slot_torso',
            'slot_left_leg',
            'slot_right_leg',
            'slug',
            'overdrives',
        )