from rest_framework import serializers
from gm.models.character import Stance

class DetailStanceSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Stance instance into JSON format from a Stance Detail view."""
    
    description = serializers.CharField(source="description_text_only")
    bonus = serializers.CharField(source="bonus_text_only")
    malus = serializers.CharField(source="malus_text_only")
    examples = serializers.CharField(source="examples_text_only")
    constraint = serializers.CharField(source="constraint_text_only")
    
    class Meta:
        model = Stance
        fields = (
            'id',
            'name',
            'constraint',
            'description',
            'bonus',
            'malus',
            'examples',
        )
        read_only_fields = (
            'id',
            'name',
            'constraint',
            'description',
            'bonus',
            'malus',
            'examples',
        )

class ListStanceSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Stance instance into JSON format from a Stance List view."""
    
    description = serializers.CharField(source="description_text_only")
    
    class Meta:
        model = Stance
        fields = (
            'id',
            'name',
            'constraint',
            'description',
        )
        read_only_fields = (
            'id',
            'name',
            'constraint',
            'description',
        )