from rest_framework import generics
from .serializers.weapons import WLWeaponSerializer, WDWeaponSerializer, WCWeaponCategorySerializer
from .serializers.modules import MLModuleSerializer, MDModuleSerializer, MCModuleCategorySerializer
from .serializers.effects import EDEffectSerializer, ELEffectSerializer
from .serializers.enhancements import BDEnhancementSerializer, BLEnhancementSerializer
from .serializers.overdrives import DetailOverdriveSerializer, ListOverdriveSerializer
from .serializers.archetypes import DetailArchetypeSerializer, ListArchetypeSerializer
from .serializers.crests import DetailCrestSerializer, ListCrestSerializer
from .serializers.great_deeds import DetailGreatDeedSerializer, ListGreatDeedSerializer
from .serializers.arcanas import DetailMajorArcanaSerializer, ListMajorArcanaSerializer
from .serializers.divisions import DetailDivisionSerializer, ListDivisionSerializer
from .serializers.armours import DetailArmourSerializer, ListArmourSerializer
from .serializers.skills import DetailHeroicSkillSerializer, ListHeroicSkillSerializer, ListHeroicSkillCategorySerializer
from .serializers.npc_capacity import DetailNonPlayerCharacterCapacitySerializer, ListNonPlayerCharacterCapacitySerializer
from .serializers.stance import DetailStanceSerializer, ListStanceSerializer
from .serializers.trauma import DetailTraumaCategorySerializer, ListTraumaCategorySerializer, DetailTraumaSerializer
from .serializers.mecha_armour import DetailMechaArmourSerializer, ListMechaArmourSerializer

from gm.models.character import Archetype, Crest, GreatDeed, Armour, MajorArcana, Division, NonPlayerCharacterCapacity, Stance, Trauma, TraumaCategory, MechaArmour
from gm.models.weaponry import Weapon, WeaponCategory, Module, ModuleCategory, Effect, Enhancement, Overdrive, HeroicSkill, HeroicSkillCategory
# from django.shortcuts import render

class EffectListView(generics.ListAPIView):
    """This class handles the list requests of effects."""
    queryset = Effect.objects.all()
    serializer_class = ELEffectSerializer
    
class EffectDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an effect."""
    queryset = Effect.objects.all()
    serializer_class = EDEffectSerializer
    
class EffectDetailSlugView(generics.RetrieveAPIView):
    """This class handles the get requests of an effect."""
    queryset = Effect.objects.all()
    serializer_class = EDEffectSerializer
    lookup_field = 'slug'
    
class EnhancementListView(generics.ListAPIView):
    """This class handles the list requests of enhancements."""
    queryset = Enhancement.objects.all()
    serializer_class = BLEnhancementSerializer
    
class EnhancementDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an enhancement."""
    queryset = Enhancement.objects.all()
    serializer_class = BDEnhancementSerializer
    
class ModuleListView(generics.ListAPIView):
    """This class handles the list requests of modules."""
    queryset = Module.objects.all()
    serializer_class = MLModuleSerializer
    
class ModuleDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a module."""
    queryset = Module.objects.all()
    serializer_class = MDModuleSerializer
    
class ModuleCategoryListView(generics.ListAPIView):
    """This class handles the list requests of module categories."""
    queryset = ModuleCategory.objects.all()
    serializer_class = MCModuleCategorySerializer
    
class ModuleCategoryDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a module category."""
    queryset = ModuleCategory.objects.all()
    serializer_class = MCModuleCategorySerializer
    
class WeaponListView(generics.ListAPIView):
    """This class handles the list requests of weapons."""
    queryset = Weapon.objects.all()
    serializer_class = WLWeaponSerializer
    
class WeaponDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a weapon."""
    queryset = Weapon.objects.all()
    serializer_class = WDWeaponSerializer
    
class WeaponCategoryListView(generics.ListAPIView):
    """This class handles the list requests of weapon categories."""
    queryset = WeaponCategory.objects.all()
    serializer_class = WCWeaponCategorySerializer
    
class WeaponCategoryDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a weapon category."""
    queryset = WeaponCategory.objects.all()
    serializer_class = WCWeaponCategorySerializer
    
class OverdriveListView(generics.ListAPIView):
    """This class handles the list requests of overdrives."""
    queryset = Overdrive.objects.all()
    serializer_class = ListOverdriveSerializer
    
class OverdriveDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an overdrive."""
    queryset = Overdrive.objects.all()
    serializer_class = DetailOverdriveSerializer
    
class ArchetypeListView(generics.ListAPIView):
    """This class handles the list requests of archetypes."""
    queryset = Archetype.objects.all()
    serializer_class = ListArchetypeSerializer
    
class ArchetypeDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an archetype."""
    queryset = Archetype.objects.all()
    serializer_class = DetailArchetypeSerializer
    
class CrestListView(generics.ListAPIView):
    """This class handles the list requests of crests."""
    queryset = Crest.objects.all()
    serializer_class = ListCrestSerializer
    
class CrestDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an crest."""
    queryset = Crest.objects.all()
    serializer_class = DetailCrestSerializer
    
class GreatDeedListView(generics.ListAPIView):
    """This class handles the list requests of great deeds."""
    queryset = GreatDeed.objects.all()
    serializer_class = ListGreatDeedSerializer
    
class GreatDeedDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a great deed."""
    queryset = GreatDeed.objects.all()
    serializer_class = DetailGreatDeedSerializer
    
class DivisionListView(generics.ListAPIView):
    """This class handles the list requests of divisions."""
    queryset = Division.objects.all()
    serializer_class = ListDivisionSerializer
    
class DivisionDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an division."""
    queryset = Division.objects.all()
    serializer_class = DetailDivisionSerializer
    
class MajorArcanaListView(generics.ListAPIView):
    """This class handles the list requests of major arcanas."""
    queryset = MajorArcana.objects.all()
    serializer_class = ListMajorArcanaSerializer
    
class MajorArcanaDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an major arcana."""
    queryset = MajorArcana.objects.all()
    serializer_class = DetailMajorArcanaSerializer
    
class ArmourListView(generics.ListAPIView):
    """This class handles the list requests of armours."""
    queryset = Armour.objects.all()
    serializer_class = ListArmourSerializer
    
class ArmourDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an armour."""
    queryset = Armour.objects.all()
    serializer_class = DetailArmourSerializer
    
class HeroicSkillListView(generics.ListAPIView):
    """This class handles the list requests of heroic skills."""
    queryset = HeroicSkill.objects.all()
    serializer_class = ListHeroicSkillSerializer
    
class HeroicSkillDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an heroic skill."""
    queryset = HeroicSkill.objects.all()
    serializer_class = DetailHeroicSkillSerializer
    
class HeroicSkillCategoryListView(generics.ListAPIView):
    """This class handles the list requests of heroic skill categories."""
    queryset = HeroicSkillCategory.objects.all()
    serializer_class = ListHeroicSkillCategorySerializer
    
class HeroicSkillCategoryDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a heroic skill category."""
    queryset = HeroicSkillCategory.objects.all()
    serializer_class = ListHeroicSkillCategorySerializer
    
class NonPlayerCharacterCapacityListView(generics.ListAPIView):
    """This class handles the list requests of NPC capacities."""
    queryset = NonPlayerCharacterCapacity.objects.all()
    serializer_class = ListNonPlayerCharacterCapacitySerializer
    
class NonPlayerCharacterCapacityDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of an NPC capacity."""
    queryset = NonPlayerCharacterCapacity.objects.all()
    serializer_class = DetailNonPlayerCharacterCapacitySerializer
    
class StanceListView(generics.ListAPIView):
    """This class handles the list requests of NPC capacities."""
    queryset = Stance.objects.all()
    serializer_class = ListStanceSerializer
    
class StanceDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a NPC capacity."""
    queryset = Stance.objects.all()
    serializer_class = DetailStanceSerializer
    
class TraumaCategoryListView(generics.ListAPIView):
    """This class handles the list requests of trauma categories."""
    queryset = TraumaCategory.objects.all()
    serializer_class = ListTraumaCategorySerializer
    
class TraumaCategoryDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a trauma categories."""
    queryset = TraumaCategory.objects.all()
    serializer_class = DetailTraumaCategorySerializer
    
class TraumaDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a trauma."""
    queryset = Trauma.objects.all()
    serializer_class = DetailTraumaSerializer
    
class MechaArmourListView(generics.ListAPIView):
    """This class handles the list requests of NPC capacities."""
    queryset = MechaArmour.objects.all()
    serializer_class = ListMechaArmourSerializer
    
class MechaArmourDetailView(generics.RetrieveAPIView):
    """This class handles the get requests of a NPC capacity."""
    queryset = MechaArmour.objects.all()
    serializer_class = DetailMechaArmourSerializer
    