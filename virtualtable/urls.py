from django.urls import include, path
from django.views.decorators.cache import cache_page

from . import views

app_name = 'virtualtable'
urlpatterns = [
    path('create/', cache_page(0)(views.CreateTableView.as_view()), name='vt_create'),
    path('list/', cache_page(0)(views.ListTableView.as_view()), name='vt_list'),
    path('detail/<int:pk>/', cache_page(0)(views.DetailTableView.as_view()), name='vt_detail'),
    path('delete/<int:pk>/', cache_page(0)(views.DeleteTableView.as_view()), name='vt_delete'),
    path('update/<int:pk>/', cache_page(0)(views.UpdateTableView.as_view()), name='vt_update'),
    path('help/', cache_page(0)(views.HelpTableView.as_view()), name='vt_help'),
    
    path('logo/<int:vt>/<int:faction>', cache_page(0)(views.UpdateLogoFactionView.as_view()), name='vt_faction_update'),
    
    path('test/', cache_page(0)(views.TestView.as_view()), name='vt_test'),
    
    path('consent/<int:pk>', cache_page(0)(views.ConsentTableView.as_view()), name='vt_consent'),
    path('join/<str:invitation_token>/', cache_page(0)(views.PlayerJoinTableView.as_view()), name='join'),
    
    path('ajax/default_consent.json', views.DefaultConsentValues.as_view(), name='vt_default_consent'),
]
