virtual_table_cy = {
	MENU_OPTIONS: {
		CORE: {
			selector: 'core',
			commands: [
				{
					content: '<i title="ajouter une faction" class="fa-solid fa-circle-plus">',
					select: function(ele){
						virtual_table_cy.element_in_edition = 0;
						
						virtual_table.websocket_send(JSON.stringify({
							"command": "faction_add_form",
						}));
					},
					enabled: true
				},
				{
					content: '<i title="(ré-)importer les factions de base" class="fa-solid fa-cloud-arrow-down"></i>',
					select: function(ele){
						virtual_table.websocket_send(JSON.stringify({
							"command": "factions_import",
						}));
					},
					enabled: true
				}
			],
		},
		NODE: {
			selector: 'node',
			commands: [
				{
					content: '<i class="fa-solid fa-edit">',
					select: function(ele){
						// console.log( ele.id(), ele.attr('name') );
						virtual_table_cy.element_in_edition = parseInt(ele.id(),10);
						
						virtual_table.websocket_send(JSON.stringify({
							"command": "faction_form",
							"faction_id": parseInt(virtual_table_cy.element_in_edition, 10),
						}));
					},
					enabled: true
				},
				{
					content: '<i class="fa-solid fa-handshake-simple"></i>',
					select: function(ele){
						// console.log( ele.id(), ele.attr('name') );
						virtual_table_cy.element_in_edition = parseInt(ele.id(),10);
						
						virtual_table.websocket_send(JSON.stringify({
							"command": "faction_relations_manager",
							"faction_id": parseInt(virtual_table_cy.element_in_edition, 10),
						}));
					},
					enabled: true
				},
				{
					content: '<i class="fa-solid fa-eye">/<i class="fa-solid fa-eye-slash">',
					select: function(ele) {
						virtual_table_cy.toggle_faction(ele.id());
					},
					enabled: true,
				},
				{
					content: '<i class="fa-solid fa-circle-xmark">',
					select: function(ele) {
						virtual_table_cy.element_in_edition = parseInt(ele.id(),10);
						$('#confirm-delete-faction').modal('show');
					},
					enabled: true,
				},
			],
		},
	},
	menus: {
		node: null,
		core: null,
	},
	concentric_layout: null,
	breadthfirst_layout: null,
	styles: [
		{
			"selector": 'node',
			"style": {
				'text-background-color': '#000',
				'text-outline-color': '#f00',
				'background-color': '#fff',
				'label': 'data(name)',
				// 'label': '',
				'color': '#fff',
				'text-background-color': '#000',
				'text-background-shape': 'rectangle',
				"text-background-opacity": 1,
				"text-background-padding": 2,
				'background-image': 'data(background)',
				'background-fit': 'cover cover',
				'background-position-x': '50%',
				'background-position-y': '50%',
			}
		},
		{
			"selector": 'edge',
			"style": {
				'width': 3,
				'line-color': '#ccc',
				'target-arrow-color': '#ccc',
				'target-arrow-shape': 'none',
				'curve-style': 'bezier',
				// 'label': 'data(level)',
				// 'source-label': 'data(source_label)',
				// 'target-label': 'data(target_label)',
			}
		},
		{
			"selector": 'edge:selected',
			"style": {
				// 'label': 'data(level)',
				// 'source-label': 'data(source_label)',
				// 'target-label': 'data(target_label)',
				'z-index': 9999,
			}
		},
		{
			"selector": 'node:selected',
			"style": {
				'label': 'data(name)',
				'z-index': 9999,
			}
		},
		{
			"selector": ".hidden",
			"style": {
				"display": "none",
			}
		},
		{
			"selector": ".visible",
			"style": {
				"background-color": "#9c9",
			}
		},
		{
			"selector": ".not-visible",
			"style": {
				"background-color": "#c99",
			}
		},
	],
	cy: null,
	pc_node: null,
	refresh_on_next_init: false,
	
	add_effect: function(effect) {
		if (typeof effect === 'undefined') {
			effect = {
				id: 0,
				level: '',
				effect: 0,
				level_name: '',
				level_memo: '',
				effect_name: '',
				effect_description: '',
			};
		}
		var template = document.importNode(document.querySelector('#template-effect-form').content, true);
		
		template.querySelector('.effect-form-row').setAttribute('eid', effect.id);
		template.querySelector('[name=level]').value = effect.level;
		template.querySelector('[name=effect]').value = effect.effect;
		template.querySelector('[name=level_name]').value = effect.level_name;
		template.querySelector('[name=level_memo]').value = effect.level_memo;
		template.querySelector('[name=effect_name]').value = effect.effect_name;
		template.querySelector('[name=effect_description]').value = effect.effect_description;
		
		document.querySelector('#effects-form').appendChild(template);
	},
	
	center_element: function(elem) {
		// elem = virtual_table_cy.cy.elements('node:selected');
		var has_hidden = virtual_table_cy.cy.elements('node.hidden').length;
		virtual_table_cy.concentric_layout = virtual_table_cy.cy.elements().layout({
			name: 'concentric',
			fit: true,
			animate: false,
			avoidOverlap: true,
			concentric: function( ele ) {
				
				if (ele.same(elem)) {
					return 10;
				}
				else {
					if (ele.is('node.hidden')) {
						return 1;
					}
					else {
						return 5;
					}
				}
			},
			equidistant: true,
			minNodeSpacing: has_hidden ? 80 : 120,
		});
		
		virtual_table_cy.concentric_layout.run();
	},
	
	change_affinity: function(elem) {
		virtual_table.websocket_send(JSON.stringify({
			"command": "faction_affinity",
			"faction_id": virtual_table_cy.focused_element,
			"player": parseInt($(elem).attr('pid'), 10),
			"affinity": parseInt($(elem).val(), 10),
		}));
		virtual_table_cy.refresh_on_next_init = true;
	},
	
	check_level_validity: function(elem) {
		var duplicate = false;
		$(elem).closest('.effect-form-row').prevAll('.effect-form-row').each(function() {
			if ($(this).find(':input[name=level]').val() === $(elem).val()) {
				duplicate = true;
				return false;
			}
		});
		
		if (duplicate) {
			$(elem).get(0).setCustomValidity('duplicate');
		}
		else {
			$(elem).get(0).setCustomValidity('');
		}
	},
	
	delete_effect: function(elem) {
		$(elem).closest('.effect-form-row').remove();
	},
	
	delete_node: function() {
		virtual_table.websocket_send(JSON.stringify({
			"command": "faction_delete_node",
			"faction_id": virtual_table_cy.element_in_edition,
		}));
		
		$('#confirm-delete-faction').modal('hide');
	},
	
	destroy_cy_menu: function() {
		if (virtual_table_cy.menus.core !== null) {
			try {
				virtual_table_cy.menus.core.destroy();
			}
			catch(e) {
				console.error(e);
			}
			virtual_table_cy.menus.core = null;
		}
		
		if (virtual_table_cy.menus.node !== null) {
			try {
				virtual_table_cy.menus.node.destroy();
			}
			catch(e) {
				console.error(e);
			}
			virtual_table_cy.menus.node = null;
		}
	},
	
	display_faction: function(data) {
		$('#faction-relation-title').text(data.faction.title);
		
		if (data.faction.desc !== '') {
			$('#faction-public-resume').text(data.faction.desc);
		}
		else {
			$('#faction-public-resume').text($('#faction-public-resume').attr('none-text'));
		}
		
		if (data.faction.secret !== '') {
			$('#players-discovered-secrets').text(data.faction.secret);
		}
		else {
			$('#players-discovered-secrets').text($('#players-discovered-secrets').attr('none-text'));
		}
		
		if (data.faction.effects.length > 0) {
			$('#faction-effect').html(data.faction.effects.join('<hr>')).parent().show();
		}
		else {
			$('#faction-effect').text('').parent().hide();
		}
		
		if (data.faction.gm_badge) {
			$('.faction-gm-badge').show();
		}
		
		if (data.faction.affinity_div.trim() === '') {
			$('#faction-character-affinity').html('').parent().addClass('d-none');
		}
		else {
			$('#faction-character-affinity').html(data.faction.affinity_div).parent().removeClass('d-none');
			data.faction.votes.forEach(vote => {
				$('#faction-affinity-' + vote.player).val(vote.affinity);
			});
		}
		
		if (data.faction.vote_overview_div.trim() === '') {
			$('#faction-character-vote-overview').html('').parent().addClass('d-none');
		}
		else {
			$('#faction-character-vote-overview').html(data.faction.vote_overview_div).parent().removeClass('d-none');
		}
		
		$('.toggle-faction-memo-description').on('click', function() {
			$(this).closest('.flip-card').toggleClass('is-flipped');
		});
	},
	
	focus_element: function(elem) {
		var number_of_neighbors = elem.closedNeighborhood('node').length - 1;
		
		if (number_of_neighbors > 5) {
			return virtual_table_cy.center_element(elem);
		}
		
		virtual_table_cy.breadthfirst_layout = virtual_table_cy.cy.elements().layout({
			name: 'breadthfirst',
			fit: true,
			roots: elem,
			avoidOverlap: true,
			spacingFactor: 3,
			boundingBox: {
				x1: 0,
				y1: 0,
				w: 500,
				h: 300,
			}
		});
		
		virtual_table_cy.breadthfirst_layout.run();
	},
	
	info_form: function(data) {
		$('#faction-form-label-name').text(data.name);
		$('#faction-form .modal-body form').remove(); // to destroy jquery events
		$('#faction-form .modal-body').html(data.html);
		
		$('#faction-form-name').val(data.node.name);
		$('#faction-form-public-description').val(data.node.public_description);
		$('#faction-form-gm-description').val(data.node.gm_description);
		$('#faction-form-original-faction').val(data.node.faction || 0);
		$('#faction-form-pc-faction').prop('checked', data.node.pc_faction);
		$('#faction-form-pc-faction').prop('disabled', data.node.pc_faction);
		
		data.node.effects.forEach(virtual_table_cy.add_effect);
		
		$('#faction-form .modal-body form').on('submit', function(event) {
			event.preventDefault();
			virtual_table_cy.save_node();
		});
		
		$('#faction-form').modal('show');
	},
	
	init_cy: function(data) {
		// console.log(data);
		if (virtual_table_cy.cy === null) {
			
			virtual_table_cy.cy = cytoscape({
				container: document.getElementById('cy'),
				layout: {
					name: 'preset',
					row: 3,
				},
	
				style: virtual_table_cy.styles,
				
				elements: data.elements,
				
				userZoomingEnabled: false,
				userPanningEnabled: false,
			});
			
			virtual_table_cy.cy.on('tap', function(evt){
				var elem_to_center;
				
				$('#faction-relation-title').text('');
				$('#faction-public-resume').text($('#faction-public-resume').attr('unselected-text'));
				$('#players-discovered-secrets').text($('#players-discovered-secrets').attr('unselected-text'));
				$('#faction-effect').text('').parent().hide();
				$('.faction-gm-badge').hide();
				$('#faction-character-affinity').html('').parent().addClass('d-none');
				$('#faction-character-vote-overview').html('').parent().addClass('d-none');
				
				if (evt.target === virtual_table_cy.cy) {
					virtual_table_cy.cy.elements().removeClass('hidden');
					if (virtual_table_cy.pc_node) {
						elem_to_center = virtual_table_cy.cy.getElementById(virtual_table_cy.pc_node.data.id);
					}
					virtual_table_cy.focused_element = 0;
					
					if (elem_to_center) {
						virtual_table_cy.center_element(elem_to_center);
					}
					
					if (virtual_table_cy.refresh_on_next_init) {
						virtual_table_cy.refresh_on_next_init = false;
						virtual_table_cy.refresh();
					}
				}
				else {
					if (evt.target.isEdge()) {
						return;
					}
					elem_to_center = evt.target;
					virtual_table_cy.cy.elements().removeClass('hidden');
					var neighbors = elem_to_center.closedNeighborhood();
					virtual_table_cy.cy.elements().not(neighbors).addClass('hidden');
					
					virtual_table_cy.focus_element(elem_to_center);
					virtual_table_cy.focused_element = parseInt(elem_to_center.id(), 10);
					virtual_table.websocket_send(JSON.stringify({
						"command": "faction_info",
						"faction_id": parseInt(elem_to_center.id(), 10),
						"force_pc_view": $('#faction-force-pc-view').prop('checked') || false,
					}));
				}
			});
			
			if (typeof virtual_table_cy.cy.cxtmenu !== 'undefined') {
				virtual_table_cy.init_cy_menu();
			}
		}
		else {
			$('#faction-relation-title').text('');
			$('#faction-public-resume').text($('#faction-public-resume').attr('unselected-text'));
			$('#players-discovered-secrets').text($('#players-discovered-secrets').attr('unselected-text'));
			$('#faction-effect').text('').parent().hide();
			$('.faction-gm-badge').hide();
			$('#faction-character-affinity').html('').parent().addClass('d-none');
			$('#faction-character-vote-overview').html('').parent().addClass('d-none');
			
			virtual_table_cy.cy.elements().remove();
			virtual_table_cy.cy.add(data.elements);
			virtual_table_cy.focused_element = 0;
			
			if ($('#faction-force-pc-view').prop('checked')) {
				virtual_table_cy.destroy_cy_menu();
			}
			else {
				virtual_table_cy.init_cy_menu();
			}
		}
		
		virtual_table_cy.pc_node = data.elements.find(element => element.group === 'nodes' && element.data.pc_faction);
		
		
		virtual_table_cy.cy.elements().removeClass('hidden');
		if (virtual_table_cy.pc_node) {
			virtual_table_cy.center_element(virtual_table_cy.cy.getElementById(virtual_table_cy.pc_node.data.id));
		}
	},
	
	init_cy_menu: function() {
		if (virtual_table_cy.menus.core === null) {
			virtual_table_cy.menus.core = virtual_table_cy.cy.cxtmenu(virtual_table_cy.MENU_OPTIONS.CORE);
		}
		if (virtual_table_cy.menus.node === null) {
			virtual_table_cy.menus.node = virtual_table_cy.cy.cxtmenu(virtual_table_cy.MENU_OPTIONS.NODE);
		}
	},
	
	preview_logo: function() {
		var reader = new FileReader();
		
		if (typeof $('#faction-form-logo')[0].files[0] !== 'undefined') {
			reader.readAsDataURL($('#faction-form-logo')[0].files[0]);
			reader.onloadend = function() {
				$('#logo-preview').attr('src', reader.result);
				$('#faction-form-logo').get(0).setCustomValidity('');
			}
		}
		else {
			if ($('#logo-preview').attr('initial-custom-src') !== '') {
				$('#logo-preview').attr('src', $('#logo-preview').attr('initial-custom-src'));
			}
			else {
				$('#logo-preview').attr('src', $('#faction-form-original-faction>option:selected').attr('initial-logo-preview'));
			}
		}
	},
	
	refresh: function() {
		virtual_table.websocket_send(JSON.stringify({
			"command": "factions_load",
			"force_pc_view": $('#faction-force-pc-view').prop('checked') || false,
		}));
	},
	
	save_node: async function() {
		/*
		https://www.smashingmagazine.com/2018/01/drag-drop-file-uploader-vanilla-js/
		*/
		$('#faction-form form').addClass('was-validated');
		var error = !$('#faction-form form').get(0).checkValidity();
		// if (!$('#faction-form form').get(0).checkValidity()) {
		// 	return false;
		// }
		
		var file = $('#faction-form-logo')[0].files[0];
		var url = document.location.pathname.split('detail').join('logo') + virtual_table_cy.element_in_edition;
		var formData = new FormData();
		var faction = {
			"name": $('#faction-form-name').val(),
			"gm_desc": $('#faction-form-gm-description').val(),
			"public_desc": $('#faction-form-public-description').val(),
			"sync_faction": parseInt($('#faction-form-original-faction').val(), 10),
			"pc_faction": $('#faction-form-pc-faction').prop('checked'),
			"delete_logo": $('#faction-form-reset-logo').prop('checked'),
			"effects": []
		};
		
		$('#effects-form div.effect-form-row').each(function() {
			if (faction.effects.findIndex(effect => effect.level === parseInt($(this).find(':input[name=level]').val(), 10)) !== -1) {
				$(this).find(':input[name=level]').get(0).scrollIntoView();
				$(this).find(':input[name=level]').get(0).setCustomValidity("Already existing level");
				error = true;
				return false;
			}
			
			faction.effects.push({
				"id": parseInt($(this).attr('eid'), 10),
				"level": parseInt($(this).find(':input[name=level]').val(), 10),
				"level_name": $(this).find(':input[name=level_name]').val(),
				"memo": $(this).find(':input[name=level_memo]').val(),
				"effect_name": $(this).find(':input[name=effect_name]').val(),
				"effect_desc": $(this).find(':input[name=effect_description]').val(),
				"sync_effect": parseInt($(this).find(':input[name=effect]').val(), 10),
			});
		});
		
		if (error) {
			return false;
		}
		
		formData.append('faction', JSON.stringify(faction));
		formData.append('logo', file);
		
		var result = await fetch(url, {
			method: 'POST',
			headers: {'X-CSRFToken': getCookie('csrftoken')},
			mode: 'same-origin', // Do not send CSRF token to another domain.
			body: formData
		}).catch(() => { console.error('failed'); });
		
		await result.json().then((res) => {
			if (res.success) {
				$('#faction-form').modal('hide');
			}
			else {
				if (res.error.code) {
					switch (res.error.code) {
						case 'factions_update_logo_not_image':
							alert("Le logo envoyé n'est pas un fichier image valide.");
							$('#faction-form-logo').get(0).setCustomValidity("factions_update_logo_not_image");
							break;
						
						case 'factions_update_logo_bad_format':
							alert("Le logo envoyé n'est pas au bon format (PNG ou JPG).");
							$('#faction-form-logo').get(0).setCustomValidity("factions_update_logo_bad_format");
							break;
						
						case 'factions_update_logo_too_big':
							alert("Le logo envoyé est plus grand que 200 pixels de large et/ou de haut.");
							$('#faction-form-logo').get(0).setCustomValidity("factions_update_logo_too_big");
							break;
					}
				}
				else {
					console.error("Save faction error:", res.error);
				}
			}
		});
		
	},
	
	save_relations: function() {
		var faction = {
			id: virtual_table_cy.element_in_edition,
			relations: [],
			delete_relations: [],
			discovered_secrets: $('#faction-discovered-secrets').val(),
		}
		
		$('#faction-relations :checkbox[id^=toggle-relation]:checked').each(function() {
			var faction_id = parseInt($(this).attr('toggle-id'), 10);
			faction.relations.push({
				type: 'source',
				target: faction_id,
				level: parseInt($('#line-faction-source-' + faction_id + ' .slider').slider('option', 'value'), 10),
				visible: $('#line-faction-source-' + faction_id + ' .edge-visible').prop('checked'),
				effect: $('#line-faction-source-' + faction_id + ' .effect-visible').prop('checked') || false,
			});
			
			faction.relations.push({
				type: 'target',
				source: faction_id,
				level: parseInt($('#line-faction-target-' + faction_id + ' .slider').slider('option', 'value'), 10),
				visible: $('#line-faction-target-' + faction_id + ' .edge-visible').prop('checked'),
				effect: $('#line-faction-target-' + faction_id + ' .effect-visible').prop('checked') || false,
			});
			
		});
		
		$('#faction-relations :checkbox[id^=toggle-relation]:not(:checked)').each(function() {
			faction.delete_relations.push(parseInt($(this).attr('toggle-id'), 10));
		});
		
		virtual_table.websocket_send(JSON.stringify({
			"command": "faction_update_relations",
			"faction": faction,
		}));
		
		$('#faction-relations').modal('hide');
	},
	
	save_settings: function() {
		var settings = {
			"vote_active": $('#faction-settings-enable-vote-system').prop('checked'),
			"vote_blacklist": $('.faction-settings-vote-blacklist :checked').map(function() { return parseInt($(this).val(), 10); }).get(),
		};
		
		virtual_table.websocket_send(JSON.stringify({
			"command": "faction_update_settings",
			"settings": settings,
		}));
		
		$('#faction-overview').removeClass('d-none');
		$('#faction-settings').addClass('d-none');
	},
	
	relations_form: function(data) {
		$('#faction-relations-label-name').text(data.name);
		$('#faction-relations .modal-body form').remove(); // to destroy jquery events
		$('#faction-relations .modal-body').html(data.html);
		$( ".slider" ).each(function() {
			$(this).slider({
				min: -9,
				max: 9,
				value: parseInt($(this).find('.custom-handle').text(), 10),
				create: function() {
					$(this).find('.custom-handle').text( $( this ).slider( 'option', 'value' ) );
				},
				slide: function( event, ui ) {
					$(this).find('.custom-handle').text( ui.value );
				}
			});
		});
		
		$('#faction-relations').modal('show');
		return;
		$('#faction-relations tr').show();
		$('#line-faction-source-' + virtual_table_cy.element_in_edition + ',#line-faction-target-' + virtual_table_cy.element_in_edition).hide();
		$('#faction-relations :checkbox[id^=toggle-relation]').prop('checked', false);
		$('#faction-relations .faction-active').hide();
		
		if (data.faction.public_description !== '' || data.faction.gm_description !== '') {
			$('#faction-reminders').show();
			$('#collapse-reminder-gm-desc').text(data.faction.gm_description).collapse('hide');
			$('#collapse-reminder-public-desc').text(data.faction.public_description).collapse('hide');
			
			if (data.faction.public_description !== '') {
				$('#btn-collapse-reminder-public-desc').show();
			}
			else {
				$('#btn-collapse-reminder-public-desc').hide();
			}
			
			if (data.faction.gm_description !== '') {
				$('#btn-collapse-reminder-gm-desc').show();
			}
			else {
				$('#btn-collapse-reminder-gm-desc').hide();
			}
		}
		else {
			$('#faction-reminders').hide();
		}
		
		$('#faction-relations-label-name,#faction-relations .target-name').text(data.faction.name);
		$('#faction-relations > tr').each(function() {
			$(this).find('.slider').slider('option', 'value', 0);
			$(this).find('.custom-handle').text( 0 );
			$(this).find('.edge-visible').prop('checked', false);
			// Use class d-none to avoid the checkbox being shown by toggle_relation
			$(this).find('.effect-visible').prop('checked', false).addClass('d-none');
			$(this).find('.effect-description').text('');
		});
		
		$('#faction-discovered-secrets').val(data.faction.discovered_secrets);
		
		data.faction.relations.forEach(relation => {
			$('#toggle-relation-' + relation.faction).prop('checked', true);
			
			$('#line-faction-' + relation.type + '-' + relation.faction + ' .slider').slider('option', 'value', relation.level);
			$('#line-faction-' + relation.type + '-' + relation.faction + ' .custom-handle').text( relation.level );
			$('#line-faction-' + relation.type + '-' + relation.faction + ' .edge-visible').prop('checked', relation.visible_player);
			if (relation.effects.length > 0) {
				$('#line-faction-' + relation.type + '-' + relation.faction + ' .effect-visible').prop('checked', relation.visible_effect).removeClass('d-none');
				var effects = [];
				relation.effects.forEach(effect => {
					effects.push('<li class="text-' + (effect.value > 0 ? 'success' : 'danger') + '"><i class="fa-solid fa-eye faction-effect-tooltip"></i> (' + effect.value + ') ' + effect.title + '<div class="d-none effect-resume">' + (effect.effect_name !== '' ? ('<h6>' + effect.effect_name + '</h6>') : '') + (effect.description !== '' ? 'Effet (visible PJ si coché) : ' + effect.description + '<div class="text-center"><div class="knight-hr my-3 d-inline-block w-50"></div></div>' : '') + 'Memo MJ : ' + effect.memo + '</div></li>');
				});
				$('#line-faction-' + relation.type + '-' + relation.faction + ' .effect-description').html('<ul>' + effects.join('') + '</ul>');
			}
		});
		
		$('#faction-relations :checkbox[id^=toggle-relation]:checked').each(function() {
			virtual_table_cy.toggle_relation($(this));
		});
		
		$('#faction-relations').modal('show');
	},
	
	toggle_faction: function(faction_id) {
		virtual_table.websocket_send(JSON.stringify({
			"command": "factions_toggle",
			"faction_id": parseInt(faction_id, 10),
		}));
	},
	
	toggle_relation: function(node) {
		var faction_id = $(node).attr('toggle-id');
		
		if ($(node).prop('checked')) {
			$('#line-faction-source-' + faction_id + ', #line-faction-target-' + faction_id).find('.faction-active').removeClass('d-none');
		}
		else {
			$('#line-faction-source-' + faction_id + ', #line-faction-target-' + faction_id).find('.faction-active').addClass('d-none');
		}
	},
	
	toggle_settings: function() {
		$('#faction-overview').toggleClass('d-none');
		$('#faction-settings').toggleClass('d-none');
	},
};

$(function() {
	// var elements = [];
	
	// virtual_table_cy.factions.forEach(faction => {
	// 	elements.push({
	// 		group: 'nodes',
	// 		data: faction,
	// 	});
	// });
	
	// virtual_table_cy.default_relations.forEach(relation => {
	// 	elements.push({
	// 		group: 'edges',
	// 		data: relation,
	// 		classes: relation.classes,
	// 	});
	// });
	
	var level_styles = {
		"-9": "#CD1719",
		"-8": "#CD1719",
		"-7": "#CD1719",
		"-6": "#9B172F",
		"-5": "#9B172F",
		"-4": "#9B172F",
		"-3": "#9B172F",
		"-2": "#9B172F",
		"-1": "#9B172F",
		"0": "#979797",
		"1": "#283583",
		"2": "#283583",
		"3": "#283583",
		"4": "#283583",
		"5": "#283583",
		"6": "#283583",
		"7": "#009FE3",
		"8": "#009FE3",
		"9": "#009FE3",
	};
	
	for (var index_from in level_styles) {
		for (var index_to in level_styles) {
			virtual_table_cy.styles.push({
				"selector": '.from' + index_from + '-to' + index_to,
				"style": {
					'line-fill': 'linear-gradient',
					'line-gradient-stop-colors': level_styles[index_from] + ' #ccc #ccc ' + level_styles[index_to],
					// "edge-text-rotation": "autorotate",
					"source-text-rotation": "autorotate",
					"source-text-offset": "50",
					"target-text-rotation": "autorotate",
					"target-text-offset": "50",
					"color": "#123456",
					"text-background-color": "#fedcba",
					"text-background-opacity": 1,
					"text-background-padding": 2,
				}
			});
		}
	}
	
	$('#faction').on('shown.bs.modal', function() {
		if ($('#faction-force-pc-view').prop('checked')) {
			$('#faction-force-pc-view').parent().button('toggle');
		}
		virtual_table.websocket_send(JSON.stringify({
			"command": "factions_load",
		}));
	});
	
	$(document).tooltip({
		selector: '.faction-effect-tooltip',
		html: true,
		title: function() {
			return $(this).parent().find('.effect-resume').html();
		},
		placement: 'auto',
		template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner faction-tooltip"></div></div>',
		customClass: 'faction-tooltip2',
	});
});
