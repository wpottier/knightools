virtual_table_manager = {
	
	confirm_delete_vtp: function() {
		$('#id_vtp_to_del').val('');
		
		var migrate = {};
		
		$('.vtp-del:not(:checked)')
			.closest('tr')
			.each(function() {
				var migrate_table_id = parseInt($(this).find('select.migrate').val());
				
				if (typeof migrate[migrate_table_id] === 'undefined') {
					migrate[migrate_table_id] = [];
				}
				
				migrate[migrate_table_id].push(parseInt($(this).attr('vtp-id'), 10));
			});

		
		$('#id_vtp_to_migrate').val(
			JSON.stringify(migrate)
		);
		
		
		if ($('.vtp-del:checked').length > 0) {
			var liste_personnages = $('.vtp-del:checked')
				.closest('tr')
				.find('.vtp-name')
				.map(function() {
					return $(this).text()
				})
				.get()
				.join(', ');
			
			if (confirm("Voulez-vous vraiment supprimer le(s) personnage(s) suivant(s) : " + liste_personnages + "\n\nSi des comptes n'ont plus de personnages après cette opération et qu'ils sont toujours connectés à la table virtuelle, ils seront déconnectés et renvoyé à la liste des tables virtuelles.")) {
				$('#id_vtp_to_del')
					.val(
						JSON.stringify(
							$('.vtp-del:checked')
								.closest('tr')
								.map(function() {
									return parseInt($(this).attr('vtp-id'), 10);
								})
								.get()
						)
					);
			}
			else {
				return false;
			}
		}
	}
};

$(function() {
	$('form').on('submit', virtual_table_manager.confirm_delete_vtp);
	$('.knightools-mde textarea').each(function() {
		new SimpleMDE({
			element: $(this)[0],
			status: false,
			toolbar: ["bold", "italic", "strikethrough", "heading", "|",
				"quote", "unordered-list", "ordered-list",  "|",
				"link", "image", "table",  "|",
				"preview", "side-by-side", "fullscreen",  "|",
				"guide"],
		});
		
		$('.editor-toolbar').addClass('bg-white');
	});
	
	$('.vtp-del').on('change', function() {
		$(this).closest('tr').toggleClass('text-line-through text-danger');
	});
	
});