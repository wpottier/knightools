virtual_table = {
	CLASS_BY_EDIT_LVL: {
		"p": "fa-user-friends",
		"a": "fa-globe",
		"g": "fa-user-shield",
	},
	CLASS_BY_GEST_ADVANTAGE: {
		"n": "fa-ban",
		"b": "fa-balance-scale",
		"l": "fa-sun",
	},
	request_counter: 0,
	debug: false,
	avalon_fluff_displayed: false,
	
	add_progress_bar: function(node) {
		var progress_node = $(node).closest('div.calc-attribute').find('div.progress-bar');
		
		virtual_table.websocket_send(JSON.stringify({
			"command": "update",
			"subcommand": "add",
			"vtp": parseInt($(node).closest('div[data-vtp]').attr('data-vtp'), 10),
			"attr": progress_node.closest('.progress-bar-badge').find('.progress-bar-badge-label').attr('data-attr-code'),
		}));
	},
	
	add_value: function(node) {
		virtual_table.websocket_send(JSON.stringify({
			"command": "update",
			"subcommand": "add",
			"vtp": parseInt($(node).closest('div[data-vtp]').attr('data-vtp'), 10),
			"attr": $(node).closest('div[data-attr-code]').attr('data-attr-code'),
		}));
	},
	
	/**
	 * Apply CSS classes switch the expected display behavior of the vtp div
	 *
	 * @param   {div}  node  The VTP div. Must have a data-display value, having either expanded or compressed.
	 *
	 * @return  {void}
	 */
	apply_classes: function(node) {
		if (!node.is('[data-display]')) {
			return;
		}
		
		switch (node.attr('data-display')) {
			case 'expanded':
				node.removeClass('col-sm-4 col-lg-2').addClass('col-lg-6');
				node.find('i.toggle-display').removeClass('fa-expand-alt').addClass('fa-compress-alt');
				node.find('.dot').removeClass('d-lg-none d-xl-inline-block');
				node.find('.toggle-display-sensitive').removeClass('d-block d-sm-none d-xl-block');
				node.find('.toggle-display-inline-sensitive').removeClass('d-inline-block d-sm-none d-xl-inline-block');
				node.find('.result-last-test').removeClass('col-6 col-sm-12').addClass('col-6');
			break;
			
			case 'compressed':
				var editable = node.attr('data-editable') === 'True';
				
				node.find('i.toggle-display').removeClass('fa-compress-alt').addClass('fa-expand-alt');
				
				if (editable) {
					node.removeClass('col-sm-4 col-lg-2').addClass('col-lg-6');
					node.find('.dot').removeClass('d-lg-none d-xl-inline-block');
					node.find('.toggle-display-sensitive').removeClass('d-block d-sm-none d-xl-block');
					node.find('.toggle-display-inline-sensitive').removeClass('d-inline-block d-sm-none d-xl-inline-block');
					node.find('.result-last-test').removeClass('col-6 col-sm-12').addClass('col-6');
				}
				else {
					node.removeClass('col-lg-6').addClass('col-sm-4 col-lg-2');
					node.find('.dot').addClass('d-lg-none d-xl-inline-block');
					node.find('.toggle-display-sensitive').addClass('d-block d-sm-none d-xl-block');
					node.find('.toggle-display-inline-sensitive').addClass('d-inline-block d-sm-none d-xl-inline-block');
					node.find('.result-last-test').removeClass('col-6').addClass('col-6 col-sm-12');
				}
			break;
		}
		
		// Strangely, using hide and show instead of addClass and removeClass break the ease transition on progressbar... So...
		node.find('.customizable-display').addClass('d-none');
		node.find(node.attr('data-classes-to-show')).removeClass('d-none');
	},
	
	avalon_apply_fluff: function() {
		if (virtual_table.avalon_fluff_displayed) {
			$('#avalon-vote .avalon-extension-fluff').show();
			$('#avalon-toggle-fluff i').removeClass('fa-eye-slash fa-eye').addClass('fa-eye-slash');
			$('#avalon-toggle-fluff span').text($('#avalon-toggle-fluff').attr('displayed-text'));
		}
		else {
			$('#avalon-vote .avalon-extension-fluff').hide();
			$('#avalon-toggle-fluff i').removeClass('fa-eye-slash fa-eye').addClass('fa-eye');
			$('#avalon-toggle-fluff span').text($('#avalon-toggle-fluff').attr('hidden-text'));
		}
	},
	
	avalon_hide_construction_points_form: function() {
		$('#construction-points-span').show();
		$('#construction-points-form').hide();
	},
	
	avalon_move_extension_down: function(node) {
		var next_tr = $(node).closest('tr').next();
		if (next_tr.length > 0) {
			next_tr.after($(node).closest('tr'));
			$(node).closest('tr').trigger('sortupdate');
		}
	},
	
	avalon_move_extension_up: function(node) {
		var prev_tr = $(node).closest('tr').prev();
		if (prev_tr.length > 0) {
			prev_tr.before($(node).closest('tr'));
			$(node).closest('tr').trigger('sortupdate');
		}
	},
	
	avalon_show_construction_points_form: function() {
		$('#construction-points-span').hide();
		$('#construction-points-form').show();
		$('#construction-points-input').val($('#construction-points-span .total').text());
	},
	
	avalon_update_construction_points: function() {
		virtual_table.websocket_send(JSON.stringify({
			"command": "avalon_update_points",
			"points": parseInt($('#construction-points-input').val(), 10),
		}));
	},
	
	avalon_toggle_fluff: function() {
		virtual_table.avalon_fluff_displayed = !virtual_table.avalon_fluff_displayed;
		virtual_table.avalon_apply_fluff();
	},
	
	calc_progress_bar: function() {
		var max = $(this).attr('aria-valuemax');
		var percent = 0;
		if (max > 0) {
			percent = ($(this).attr('aria-valuenow') / (max - $(this).attr('aria-valuemin'))) * 100;
		}
		
		$(this).css('width', percent+'%');
		$(this).closest('.progress-bar-badge').find('.progress-bar-badge-label').text($(this).attr('aria-valuenow') + ' / ' + max);
	},
	
	calc_progress_bars: function() {
		$('div.progress-bar').each(virtual_table.calc_progress_bar);
	},
	
	change_end_mission_checkboxes: function(checked) {
		$('.end-mission-checkbox').prop('checked', checked);
	},
	
	change_name: function(e) {
		var form = $('#updateName .modal-body form');
		var name = $('#vtp-name-value').val();
		
		if (name.trim() === '') {
			// alert("Le nom doit au moins contenir un caractère autre qu'espace.");
			$('#vtp-name-value').addClass('is-invalid').one('input', function() {
				$(this).removeClass('is-invalid');
			});
			return false;
		}
		
		virtual_table.websocket_send(JSON.stringify({
			"command": "update",
			"subcommand": "edit",
			"attr": "name",
			"name": name,
			"vtp": parseInt(form.attr('data-vtp'), 10),
		}));
		
		$('#updateName').modal('hide');
		$('#vtp-name-value').val('');
		
		e.preventDefault();
		return false;
	},
	
	change_rights: function(node) {
		virtual_table.websocket_send(JSON.stringify({
			"command": "toggle",
			"attr": "rights",
			"vtp": parseInt($(node).closest('div[data-vtp]').attr('data-vtp'), 10),
			"value": $(node).attr('data-value'),
		}));
	},
	
	check_order_vtp: function() {
		var player_list = [];
		var actual_order = [];
		$('div[data-vtp]').each(function() {
			actual_order.push({
				id: $(this).attr('data-vtp'),
				editable: $(this).attr('data-editable') === 'True' ? 1 : 0,
				expanded: $(this).attr('data-display') === 'expanded' ? 1 : 0,
				name: $(this).find('.character-name').text().trim(),
			});
			
			player_list.push({
				id: $(this).attr('data-vtp'),
				editable: $(this).attr('data-editable') === 'True' ? 1 : 0,
				expanded: $(this).attr('data-display') === 'expanded' ? 1 : 0,
				name: $(this).find('.character-name').text().trim(),
			});
		});
		
		player_list.sort(function(a, b) {
			if (a.editable > b.editable) {
				return -1;
			}
			
			if (a.editable < b.editable) {
				return 1;
			}
			
			if (a.expanded > b.expanded) {
				return -1;
			}
			
			if (a.expanded < b.expanded) {
				return 1;
			}
			
			if (a.name > b.name) {
				return 1;
			}
			
			if (a.name < b.name) {
				return -1;
			}
			
			return 0;
		});
		
		// console.log(player_list, actual_order);
		
		if (JSON.stringify(player_list) === JSON.stringify(actual_order)) {
			return;
		}
		
		// If we're here, a new order has been set, either by expanding or compressing a player, or the editable value has been changed by the gm
		player_list.forEach(function(player) {
			$('#vtp-list').append($('div[data-vtp='+player.id+']'));
		});
	},
	
	config_vtp: function(e) {
		var form = $('#config-vtp .modal-body form');
		var classes_to_show = [];
		var vtp = parseInt(form.attr('data-vtp'), 10);
		
		$('#config-vtp :checkbox.display-management:checked').each(function() {
			classes_to_show.push($(this).val());
		});
		
		if ($('#armour-bar').val() !== '') {
			classes_to_show.push($('#armour-bar').val());
		}
		
		virtual_table.websocket_send(JSON.stringify({
			"command": "config",
			"classes_to_show": classes_to_show.join(','),
			"vtp": vtp,
			"attr": virtual_table.attr_code,
		}));
		
		if ($('#ignore-health-loss-by-armour').length > 0) {
			virtual_table.websocket_send(JSON.stringify({
				"command": "toggle",
				"attr": "ignore_health_loss",
				"vtp": vtp,
				"value": $('#ignore-health-loss-by-armour').prop('checked')
			}));
		}
		
		if ($('#edit-level').length > 0) {
			virtual_table.websocket_send(JSON.stringify({
				"command": "toggle",
				"attr": "rights",
				"vtp": vtp,
				"value": $('#edit-level').val(),
			}));
		}
		
		if ($('#gest-advantage').length > 0) {
			virtual_table.websocket_send(JSON.stringify({
				"command": "toggle",
				"attr": "gest_advantage",
				"vtp": vtp,
				"value": $('#gest-advantage').val(),
			}));
		}
		
		$('#config-vtp').modal('hide');
		
		e.preventDefault();
		return false;
	},
	
	destiny_state: function() {
		virtual_table.websocket_send(JSON.stringify({
			"command": "destiny_state",
			"val": $('#destiny-state').val(),
		}));
	},
	
	draw_destiny: function(node, gm_roll) {
		virtual_table.websocket_send(JSON.stringify({
			"command": "destiny_card",
			"vtp": parseInt($(node).closest('div[data-vtp]').attr('data-vtp'), 10),
		}));
	},
	
	end_battle: function() {
		virtual_table.websocket_send(JSON.stringify({
			"command": "end_battle",
		}));
	},
	
	end_mission: function(e) {
		var rewards = {};
		$('#rewards-players>tr').each(function() {
			var gp = parseInt($(this).find('input[data-source=reward-gp]').val(), 10);
			var xp = parseInt($(this).find('input[data-source=reward-xp]').val(), 10);
			var hep = parseInt($(this).find('input[data-source=reward-hep]').val(), 10);
			
			if (isNaN(gp)) {
				gp = 0;
			}
			if (isNaN(xp)) {
				xp = 0;
			}
			if (isNaN(hep)) {
				hep = 0;
			}
			
			rewards[parseInt($(this).attr('data-vtp'), 10)] = {
				"gp": gp,
				"xp": xp,
				"hep": hep,
			};
		});
		
		virtual_table.websocket_send(JSON.stringify({
			"command": "end_mission",
			"rewards": rewards,
			"restore_reset": $('.end-mission-checkbox:checked').map(function() { return $(this).val(); }).get(),
		}));
		
		$('#rewards').modal('hide');
		
		e.preventDefault();
		return false;
	},
	
	get_modal_title: function() {
		switch (virtual_table.attr_code) {
			case 'hp': return 'Modifier PS';
			case 'hn': return 'Modifier nods de soin';
			case 'ap': return 'Modifier PA';
			case 'an': return "Modifier nods d'armure";
			case 'ep': return 'Modifier PE';
			case 'en': return "Modifier nods d'énergie";
			case 'hop': return 'Modifier PES';
			case 'gs': return 'Modifier grenades';
			case 'ff': return 'Modifier CdF';
			case 'ae': return 'Modifier Égide';
			case 'hep': return 'Modifier Héroïsme';
			case 'flux': return 'Modifier Flux';
			case 'pard': return 'Modifier Paradoxe temporel';
			case 'imp': return 'Modifier Imprégnation (jauge)';
			case 'impd': return 'Modifier Imprégnation (diff.)';
			case 'gp': return 'Modifier PG';
			case 'xp': return 'Modifier PX';
		}
		
		return 'Modifier';
	},
	
	hurt: function() {
		var damage = parseInt($('#damage-value').val(), 10);
		var ignore_ff = $('#damage-ignore-ff').prop('checked');
		var ignore_armour = $('#damage-ignore-armour').prop('checked');
		var aegis_active = $('#damage-aegis-active').prop('checked');
		var recoverable = $('#damage-recoverable').prop('checked');
		var armour_piercing = parseInt($('#damage-armour-piercing').val(), 10);
		var penetrating = parseInt($('#damage-penetrating').val(), 10);
		var target = $('#damage-target').val();
		var vtps = [];
		
		if (isNaN(damage)) {
			alert('Les dégâts doivent être un nombre entier');
			return false;
		}
		
		if (isNaN(armour_piercing)) {
			armour_piercing = null;
		}
		
		if (isNaN(penetrating)) {
			penetrating = null;
		}
		
		$('button.damage-vtp').each(function() {
			if (!$(this).is('.btn-danger')) {
				return;
			}
			
			var vtp = parseInt($(this).closest('div[data-vtp]').attr('data-vtp'), 10);
			if (isNaN(vtp)) {
				return;
			}
			
			vtps.push(vtp);
		});
		
		if (vtps.length === 0) {
			alert("Aucun personnage n'a été ciblé");
			return false;
		}
		
		virtual_table.websocket_send(JSON.stringify({
			"command": "hurt",
			"value": damage,
			"ignore-ff": ignore_ff,
			"ignore-armour": ignore_armour,
			"armour-piercing": armour_piercing,
			"penetrating": penetrating,
			"aegis-active": aegis_active,
			"recoverable": recoverable,
			"target": target,
			"vtps": vtps,
		}));
	},
	
	init_sortable: function() {
		$('.sortable').sortable({
			items: ':not(.drag-disabled)',
			handle: '.fa-grip-lines',
			forcePlaceholderSize: true ,
		}).bind('sortupdate', function(e, ui) {
			var new_ordered_extensions = [];
			$('.sortable tr:not(.drag-disabled)').each(function() {
				new_ordered_extensions.push(parseInt($(this).attr('data-vtaeid'), 10));
			});
			
			virtual_table.websocket_send(JSON.stringify({
				"command": "avalon_vote",
				"new_ordered_extensions": new_ordered_extensions,
			}));
		});
		
		virtual_table.avalon_apply_fluff();
	},
	
	init_ws: function() {
		var vt_id = parseInt(parseInt($('h1[data-vt]').attr('data-vt'), 10));
		
		if (isNaN(vt_id)) {
			return false;
		}
		
		var prot = location.protocol.split(':')[0] === 'https' ? 'wss' : 'ws';
		
		virtual_table.websocket = new WebSocket(
			prot + '://' + window.location.host +
			'/ws/vt/' + vt_id + '/');
		
		virtual_table.websocket.onmessage = function(e) {
			var data = JSON.parse(e.data);
			if (virtual_table.debug) {
				console.log(data);
			}
			
			if (data.error) {
				var code = data.error.code;
				if (code === 'unauthorized_user_vtp') {
					alert("Vous n'avez pas les droits de modifications sur ce personnage.");
				}
				console.error(data.error);
				return;
			}
			
			switch(data['command']) {
				case 'refresh':
					virtual_table.refresh_vtp(data);
					break;
				
				case 'join':
					// Set the flag offline
					$('.dot').removeClass('green').addClass('grey');
					virtual_table.websocket_send(JSON.stringify({
						"command": "status",
						"counter": ++virtual_table.request_counter,
					}));
					break;
				
				case 'quit':
					// Set the flag online
					$('.dot').removeClass('green').addClass('grey');
					virtual_table.websocket_send(JSON.stringify({
						"command": "status",
						"counter": ++virtual_table.request_counter,
					}));
					break;
				
				case 'status':
					data.vtp.forEach(nVtp => {
						$('div[data-vtp='+nVtp+'] .dot').removeClass('grey').addClass('green');
					});
					break;
				
				case 'refresh_avalon':
					var active_tab_id = $('#avalon .tab-pane.active').attr('id');
					$('#avalon .modal-body').html($(data.html));
					$('#' + active_tab_id + '-tab').tab('show');
					virtual_table.init_sortable();
					break;
				
				case 'new':
					// Create a new VTP? May need an ajax request for it
					$('#vtp-list').append($(data.html));
					virtual_table.refresh_vtp(data);
					break;
				
				case 'del':
					if (data.new_url) {
						document.location = data.new_url;
					}
					else {
						data.vtp.forEach(nVtp => {
							$('div[data-vtp='+nVtp+']').remove();
						});
					}
					break;
				
				case 'kick':
					if (data.new_url) {
						document.location = data.new_url;
					}
					break;
				
				case 'consent':
					virtual_table.refresh_consent('#unsafe-topics', data.unsafe_topics);
					virtual_table.refresh_consent('#unknown-topics', data.unknown_topics);
					virtual_table.refresh_consent('#safe-topics', data.safe_topics);
					break;
				
				case 'description':
					$('#vt-informations-description').html(data.description);
					break;
				
				case 'combo_result':
					virtual_table.process_combo_result(data, $('div[data-vtp='+data.vtp.id+'] .result-last-test'), false, false);
					break;
				
				case 'combo_gm_result':
					virtual_table.process_combo_result(data, $('#damage-manager .result-last-test'), true, true);
					if (data.public) {
						$('div[data-vtp]').each(function() {
							data.vtp = {
								id: parseInt($(this).attr('data-vtp'), 10),
							}
							virtual_table.process_combo_result(data, $(this).find('.result-last-test'), true, false);
						});
					}
					break;
				
				case 'damage_result':
					virtual_table.process_damage_result(data, $('div[data-vtp='+data.vtp.id+'] .result-last-test'), false, false);
					break;
				
				case 'damage_gm_result':
					virtual_table.process_damage_result(data, $('#damage-manager .result-last-test'), true, true);
					if (data.public) {
						$('div[data-vtp]').each(function() {
							data.vtp = {
								id: parseInt($(this).attr('data-vtp'), 10),
							}
							virtual_table.process_damage_result(data, $(this).find('.result-last-test'), true, false);
						});
					}
					break;
				
				case 'destiny_result':
					virtual_table.process_destiny_result(data);
					break;
				
				case 'refresh_factions':
					virtual_table_cy.init_cy(data);
					break;
				
				case 'faction_relations':
					virtual_table_cy.relations_form(data);
					break;
				
				case 'faction_form':
					virtual_table_cy.info_form(data);
					break;
				
				case 'faction_info':
					virtual_table_cy.display_faction(data);
					break;
			}
		};
	
		virtual_table.websocket.onclose = function(e) {
			console.error('Chat socket closed unexpectedly', e);
		};
		
		// document.querySelector('#chat-message-input').focus();
		// document.querySelector('#chat-message-input').onkeyup = function(e) {
		// 	if (e.keyCode === 13) {  // enter, return
		// 		document.querySelector('#chat-message-submit').click();
		// 	}
		// };
	
		// document.querySelector('#chat-message-submit').onclick = function(e) {
		// 	var messageInputDom = document.querySelector('#chat-message-input');
		// 	var message = messageInputDom.value;
		// 	chatSocket.send(JSON.stringify({
		// 		'message': message
		// 	}));
	
		// 	messageInputDom.value = '';
		// };
	
	},
	
	kill_npc: function() {
		var vtps = [];
		
		$('button.damage-vtp').each(function() {
			if (!$(this).is('.btn-danger')) {
				return;
			}
			
			var vtp = parseInt($(this).closest('div[data-vtp]').attr('data-vtp'), 10);
			if (isNaN(vtp)) {
				return;
			}
			
			vtps.push(vtp);
		});
		
		if (vtps.length === 0) {
			alert("Aucun personnage n'a été ciblé");
			return false;
		}
		
		virtual_table.websocket_send(JSON.stringify({
			"command": "recover",
			"vtps": vtps,
		}));
	},
	
	notify: function(notif_content, notif_icon, notif_tag) {
		options = {
			icon: '/static/virtualtable/img/' + notif_icon,
			tag: notif_tag,
			title: $('.navbar-brand').text() + " VirtualTable"
		};
		
		// Let's check if the browser supports notifications
		if (!("Notification" in window)) {
			console.error("This browser does not support desktop notification");
		}
		
		// Let's check whether notification permissions have already been granted
		else if (Notification.permission === "granted") {
			// If it's okay let's create a notification
			var notif = new Notification(notif_content, options);
			notif.onclick = function(event) {
				// Oddly, the defautl click on Brave open a new knightools window, on the index of the site. Strange.
				window.focus(); // XXX Use a serviceWorker to create actions and allow to focus or just close the notification more easily
				event.preventDefault();
				this.close();
			}
			notif.onshow = function(event) {
				setTimeout(function() {
					notif.close();
				}, 7500); // 7.5s to read the result is long enough. Tried at 5s, was too quick.
			}
		}
	  
		// Otherwise, we need to ask the user for permission
		else if (Notification.permission !== "denied") {
			Notification.requestPermission().then(function (permission) {
				// If the user accepts, let's create a notification
				if (permission === "granted") {
					var notif = new Notification(notif_content, options);
					notif.onclick = function(event) {
						// Oddly, the defautl click on Brave open a new knightools window, on the index of the site. Strange.
						window.focus(); // XXX Use a serviceWorker to create actions and allow to focus or just close the notification more easily
						event.preventDefault();
						this.close();
					}
					notif.onshow = function(event) {
						setTimeout(function() {
							notif.close();
						}, 7500); // 7.5s to read the result is long enough. Tried at 5s, was too quick.
					}
				}
			});
		}
	},
	
	open_reward: function() {
		$('#rewards-players').empty();
		$('#reward-gp,#reward-xp,#reward-hep').val('0');
		$('#vtp-list>div[data-vtp]').each(function() {
			var template = document.importNode(document.querySelector('#reward-template').content, true);
			
			template.querySelector('tr').setAttribute('data-vtp', $(this).attr('data-vtp'));
			template.querySelector('.name').textContent = $(this).find('span.character-name').text().trim();
			
			document.querySelector('#rewards-players').appendChild(template);
		});
		
		$('#rewards').modal('show');
	},
	
	process_combo_result: function(data, div_result, gm_roll, gm_target) {
		var html = '';
		var combo = '';
		var notif_content = '';
		var notif_icon = "notif-combo.png";
		var notif_tag = "vtp" + (data.vtp ? data.vtp.id : '-gm');
		
		div_result.find('.special-effect').hide().removeClass('d-none');
		// console.log(div_result);
		
		if (gm_roll) {
			notif_content = 'MJ : ';
			notif_tag = "gm";
		}
		else {
			notif_content = $(div_result).closest('.card').find('.character-name').text().trim() + ' : ';
		}
		
		if (data.response.dice > 0) {
			combo = '[' + data.response.dice + 'D6' + (data.response.od > 0 ? (' + ' + data.response.od + 'OD') : '') + ']';
			
			if (data.response.fail) {
				div_result.find('.fail').show();
				notif_content+= div_result.find('.fail').text()+ ' ';
				notif_icon = "notif-fail.png";
			}
			else {
				
				if (data.response.exploit) {
					div_result.find('.exploit').show();
					notif_content+= div_result.find('.exploit').text()+ ' ';
					notif_icon = "notif-exploit.png";
				}
				else {
					div_result.find('.combo').show();
					if (data.response.support) {
						div_result.find('.combo').find('.fas').removeClass('fa-dice fa-people-carry').addClass('fa-people-carry');
					}
					else {
						div_result.find('.combo').find('.fas').removeClass('fa-dice fa-people-carry').addClass('fa-dice');
					}
				}
				html = (data.response.support ? '+' : '') + data.response.success + ' succès : ';
				notif_content+= (data.response.support ? '+' : '') + data.response.success + ' succès';
			}
			
			var dice_values = [2,4,6,1,3,5];
			for (var value = 0; value<6; value++) {
				if (dice_values[value] === 1 && html !== '') {
					html += '<span class="mx-1"></span>';
				}
				for (var i=data.response.detail[dice_values[value]].count; i--; ) {
					var color = dice_values[value] % 2 ? 'text-danger' : 'text-success';
					// console.log(dice_values[value], data.response.detail[dice_values[value]].count, data.response.detail[dice_values[value]].class);
					html+= '<i class="' + color + ' fas fa-' + data.response.detail[dice_values[value]].class + '"></i>';
				}
			}
		
			if (div_result.closest('.card-body').find('.roll-cmb').length === 0 && (!gm_roll || gm_target)) {
				// console.log('notification', notif_content);
				virtual_table.notify(notif_content, notif_icon, notif_tag);
			}
		}
		div_result.find('.test-combo').text(combo);
		div_result.find('.test-result').html(html);
		
		if (data.response.dice > 0) {
			var date = new Date();
			var button = '';
			if (div_result.closest('.card-body').find('.roll-cmb').length > 0) {
				var roll_type = data.response.support ? 'roll-spr' : 'roll-cmb';
				button = '<button class="btn btn-sm btn-outline-success mr-1 redo-roll" ' + (gm_roll && !gm_target ? 'disabled' : '') + ' data-roll-type="' + roll_type + '" data-die="' + data.response.dice + '" data-bonus="' + data.response.od + '" data-public="' + JSON.stringify(data.public) + '" type="button"><i class="fas fa-' + (gm_roll && !gm_target ? 'times' : 'redo-alt') + '"></i></button>';
			}
			button += '<span class="mx-2"><i class="fas fa-' + (gm_roll && !gm_target ? 'book-reader' : 'share-alt' ) + ' text-' + (data.public ? 'primary' : 'secondary') + '"></i></span>';
			$('#result-history'+(data.vtp ? data.vtp.id : '-gm')+' .modal-body .history-results')
				.prepend(
					div_result
						.clone()
						.removeClass()
						.prepend(
							button
							+ '<span class="mr-2">'
							+ date.toLocaleTimeString()
							+ date.toISOString().substr(19, 4)
							+ '</span>'
						)
				);
		}
		
		// console.log(data);
	},
	
	process_damage_result: function(data, div_result, gm_roll, gm_target) {
		var html = '';
		var combo = '';
		var notif_content = '';
		var notif_icon = 'notif-damage.png';
		var notif_tag = "vtp" + (data.vtp ? data.vtp.id : '-gm');
		
		div_result.find('.special-effect').hide().removeClass('d-none');
		
		if (gm_roll) {
			notif_content = 'MJ : ';
		}
		else {
			notif_content = $(div_result).closest('.card').find('.character-name').text().trim() + ' : ';
		}
		
		if (data.response.dice !== 0 || data.response.bonus !== 0) {
			if (data.response.dice > 0) {
				var sign = data.response.bonus > 0 ? '+' : '';
				combo = ['[', data.response.dice, 'D6', (data.response.bonus !== 0 ? (sign + data.response.bonus) : ''), ']'].join('');
			}
			else {
				combo = '[' + data.response.bonus + ']';
			}
			
			div_result.find('.damage').show();
			html = (data.response.damage > 0 ? data.response.damage : 0) + ' ' + (typeof data.response.stronger_damage !== 'undefined' && data.response.stronger_damage !== data.response.damage ? (' (' + data.response.stronger_damage + ') ') : '');
			notif_content+= (data.response.damage > 0 ? data.response.damage : 0) + ' ' + (typeof data.response.stronger_damage !== 'undefined' && data.response.stronger_damage !== data.response.damage ? (' (' + data.response.stronger_damage + ') ') : '');
			
			var dice_values = [6,5,4,3,2,1];
			for (var value = 0; value<6; value++) {
				for (var i=data.response.detail[dice_values[value]].count; i--; ) {
					// console.log(dice_values[value], data.response.detail[dice_values[value]].count, data.response.detail[dice_values[value]].class);
					html+= '<i class="fas fa-' + data.response.detail[dice_values[value]].class + '"></i>';
				}
			}
		
			if (div_result.closest('.card-body').find('.roll-cmb').length === 0 && (!gm_roll || gm_target)) {
				// console.log('notification', notif_content);
				virtual_table.notify(notif_content, notif_icon, notif_tag);
			}
		}
		div_result.find('.test-combo').text(combo);
		div_result.find('.test-result').html(html);
		
		if (data.response.dice !== 0 || data.response.bonus !== 0) {
			var date = new Date();
			var button = '';
			if (div_result.closest('.card-body').find('.roll-dmg').length > 0) {
				button = '<button class="btn btn-sm btn-outline-success mr-1 redo-roll" ' + (gm_roll && !gm_target ? 'disabled' : '') + ' data-roll-type="roll-dmg" data-die="' + data.response.dice + '" data-bonus="' + data.response.bonus + '" data-public="' + JSON.stringify(data.public) + '" type="button"><i class="fas fa-' + (gm_roll && !gm_target ? 'times' : 'redo-alt') + '"></i></button>';
			}
			button += '<span class="mx-2"><i class="fas fa-' + (gm_roll && !gm_target ? 'book-reader' : 'share-alt' ) + ' text-' + (data.public ? 'primary' : 'secondary') + '"></i></span>';
			$('#result-history'+(data.vtp ? data.vtp.id : '-gm')+' .modal-body .history-results')
				.prepend(
					div_result
						.clone()
						.removeClass()
						.prepend(
							button
							+ '<span class="mr-2">'
							+ date.toLocaleTimeString()
							+ date.toISOString().substr(19, 4)
							+ '</span>'
						)
				);
		}
		
		// console.log(data);
	},
	
	process_destiny_result: function(data) {
		var html = '';
		var combo = 'Arcane :';
		var div_result = $('div[data-vtp='+data.vtp.id+'] .result-last-test');
		var notif_content = $(div_result).closest('.card').find('.character-name').text().trim() + ' : ' + data.arcana.name + (data.arcana.roman_number !== '' ? ' (' + data.arcana.roman_number + ')' : '');
		var notif_icon = 'notif-destiny.png';
		var notif_tag = "vtp" + (data.vtp ? data.vtp.id : '-gm');
		
		div_result.find('.special-effect').hide().removeClass('d-none');
		
		html = '<span>' + data.arcana.name + (data.arcana.roman_number !== '' ? ' (' + data.arcana.roman_number + ')' : '') + '</span><div><small><button role="button" class="btn btn-sm btn-link" data-toggle="tooltip" data-html="true" data-placement="top" title="TODO">Voir le texte...</button></small></div>';
		
		if (div_result.closest('.card-body').find('.roll-cmb').length === 0) {
			virtual_table.notify(notif_content, notif_icon, notif_tag);
		}
		
		div_result.find('.test-combo').text(combo);
		div_result.find('.test-result').html(html);
		
		div_result.find('.test-result').find('button').attr('title', data.arcana.destiny_effect);
		div_result.find('.test-result').find('button').tooltip();
	},
	
	refresh_consent: function(node, topics) {
		$(node).empty();
		
		topics.forEach(topic => {
			$(node).append(
				'<li>' +
					(topic.comment ?
					('<i class="fa-solid fa-comments consent-comment"><span class="d-none">' + topic.comment + '</span></i> ') :
					'') +
					'<span>' + topic.name + '</span>' +
				'</li>'
			);
		});
	},
	
	refresh_vtp: function(data) {
		var vtp = $('div[data-vtp='+data.vtp.id+']');
		vtp.find('.progress-bar').each(function() {
			var target = $(this).attr('data-target');
			var current = data.vtp['current_'+target];
			var max = data.vtp['max_'+target];
			var badge = $(this).closest('div.progress-bar-badge').find('.badge');
			
			if (typeof data.vtp['max_'+target] === 'undefined') {
				max = data.vtp['total_'+target];
			}
			
			if (max !== null) {
				if (badge.is('.badge-warning')) {
					badge.removeClass('badge-warning').addClass('badge-light progress-bar-badge-label');
				}
			}
			else {
				if (badge.is('.badge-light')) {
					badge.addClass('badge-warning').removeClass('badge-light progress-bar-badge-label').text('à configurer');
				}
				max = 0;
				current = 0;
			}
			
			$(this).attr('aria-valuenow', current);
			$(this).attr('aria-valuemax', max);
			
			if ($(this).closest('div.calc-attribute').find('.recoverable-value').length > 0) {
				var recover_attr = $(this).closest('div.calc-attribute').find('.recoverable-value').attr('data-recover-target');
				$(this).closest('div.calc-attribute').find('.recoverable-value').text(data.vtp['lost_' + recover_attr + '_counter']);
			}
			
			$(this).each(virtual_table.calc_progress_bar);
		});
		
		vtp.find('.force-field .badge-label').each(function() {
			var current = data.vtp.current_force_field;
			var def = data.vtp.default_force_field;
			if (current !== null) {
				$(this).text(current);
			}
			else {
				if (def !== null) {
					$(this).text(def);
				}
				else {
					$(this).text(0);
				}
			}
			
			if (def !== null) {
				$(this).attr('default-value', def);
			}
		});
		
		vtp.find('.aegis .badge-label').each(function() {
			var current = data.vtp.current_aegis;
			var def = data.vtp.default_aegis;
			if (current !== null) {
				$(this).text(current);
			}
			else {
				if (def !== null) {
					$(this).text(def);
				}
				else {
					$(this).text(0);
				}
			}
			
			if (def !== null) {
				$(this).attr('default-value', def);
			}
		});
		
		vtp.find('.paradox-diff .badge-label').each(function() {
			var current = data.vtp.time_paradox_difficulty;
			var def = 0;
			if (current !== null) {
				$(this).text(current);
			}
			else {
				if (def !== null) {
					$(this).text(def);
				}
				else {
					$(this).text(0);
				}
			}
			
			if (def !== null) {
				$(this).attr('default-value', def);
			}
		});
		
		vtp.find('.imp-diff .badge-label').each(function() {
			var current = data.vtp.impregnation_difficulty;
			var def = 0;
			if (current !== null) {
				$(this).text(current);
			}
			else {
				if (def !== null) {
					$(this).text(def);
				}
				else {
					$(this).text(0);
				}
			}
			
			if (def !== null) {
				$(this).attr('default-value', def);
			}
		});
		
		if (vtp.find('.character-name a').length > 0) {
			vtp.find('.character-name a').text(data.vtp.name);
		}
		else {
			vtp.find('.character-name').text(data.vtp.name);
		}
		
		vtp.attr('data-classes-to-show', data.vtp.classes_to_show);
		vtp.attr('data-edit-level', data.vtp.edit_level);
		vtp.attr('data-gest-advantage', data.vtp.gest_advantage);
		vtp.attr('data-ignore-health-loss', data.vtp.ignore_health_loss_by_armour ? 'True' : 'False');
		
		if (data.editable_by_user) {
			vtp.find('button.manage-player').removeClass('disabled').parent().show();
			vtp.find('a.manage-player').show();
			vtp.find('.stat-value').removeClass('col-9').addClass('col-6');
			vtp.attr('data-editable', 'True');
		}
		else {
			vtp.find('button.manage-player').addClass('disabled').parent().hide();
			vtp.find('a.manage-player').hide();
			vtp.find('.stat-value').removeClass('col-6').addClass('col-9');
			vtp.attr('data-editable', 'False');
		}
		virtual_table.apply_classes(vtp);
		virtual_table.check_order_vtp();
	},
	
	reset_damage_form: function() {
		$('#damage-value,#damage-armour-piercing,#damage-penetrating,#gm-die-number,#gm-bonus-number').val('');
		$('#damage-ignore-ff,#damage-ignore-armour,#damage-aegis-active,#damage-recoverable').prop('checked', false);
		$('#damage-target').val('hp');
		
		$('button.damage-vtp').removeClass('btn-danger').addClass('btn-secondary');
	},
	
	reset_value: function(node, event) {
		virtual_table.websocket_send(JSON.stringify({
			"command": "update",
			"subcommand": "reset",
			"vtp": parseInt($(node).closest('div[data-vtp]').attr('data-vtp'), 10),
			"attr": $(node).closest('div[data-attr-code]').attr('data-attr-code'),
		}));
		
		event.preventDefault();
		return false;
	},
	
	reset_votes: function() {
		virtual_table.websocket_send(JSON.stringify({
			"command": "avalon_reset_votes",
		}));
	},
	
	roll_combo: function(node, gm_roll) {
		var dice = parseInt($(node).closest('div.row').find(':input.die-number').val(), 10);
		var od = parseInt($(node).closest('div.row').find(':input.bonus-number').val(), 10);
		var public_button = $(node).closest('div.row').find('.btn-share');
		var public_checked = public_button.is('.active');
		
		virtual_table.websocket_send(JSON.stringify({
			"command": "combo",
			"dice_number": dice,
			"overdrive_value": od,
			"support": false,
			"vtp": parseInt($(node).closest('div[data-vtp]').attr('data-vtp'), 10),
			"gm_roll": gm_roll || false,
			"public": public_checked,
		}));
		
		if ((gm_roll && public_checked) || (!gm_roll && !public_checked)) {
			public_button.button('toggle');
		}
	},
	
	roll_damage: function(node, gm_roll) {
		var dice = parseInt($(node).closest('div.row').find(':input.die-number').val(), 10);
		var bonus = parseInt($(node).closest('div.row').find(':input.bonus-number').val(), 10);
		var public_button = $(node).closest('div.row').find('.btn-share');
		var public_checked = public_button.is('.active');
		
		virtual_table.websocket_send(JSON.stringify({
			"command": "damage",
			"dice_number": dice,
			"bonus_value": bonus,
			"vtp": parseInt($(node).closest('div[data-vtp]').attr('data-vtp'), 10),
			"gm_roll": gm_roll || false,
			"public": public_checked,
		}));
		
		if ((gm_roll && public_checked) || (!gm_roll && !public_checked)) {
			public_button.button('toggle');
		}
	},
	
	roll_support: function(node, gm_roll) {
		var dice = parseInt($(node).closest('div.row').find(':input.die-number').val(), 10);
		var od = 0;
		var public_button = $(node).closest('div.row').find('.btn-share');
		var public_checked = public_button.is('.active');
		
		virtual_table.websocket_send(JSON.stringify({
			"command": "combo",
			"dice_number": dice,
			"overdrive_value": od,
			"support": true,
			"vtp": parseInt($(node).closest('div[data-vtp]').attr('data-vtp'), 10),
			"gm_roll": gm_roll || false,
			"public": public_checked,
		}));
		
		if ((gm_roll && public_checked) || (!gm_roll && !public_checked)) {
			public_button.button('toggle');
		}
	},
	
	save: function(e) {
		var form = $('#updateValues .modal-body form');
		var current = parseInt($('#current-value').val(), 10);
		var current_add = parseInt($('#current-value-add').val(), 10);
		var max = parseInt($('#max-value').val(), 10);
		var recoverable = parseInt($('#recoverable-value').val(), 10);
		
		if (isNaN(current)) {
			current = undefined;
		}
		
		if (isNaN(current_add)) {
			current_add = undefined;
		}
		
		if (isNaN(max)) {
			max = undefined;
		}
		
		if (isNaN(recoverable) || $('#recoverable-value').is(':not(:visible)')) {
			recoverable = undefined;
		}
		
		virtual_table.websocket_send(JSON.stringify({
			"command": "update",
			"subcommand": "edit",
			"current": current,
			"current_add": current_add,
			"max": max,
			"recoverable": recoverable,
			"vtp": parseInt(form.attr('data-vtp'), 10),
			"attr": virtual_table.attr_code,
		}));
		
		$('#updateValues').modal('hide');
		$('#current-value-add').val('');
		
		e.preventDefault();
		return false;
	},
	
	sub_progress_bar: function(node) {
		var progress_node = $(node).closest('div.calc-attribute').find('div.progress-bar');
		
		virtual_table.websocket_send(JSON.stringify({
			"command": "update",
			"subcommand": "sub",
			"vtp": parseInt($(node).closest('div[data-vtp]').attr('data-vtp'), 10),
			"attr": progress_node.closest('.progress-bar-badge').find('.progress-bar-badge-label').attr('data-attr-code'),
		}));
	},
	
	sub_value: function(node) {
		virtual_table.websocket_send(JSON.stringify({
			"command": "update",
			"subcommand": "sub",
			"vtp": parseInt($(node).closest('div[data-vtp]').attr('data-vtp'), 10),
			"attr": $(node).closest('div[data-attr-code]').attr('data-attr-code'),
		}));
	},
	
	toggle_damage_vtp_target: function(node) {
		if ($(node).is('.btn-danger')) {
			$(node).removeClass('btn-danger').addClass('btn-secondary');
		}
		else {
			$(node).addClass('btn-danger').removeClass('btn-secondary');
		}
	},
	
	toggle_display: function(node) {
		var vtp_node = $(node).closest('div[data-vtp]');
		
		if (vtp_node.attr('data-display') === 'expanded') {
			// The div is in compress mode, we want to expand it.
			vtp_node.attr('data-display', 'compressed');
		}
		else {
			// The div is in expand mode, we want to compress it.
			vtp_node.attr('data-display', 'expanded');
		}
		
		virtual_table.apply_classes(vtp_node);
		virtual_table.check_order_vtp();
	},
	
	toggle_extension: function(node) {
		var extension_id = parseInt($(node).closest('[data-vtaeid]').attr('data-vtaeid'), 10);
		
		virtual_table.websocket_send(JSON.stringify({
			"command": "avalon_extension_toggle",
			"extension_id": extension_id,
			"weight_remaining": $('#avalon-result [data-vtaeid]:not(.text-success)').length,
		}));
	},
	
	toggle_history: function(node) {
		switch ($(node).attr('data-action')) {
			case 'hide':
				$(node).closest('.modal-body').find('.history-results div').slice(10).hide();
				$(node).addClass('d-none');
				$(node).closest('.modal-body').find('button[data-action=show]').removeClass('d-none');
			break;
			
			case 'show':
			default:
				$(node).closest('.modal-body').find('.history-results div').show();
				$(node).addClass('d-none');
				$(node).closest('.modal-body').find('button[data-action=hide]').removeClass('d-none');
			break;
		}
	},
	
	toggle_ignore_health_loss: function(node) {
		virtual_table.websocket_send(JSON.stringify({
			"command": "toggle",
			"attr": "ignore_health_loss",
			"vtp": parseInt($(node).closest('div[data-vtp]').attr('data-vtp'), 10),
		}));
	},
	
	websocket_send: function(message) {
		// Probably not the best way to handle the case where the websocket is out.
		switch (virtual_table.websocket.readyState) {
			case virtual_table.websocket.CONNECTING:
				return setTimeout(function() {
					virtual_table.websocket_send(message)
				}, 200);
				
			case virtual_table.websocket.OPEN:
				return virtual_table.websocket.send(message);
				
			case virtual_table.websocket.CLOSED:
			case virtual_table.websocket.CLOSING:
				virtual_table.init_ws();
				return setTimeout(function() {
					virtual_table.websocket_send(message)
				}, 200);
		}
	},
	
};

$(function() {
	virtual_table.calc_progress_bars();
	
	$('#destiny-state').val($('#destiny-state').attr('initial-value'));
	
	$('#updateValues').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		virtual_table.attr_code = button.data('attr-code'); // Extract info from data-* attributes
		var modal = $(this);
		
		var progress_node = $(button).closest('div.calc-attribute').find('div.progress-bar:first');
		
		modal.find('.modal-title').text(virtual_table.get_modal_title() + ' pour ' + $(button).closest('div.card').find('div.card-header span.character-name').text());
		modal.find('.modal-body #current-value').val(progress_node.length > 0 ? progress_node.attr('aria-valuenow') : button.text());
		modal.find('.modal-body #max-value').val(progress_node.length > 0 ? progress_node.attr('aria-valuemax') : button.attr('default-value'));
		modal.find('.modal-body form').attr('data-vtp', $(button).closest('[data-vtp]').attr('data-vtp'));
		modal.find('.modal-body label[for=max-value]').text(progress_node.length > 0 ? modal.find('.modal-body label[for=max-value]').attr('data-max-label') : modal.find('.modal-body label[for=max-value]').attr('data-default-label'));
		if ($(button).closest('div.calc-attribute').find('.recoverable-value').length > 0) {
			modal.find('.modal-body .recoverable').show();
			modal.find('.modal-body #recoverable-value').val($(button).closest('div.calc-attribute').find('.recoverable-value:first').text());
		}
		else {
			modal.find('.modal-body .recoverable').hide();
		}
	}).on('shown.bs.modal', function (event) {
		$(this).find('.modal-body #current-value').focus();
	});
	
	$('#updateName').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		var modal = $(this);
		
		modal.find('.modal-body #vtp-name-value').val(button.closest('.card-header').find('span.character-name').text());
		modal.find('.modal-body form').attr('data-vtp', $(button).closest('[data-vtp]').attr('data-vtp'));
	}).on('shown.bs.modal', function (event) {
		$(this).find('.modal-body #vtp-name-value').focus();
	});
	
	$('#config-vtp').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		var modal = $(this);
		var div = $(button).closest('[data-vtp]');
		
		modal.find('.modal-title').text('Paramétrer la fiche ' + $(button).closest('div.card').find('div.card-header span.character-name').text());
		modal.find('.modal-body #vtp-name-value').val(button.closest('.card-header').find('span.character-name').text());
		modal.find('.modal-body form').attr('data-vtp', div.attr('data-vtp'));
		
		modal.find(':checkbox').prop('checked', false);
		$('#armour-bar').val('');
		
		var classes_to_show = div.attr('data-classes-to-show');
		
		classes_to_show.split(',').forEach(classname => {
			classname = classname.trim();
			if (modal.find(':checkbox[value="' + classname + '"]').length > 0) {
				modal.find(':checkbox[value="' + classname + '"]').prop('checked', true);
			}
			else {
				if ($('#armour-bar option[value="' + classname + '"]').length > 0) {
					$('#armour-bar').val(classname);
				}
			}
		});
		
		modal.find('#ignore-health-loss-by-armour').prop('checked', div.attr('data-ignore-health-loss') === 'True');
		modal.find('#edit-level').val(div.attr('data-edit-level'));
		modal.find('#gest-advantage').val(div.attr('data-gest-advantage'));
	});
	
	$('.modal[id^=result-history]').on('show.bs.modal', function(event) {
		var modal = $(this);
		modal.find('.history-results div').show();
		modal.find('.history-results div').slice(10).hide();
		modal.find('button[data-action=hide]').addClass('d-none');
		modal.find('button[data-action=show]').removeClass('d-none');
	});
	
	$('.history-results').off('click').on('click', function(event) {
		var button = $(event.target).closest('button.redo-roll');
		if (button.length === 0) {
			return;
		}
		
		var body = button.closest('.card-body');
		if (body.find('.'+button.attr('data-roll-type')).length === 0) {
			return;
		}
		
		body.find('input.die-number').val(button.attr('data-die'));
		body.find('input.bonus-number').val(button.attr('data-bonus'));
		if (JSON.parse(button.attr('data-public'))) {
			body.find('.btn-share :checkbox').prop('checked', true);
			body.find('.btn-share').addClass('active');
		}
		else {
			body.find('.btn-share :checkbox').prop('checked', false);
			body.find('.btn-share').removeClass('active');
		}
		
		body.find('.'+button.attr('data-roll-type')).trigger('click');
		$(this).closest('.modal').modal('hide');
	});
	
	$('.sync-reward').on('input', function() {
		$('#rewards-players input[data-source=' + $(this).attr('id') + ']').val($(this).val());
	});
	
	virtual_table.init_ws();
	
	$('div[data-vtp]').each(function() {
		virtual_table.apply_classes($(this));
	});
	
	$('div[data-vtp][data-editable=False]').find('button.manage-player').addClass('disabled').parent().hide();
	$('div[data-vtp][data-editable=False]').find('a.manage-player').hide();
	$('div[data-vtp][data-editable=False]').find('.stat-value').removeClass('col-6').addClass('col-9');
	$('div[data-vtp][data-editable=True]').find('button.manage-player').removeClass('disabled').parent().show();
	$('div[data-vtp][data-editable=True]').find('a.manage-player').show();
	$('div[data-vtp][data-editable=True]').find('.stat-value').removeClass('col-9').addClass('col-6');
	
	$('.utc_timestamp').each(function() {
		date = new Date($(this).text());
		$(this).text(date.toLocaleTimeString() + date.toISOString().substr(19, 4));
		$(this).removeClass('utc_timestamp');
	});
	
	// Sleep/wake up management for mobile device
	
	// Set the name of the hidden property and the change event for visibility
	var hidden, visibilityChange; 
	if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support 
		hidden = "hidden";
		visibilityChange = "visibilitychange";
	} else if (typeof document.msHidden !== "undefined") {
		hidden = "msHidden";
		visibilityChange = "msvisibilitychange";
	} else if (typeof document.webkitHidden !== "undefined") {
		hidden = "webkitHidden";
		visibilityChange = "webkitvisibilitychange";
	}
	
	// When the page is shown, ask the websocket to reinit itself.
	function handleVisibilityChange() {
		if (!document[hidden]) {
			virtual_table.websocket_send(JSON.stringify({
				"command": "recap",
				"vtps": $('div[data-vtp]').map(function() { return parseInt($(this).attr('data-vtp'),10); }).get(),
			}));
		}
	}

	// Warn if the browser doesn't support addEventListener or the Page Visibility API
	if (typeof document.addEventListener === "undefined" || hidden === undefined) {
		console.log("This demo requires a browser, such as Google Chrome or Firefox, that supports the Page Visibility API.");
	} else {
		// Handle page visibility change   
		document.addEventListener(visibilityChange, handleVisibilityChange, false);
	}
	
	$('#vt-informations-consent').tooltip({
		selector: '.consent-comment',
		html: true,
		title: function() {
			return $(this).find('.d-none').html();
		}
	})
	
	virtual_table.check_order_vtp();
	virtual_table.init_sortable();
	
});
