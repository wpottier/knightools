from datetime import timedelta
import json
import random
import logging

from django.db import models
from django.db.models.fields import related
from django.utils import timezone
from django.utils.html import escape
from martor.models import MartorField
from colorful.fields import RGBColorField
from users.models import CustomUser
from django.utils.translation import gettext_lazy
from django.db.models import Q, Min
from django.core.validators import FileExtensionValidator
from django.core.exceptions import ObjectDoesNotExist

from gm.models.character import FactionEffect, GreatDeed, Archetype, Armour, Aspect, Crest, Characteristic, MajorArcana, Division, Faction, FactionEffect
# Create your models here.

logger = logging.getLogger(__name__)

class VirtualTable(models.Model):
    DUR_30MN = '30mn'
    DUR_1H = '1h'
    DUR_6H = '6h'
    DUR_12H = '12h'
    DUR_24H = '24h'
    DUR_ALWAYS = 'always'
    duration_choices = (
        (DUR_30MN, gettext_lazy('30 minutes')),
        (DUR_1H, gettext_lazy('1 heure')),
        (DUR_6H, gettext_lazy('6 heures')),
        (DUR_12H, gettext_lazy('12 heures')),
        (DUR_24H, gettext_lazy('24 heures')),
        (DUR_ALWAYS, gettext_lazy('Toujours')),
    )
    
    DESTINY_NONE = 'none'
    DESTINY_BLESS = 'bless'
    DESTINY_CURSE = 'curse'
    destiny_choices = (
        (DESTINY_NONE, gettext_lazy('Inactif')),
        (DESTINY_BLESS, gettext_lazy('Succès auto')),
        (DESTINY_CURSE, gettext_lazy('Échec auto')),
    )
    
    name = models.CharField(gettext_lazy('nom'), max_length=64)
    description = models.TextField(blank=True)
    max_character = models.PositiveSmallIntegerField(gettext_lazy('nb. personnage max'))
    game_master = models.ForeignKey(
        CustomUser,
        related_name='managed_tables',
        on_delete=models.CASCADE,
        limit_choices_to={'game_master': True}
    )
    invitation_token = models.CharField(gettext_lazy('clé d\'invitation'), max_length=16, null=True, unique=True, blank=True)
    token_valid_duration = models.CharField(gettext_lazy('accessible pendant'), max_length=6, choices=duration_choices)
    token_creation_date = models.DateTimeField(gettext_lazy('date création token'))
    destiny_state = models.CharField(gettext_lazy('maître(sse) du destin'), max_length=5, choices=destiny_choices, default=DESTINY_NONE)
    active_avalon = models.BooleanField(gettext_lazy('activer Avalon'), default=False, blank=True)
    active_faction = models.BooleanField(gettext_lazy('activer les relations diplomatiques'), default=False, blank=True)
    
    def token_has_expired(self):
        if self.invitation_token is None or self.invitation_token == '':
            return True
        
        if self.token_valid_duration == self.DUR_ALWAYS:
            return False
        
        delta = None
        if self.token_valid_duration == self.DUR_30MN:
            delta = timedelta(minutes=30)
        if self.token_valid_duration == self.DUR_1H:
            delta = timedelta(hours=1)
        if self.token_valid_duration == self.DUR_6H:
            delta = timedelta(hours=6)
        if self.token_valid_duration == self.DUR_12H:
            delta = timedelta(hours=12)
        if self.token_valid_duration == self.DUR_24H:
            delta = timedelta(hours=24)
        
        if delta is None:
            return True
        
        return self.token_creation_date + delta < timezone.now()
    
    def get_last_gm_rolls(self):
        """
        Get the last 30 rolls made by the GM.
        """
        return self.gm_rolls.order_by('-datetime').all()[:30]
    
    def get_separated_topics(self):
        safe_topics = []
        unsafe_topics = []
        unknown_topics = []
        
        users_active = CustomUser.objects.filter(
            virtualtables__virtual_table=self
        ).distinct()
        users_answered = CustomUser.objects.filter(
            consent_rules__virtual_table=self,
            consent_rules__ignore_when_left=False
        )
        
        # This list is used to know the default value of the answer. If not everyone playing on the table (including the GM)
        # answered for a particular topic, it's default is 1 and not 2 (unknown instead of safe).
        user_ids = list(CustomUser.objects.filter(virtualtables__virtual_table=self).union(CustomUser.objects.filter(pk=self.game_master.pk)).order_by('pk').values_list('pk', flat=True))
        
        for topic in self.topics.order_by('name').all():
            comments = []
            topic_user_ids = []
            final_comment = None
            final_answer = VirtualTableConsentAnswer.ANSWER_SAFE
            
            # Filter answers to only users still playing on the table
            for answer in topic.answers.filter(
                              Q(user__in=users_active)
                            | Q(user__in=users_answered)
                            | Q(user=self.game_master)
                          ).order_by('user__pk').all():
                if answer.comment != '':
                    comments.append(escape(answer.comment))
                if final_answer > answer.answer:
                    final_answer = answer.answer
                    
                if answer.user.pk in user_ids:
                    topic_user_ids.append(answer.user.pk)
                
            if topic_user_ids != user_ids and final_answer == VirtualTableConsentAnswer.ANSWER_SAFE:
                final_answer = VirtualTableConsentAnswer.ANSWER_UNKNOWN
            
            if len(comments) > 0:
                # shuffling comments to avoid being able to tell who wrote what depending on the order
                random.shuffle(comments)
                final_comment = "<hr>".join(comments)
            
            answer = {
                "name": topic.name,
                "comment": final_comment,
            }
            
            if final_answer == VirtualTableConsentAnswer.ANSWER_SAFE:
                safe_topics.append(answer)
            elif final_answer == VirtualTableConsentAnswer.ANSWER_UNSAFE:
                unsafe_topics.append(answer)
            else:
                unknown_topics.append(answer)
        
        return safe_topics, unsafe_topics, unknown_topics
    
    def is_destiny_state(self, key):
        pass
    
    def __str__(self):
        return self.name

class VirtualTablePlayer(models.Model):
    EDIT_LVL_PLAYER = 'p'
    EDIT_LVL_ALL = 'a'
    EDIT_LVL_GM = 'g'
    edit_level_choices = (
        (EDIT_LVL_PLAYER, gettext_lazy('joueur')),
        (EDIT_LVL_ALL, gettext_lazy('tous')),
        (EDIT_LVL_GM, gettext_lazy('MJ')),
    )
    
    GEST_ADVANTAGE_BALANCE = 'b'
    GEST_ADVANTAGE_LIGHT = 'l'
    GEST_ADVANTAGE_NONE = 'n'
    gest_advantage_choices = (
        (GEST_ADVANTAGE_BALANCE, gettext_lazy('équilibre')),
        (GEST_ADVANTAGE_LIGHT, gettext_lazy('lumière')),
        (GEST_ADVANTAGE_NONE, gettext_lazy('aucun')),
    )
    
    player = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='virtualtables')
    virtual_table = models.ForeignKey(VirtualTable, on_delete=models.CASCADE, related_name="players")
    name = models.CharField(gettext_lazy('nom personnage'), max_length=256)
    current_armour_points = models.PositiveSmallIntegerField(gettext_lazy("PA actuels"), null=True, blank=True)
    current_energy_points = models.PositiveSmallIntegerField(gettext_lazy("PE actuels"), null=True, blank=True)
    current_force_field = models.PositiveSmallIntegerField(gettext_lazy("CDF actuel"), null=True, blank=True)
    current_aegis = models.PositiveSmallIntegerField(gettext_lazy("Égide actuelle"), null=True, blank=True)
    current_health_points = models.PositiveSmallIntegerField(gettext_lazy("PS actuels"), null=True, blank=True)
    current_hope_points = models.PositiveSmallIntegerField(gettext_lazy("PES actuels"), default=50)
    current_armour_nod = models.PositiveSmallIntegerField(gettext_lazy("nods d'armure actuels"), blank=True, default=3)
    current_energy_nod = models.PositiveSmallIntegerField(gettext_lazy("nods d'énergie actuels"), blank=True, default=3)
    current_health_nod = models.PositiveSmallIntegerField(gettext_lazy("nods de soin actuels"), blank=True, default=3)
    current_grenade_stock = models.PositiveSmallIntegerField(gettext_lazy("grenades actuelles"), blank=True, default=5)
    current_heroism_points = models.PositiveSmallIntegerField(gettext_lazy('points héroïsme actuels'), default=0, blank=True)
    current_flux_points = models.PositiveSmallIntegerField(gettext_lazy("flux actuel"), null=True, blank=True) # only for psion
    current_impregnation_points = models.PositiveSmallIntegerField(gettext_lazy("imprégnation actuelle"), null=True, blank=True) # only for shaman
    current_gp = models.PositiveSmallIntegerField(gettext_lazy('PG actuels'), default=0, blank=True)
    current_xp = models.PositiveSmallIntegerField(gettext_lazy('PX actuels'), default=0, blank=True)
    default_force_field = models.PositiveSmallIntegerField(gettext_lazy("CDF par défaut"), null=True, blank=True)
    default_aegis = models.PositiveSmallIntegerField(gettext_lazy("Égide par défaut"), null=True, blank=True)
    max_armour_points = models.PositiveSmallIntegerField(gettext_lazy("PA max"), null=True, blank=True)
    max_energy_points = models.PositiveSmallIntegerField(gettext_lazy("PE max"), null=True, blank=True)
    max_health_points = models.PositiveSmallIntegerField(gettext_lazy("PS max"), null=True, blank=True)
    max_hope_points = models.PositiveSmallIntegerField(gettext_lazy("PES max"), default=50)
    max_armour_nod = models.PositiveSmallIntegerField(gettext_lazy("nods d'armure max"), blank=True, default=3)
    max_energy_nod = models.PositiveSmallIntegerField(gettext_lazy("nods d'énergie max"), blank=True, default=3)
    max_health_nod = models.PositiveSmallIntegerField(gettext_lazy("nods de soin max"), blank=True, default=3)
    max_grenade_stock = models.PositiveSmallIntegerField(gettext_lazy("grenades max"), blank=True, default=5)
    max_heroism_points = models.PositiveSmallIntegerField(gettext_lazy("points d'héroïsme max"), default=6, blank=True)
    max_flux_points = models.PositiveSmallIntegerField(gettext_lazy("flux max"), null=True, blank=True) # only for psion
    max_impregnation_points = models.PositiveSmallIntegerField(gettext_lazy("imprégnation max"), null=True, blank=True) # only for shaman
    total_gp = models.PositiveSmallIntegerField(gettext_lazy('PG totaux'), default=0, blank=True)
    total_xp = models.PositiveSmallIntegerField(gettext_lazy('PX totaux'), default=0, blank=True)
    impregnation_difficulty = models.SmallIntegerField(gettext_lazy("difficulté d'imprégnation"), help_text=gettext_lazy("Représente la difficulté du test d'imprégnation qui augmente à chaque fois qu'une capacité est utilisée. Remis à 0 entre chaque mission."), null=True, blank=True) # only for shaman
    time_paradox_difficulty = models.SmallIntegerField(gettext_lazy("difficulté de paradoxe temporel"), help_text=gettext_lazy("Représente la difficulté du test d'imprégnation qui augmente à chaque fois qu'une capacité est utilisée. Remis à 0 entre chaque scène."), null=True, blank=True) # only for warlock
    lost_energy_counter = models.SmallIntegerField(gettext_lazy("nombre PE perdus"), help_text=gettext_lazy("Représente les PE perdus sur ce combat et devant être restauré à hauteur de 1D6 par 6 points perdus"), default=0, blank=True)
    lost_hope_counter = models.SmallIntegerField(gettext_lazy("nombre PES perdus"), help_text=gettext_lazy("Représente les PES perdus sur ce combat et devant être restauré à hauteur de 1D6 par 6 points perdus"), default=0, blank=True)
    gest_advantage = models.CharField(gettext_lazy('avantage geste'), max_length=1, choices=gest_advantage_choices, default=GEST_ADVANTAGE_NONE)
    edit_level = models.CharField(gettext_lazy('éditable par'), max_length=1, choices=edit_level_choices, default=EDIT_LVL_PLAYER)
    ignore_health_loss_by_armour = models.BooleanField(gettext_lazy("Ignore la perte de PS via armure"), default=False)
    classes_to_show = models.TextField("classes CSS à afficher", default=".tab-status,.bar-health-points,.bar-armour-points,.bar-energy-points,.bar-hope-points,.counter-force-field,.tab-stock,.bar-health-nods,.bar-armour-nods,.bar-energy-nods,.bar-grenades-stock,.tab-legend,.bar-hero-points,.bar-glory-points,.bar-experience-points", blank=True)
    
    def get_last_rolls(self):
        """
        Get the last 30 rolls made by the character, combined with the GM public ones.
        
        Note: The *:*:-1 notation reverse the array, due to the order_by changing
        the good order to display. (EDIT: not true anymore, changed the order rules)
        """
        return self.rolls.union(VirtualTableRoll.objects.filter(virtual_table_player=None,virtual_table_gm=self.virtual_table,public=True).all()).order_by('-datetime').all()[:30]
        
    def get_last_public_rolls(self):
        """
        Get the last 30 public rolls made by the character, combined with the GM public ones.
        """
        return self.rolls.filter(public=True).union(VirtualTableRoll.objects.filter(virtual_table_player=None,virtual_table_gm=self.virtual_table,public=True).all()).order_by('-datetime').all()[:30]

class VirtualTableRoll(models.Model):
    virtual_table_player = models.ForeignKey(VirtualTablePlayer, on_delete=models.CASCADE, related_name='rolls', blank=True, null=True) # only for player rolls
    virtual_table_gm = models.ForeignKey(VirtualTable, on_delete=models.CASCADE, related_name='gm_rolls', blank=True, null=True) # Only for gm rolls
    datetime = models.DateTimeField()
    response_json = models.TextField()
    combo = models.BooleanField(blank=True) # is it a combo roll (True) or a damage roll (False)
    public = models.BooleanField(blank=True, default=False) # is it visible to all players or only to gm (and owner, eventually)
    
    parsed_response = None
    
    def get_displayed_dice(self):
        response = json.loads(self.response_json)
        result = ''
        first_iteration = True
        color_class_old = ''
        
        for die_face in response['detail']:
            if response['detail'][die_face]['count'] > 0:
                if (self.combo):
                    color_class = 'text-danger' if int(die_face) % 2 else 'text-success'
                    
                    # Mark the separation between success and danger.
                    if not first_iteration and color_class_old != color_class:
                        result = "%s <span class=\"mx-1\"></span>" % result
                    
                    first_iteration = False
                    color_class_old = color_class
                    
                    for i in range(0, response['detail'][die_face]['count']):
                        result = "%s <i class=\"%s fas fa-%s\"></i>" % (result, color_class, response['detail'][die_face]['class'])
                else:
                    for i in range(0, response['detail'][die_face]['count']):
                        # Order is reversed: biggest before.
                        result = "<i class=\"fas fa-%s\"></i> %s" % (response['detail'][die_face]['class'], result)
                    
        return result
    
    def response(self):
        if self.parsed_response is None:
            self.parsed_response = json.loads(self.response_json)
        return self.parsed_response
        
    class Meta:
        ordering = ['datetime']

class Character(models.Model):
    player = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='characters')
    name = models.CharField('nom', max_length=256, blank=True)
    nickname = models.CharField('surnom', max_length=256, blank=True)
    main_motivation = MartorField('motivation majeure')
    lesser_motivations = MartorField('motivation mineures')
    archetype = models.ForeignKey(Archetype, verbose_name="archétype", on_delete=models.CASCADE, related_name='characters')
    great_deed = models.ForeignKey(GreatDeed, verbose_name="haut fait", on_delete=models.CASCADE, related_name='characters')
    crest = models.ForeignKey(Crest, verbose_name="blason", on_delete=models.CASCADE, related_name='characters')
    armour = models.ForeignKey(Armour, verbose_name="armure", on_delete=models.CASCADE, related_name='characters')
    division = models.ForeignKey(Division, verbose_name="section", on_delete=models.CASCADE, related_name='characters')
    final_total_armour_points = models.PositiveSmallIntegerField("total PA final")
    final_total_energy_points = models.PositiveSmallIntegerField("total PE final")
    final_total_health_points = models.PositiveSmallIntegerField("total PS final")
    final_total_hope_points = models.PositiveSmallIntegerField("PES totaux", default=50)
    current_hope_points = models.PositiveSmallIntegerField("PES actuels") # not refilled between missions, so it has its place here.
    default_force_field = models.PositiveSmallIntegerField("CDF par défaut") # in case it's boosted via modules
    default_armour_nod = models.PositiveSmallIntegerField("nods d'armure", default=3) # in case it's boosted via modules
    default_energy_nod = models.PositiveSmallIntegerField("nods d'énergie", default=3) # in case it's boosted via modules
    default_health_nod = models.PositiveSmallIntegerField("nods de soin", default=3) # in case it's boosted via modules
    default_grenade_stock = models.PositiveSmallIntegerField("grenades", default=5) # in case it's boosted via modules
    defence_no_od = models.PositiveSmallIntegerField("total défense sans OD")
    defence_with_od = models.PositiveSmallIntegerField("total défense avec OD")
    reaction_no_od = models.PositiveSmallIntegerField("total reaction sans OD")
    reaction_with_od = models.PositiveSmallIntegerField("total reaction avec OD")
    initiative_no_od = models.PositiveSmallIntegerField("total initiative sans OD")
    initiative_with_od = models.PositiveSmallIntegerField("total initiative avec OD")
    current_glory_points = models.PositiveSmallIntegerField("PG actuels")
    total_glory_points = models.PositiveSmallIntegerField("PG totaux")
    current_experience_points = models.PositiveSmallIntegerField("XP actuels")
    total_experience_points = models.PositiveSmallIntegerField("XP totaux")
    current_heroic_points = models.PositiveSmallIntegerField("points d'héroïsme")
    current_contact_points = models.PositiveSmallIntegerField("points de contact")
    background = MartorField()
    notes = MartorField()
    aspects = models.ManyToManyField(Aspect, through='CharacterAspect')
    
class CharacterArcana(models.Model):
    character = models.ForeignKey(Character, on_delete=models.CASCADE, related_name='arcanas')
    arcana = models.ForeignKey(MajorArcana, on_delete=models.CASCADE, related_name='characters')
    knight_arcana = models.BooleanField()
    ai_arcana = models.BooleanField()
    choosen_knight_advantage = models.BooleanField()
    choosen_knight_disadvantage = models.BooleanField()
    choosen_ai_advantage = models.BooleanField()
    choosen_ai_disadvantage = models.BooleanField()
    
class VirtualTableCharacter(models.Model):
    character = models.ForeignKey(Character, on_delete=models.CASCADE, related_name="tables")
    virtual_table = models.ForeignKey(VirtualTable, on_delete=models.CASCADE, related_name="characters")
    current_armour_points = models.PositiveSmallIntegerField("PA actuels")
    current_energy_points = models.PositiveSmallIntegerField("PE actuels")
    current_force_field = models.PositiveSmallIntegerField("CDF actuel")
    current_health_points = models.PositiveSmallIntegerField("PS actuels")
    current_armour_nod = models.PositiveSmallIntegerField("nods d'armure actuels")
    current_energy_nod = models.PositiveSmallIntegerField("nods d'énergie actuels")
    current_health_nod = models.PositiveSmallIntegerField("nods de soin actuels")
    current_grenade_stock = models.PositiveSmallIntegerField("grenades actuelles")
    
class CharacterAspect(models.Model):
    character = models.ForeignKey(Character, on_delete=models.CASCADE)
    aspect = models.ForeignKey(Aspect, on_delete=models.CASCADE, related_name="characters")
    color = RGBColorField(null=True, blank=True)
    value = models.PositiveSmallIntegerField("valeur")
    characteristics = models.ManyToManyField(Characteristic, through='CharacterAspectCharacteristic')
    
    class Meta:
        unique_together = ('character', 'aspect')
    
class CharacterAspectCharacteristic(models.Model):
    character_aspect = models.ForeignKey(CharacterAspect, on_delete=models.CASCADE)
    characteristic = models.ForeignKey(Characteristic, on_delete=models.CASCADE, related_name="characters")
    color = RGBColorField(null=True, blank=True)
    value = models.PositiveSmallIntegerField("valeur")
    overdrive_level = models.PositiveSmallIntegerField("niv. overdrive", null=True, blank=True)
    
    class Meta:
        unique_together = ('character_aspect', 'characteristic')
    
class VirtualTableConsentTopic(models.Model):
    name = models.CharField("nom", max_length=512)
    virtual_table = models.ForeignKey(to=VirtualTable, on_delete=models.CASCADE, related_name="topics")
    
    def __str__(self):
        return self.name
    
class VirtualTableConsentRule(models.Model):
    virtual_table = models.ForeignKey(to=VirtualTable, on_delete=models.CASCADE, related_name="consent_rules")
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='consent_rules')
    ignore_when_left = models.BooleanField(
        gettext_lazy("Ignorer mes réponses si aucun personnage"),
        default=False,
        blank=True,
        help_text=gettext_lazy("En cochant cette case, vos réponses à ce questionnaire ne seront plus prises en compte une fois que vous ne serez plus sur la table. En pratique, cela signifie qu'il y a un risque pour que vos réponses puissent être devinées par les autres participants. Ne cochez cette case que si la table est constituée de personnes en qui vous avez confiance ou que vous pouvez aborder librement les sujets qui vous dérangent."))
    
    class Meta:
        unique_together = ("virtual_table", "user")

class VirtualTableConsentAnswer(models.Model):
    ANSWER_UNSAFE = 0
    ANSWER_UNKNOWN = 1
    ANSWER_SAFE = 2
    answer_choices = (
        (ANSWER_UNSAFE, gettext_lazy('Sujet complètement interdit')),
        (ANSWER_UNKNOWN, gettext_lazy('Limites à discuter')),
        (ANSWER_SAFE, gettext_lazy('Aucun problème à aborder ce sujet')),
    )
    
    topic = models.ForeignKey(to=VirtualTableConsentTopic, on_delete=models.CASCADE, related_name='answers')
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='consents')
    answer = models.IntegerField("réponse", choices=answer_choices)
    comment = models.TextField('commentaire')
    
    
    class Meta:
        unique_together = ("user", "topic")
        
    def __str__(self):
        return ' '.join([self.question.name, str(VirtualTableConsentAnswer.answer_choices[self.answer][1])])
        
    def get_question(self):
        return self.question.name
    
class AvalonExtension(models.Model):
    name = models.CharField("nom", max_length=32)
    description = MartorField()
    effect = MartorField()
    cost = models.PositiveSmallIntegerField("coût de construction")
    prerequired_extensions = models.ManyToManyField(
        'AvalonExtension',
        verbose_name="pré-requis",
        related_name="required_for",
        blank=True,
    )
    faq_text = MartorField(blank=True, null=True)
    slug = models.SlugField(null=True, blank=True, unique=True)
    
    def __str__(self):
        return self.name
    
    def get_prerequired_names(self):
        names = []
        for extension in self.prerequired_extensions.all():
            names.append(extension.name)
        return ', '.join(names)


class VirtualTableAvalon(models.Model):
    total_construction_points = models.PositiveSmallIntegerField(gettext_lazy('points de constructions totaux'), default=0)
    virtual_table = models.OneToOneField(VirtualTable, on_delete=models.CASCADE, related_name="avalon")
    
    def get_remaining_construction_points(self):
        used = 0
        for extensions in self.extensions.filter(built=True):
            used = used + extensions.avalon_extension.cost
        return self.total_construction_points - used
    
    def sync_extensions(self):
        """
        Synchronize avalon extension with a virtual table to "prepare" them all to be voted. If a newly extension is created
        on the administration interface, it automatically create it as a VirtualTableAvalonExtension.
        """
        if len(self.extensions.all()) == len(AvalonExtension.objects.all()):
            return self.extensions.all()
        
        logger.debug('missing extensions for table %s (%d), already set: %s ' % (self.virtual_table.name, self.virtual_table.id, self.extensions.values_list('avalon_extension__pk', flat=True)))
        avalon_extensions = AvalonExtension.objects.filter(
            ~Q(pk__in=self.extensions.values_list('avalon_extension__pk', flat=True)),
        )
        
        for extension in avalon_extensions:
            logger.debug('binding new extension: %s ' % (extension.name))
            
            self.extensions.create(
                built=extension.cost == 0,
                avalon_extension=extension,
            )
            # table_extension = VirtualTableAvalonExtension()
            # if extension.cost == 0:
            #     table_extension.built = True
            # table_extension.avalon_extension = extension
            # self.extensions.add(table_extension, bulk=False)
        
        return self.extensions.all()
    
    def get_ordered_extensions(self):
        extensions_list = list(self.extensions.order_by('avalon_extension__name').all())
        extensions_list.sort(key=lambda x: (not x.built, x.get_avg_weight() * -1, x.avalon_extension.name))
        return extensions_list
    
    def get_extensions_by_user_vote(self, user):
        return self.extensions.annotate( 
            user_vote=Min( 
                'votes__weight', 
                filter=Q(votes__user=user) 
            ), 
        ).order_by('-user_vote', 'built', 'avalon_extension__name').all()
    
    

class VirtualTableAvalonExtension(models.Model):
    virtual_table_avalon = models.ForeignKey(VirtualTableAvalon, on_delete=models.CASCADE, related_name="extensions")
    avalon_extension = models.ForeignKey(AvalonExtension(), on_delete=models.CASCADE)
    built = models.BooleanField(gettext_lazy("construit"), default=False)
    
    def get_avg_weight(self):
        vote_count = 0
        final_weight = 0
        count_extensions_built = self.virtual_table_avalon.extensions.filter(built=True, avalon_extension__cost__gt=0).count()
        count_extensions_to_build = self.virtual_table_avalon.extensions.filter(built=False, avalon_extension__cost__gt=0).count()
        
        for vote in self.votes.all():
            vote_count = vote_count + 1
            weight = vote.relative_weight
            if weight > count_extensions_to_build:
                weight = weight - count_extensions_built
            final_weight = final_weight + weight
            
        if vote_count == 0:
            return 0
        
        return final_weight / vote_count
    
    def get_remaining_prerequired_extensions_name(self):
        names = []
        for extension in self.avalon_extension.prerequired_extensions.all():
            prerequired_extension =  self.virtual_table_avalon.extensions.get(avalon_extension=extension)
            if not prerequired_extension.built:
                names.append(extension.name)
        return ', '.join(names)

class VirtualTableAvalonExtensionVote(models.Model):
    virtual_table_avalon_extension = models.ForeignKey(VirtualTableAvalonExtension, on_delete=models.CASCADE, related_name="votes")
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    weight = models.PositiveSmallIntegerField(gettext_lazy('poids'))
    relative_weight = models.PositiveSmallIntegerField(gettext_lazy('poids relatif'), default=0) # for the weight of the vote after some extensions are built
    

class VirtualTableFactionNode(models.Model):
    virtual_table = models.ForeignKey(VirtualTable, on_delete=models.CASCADE, related_name='factions')
    name = models.CharField("nom", max_length=128, null=True, blank=True)
    visible_player = models.BooleanField("relation visible aux joueurs", blank=True, default=False)
    faction = models.ForeignKey(Faction, on_delete=models.CASCADE, related_name="nodes", null=True, blank=True)
    discovered_secrets = models.TextField(gettext_lazy("secrets découverts"), blank=True)
    public_description = models.TextField("description publique", blank=True)
    gm_description = models.TextField("description MJ", blank=True, null=True)
    logo_file = models.ImageField(upload_to='custom_faction_logo',null=True, validators=[FileExtensionValidator(allowed_extensions=['jpg','png']),]) # if null, rollback on a placeholder file, except if faction exists, which case it takes this one
    pc_faction = models.BooleanField("faction des PJs", default=False, blank=True) # if True, effects will be active on relation to this node. One node allowed as PC faction for each virtual table.
    
    class Meta:
        unique_together = ("virtual_table", "faction")
    
    def recalculate_relationships(self):
        if not self.pc_faction:
            return
        
        allowed_players = self.virtual_table.players.all()
        try:
            if not self.virtual_table.faction_settings.enable_vote_system:
                return
            allowed_players = (
                self
                .virtual_table
                .players
                .exclude(
                    pk__in=(
                        self
                        .virtual_table
                        .faction_settings
                        .vote_blacklist
                        .all()
                    )
                )
                .all()
            )
        except ObjectDoesNotExist:
            pass
        
        # We just have to parse relations going from PC faction to other
        # faction, because when a vote is saved, the source relation is
        # created if it wasn't already existing. If the PC faction has
        # no relation with another faction, you can be sure there is no
        # vote for it.
        for rel in self.sources.all():
            # Avoid a division by 0
            if len(allowed_players) == 0:
                rel.level = 0
                rel.save()
                continue
            
            avg = 0
            target = rel.node_target
            votes = (
                VirtualTableFactionNodeVote
                .objects
                .filter(
                    node=target,
                    player__in=allowed_players
                )
                .all()
            )
            for vote in votes:
                avg = avg + vote.affinity
            
            rel.level = round(avg / len(allowed_players))
            rel.save()
            
    
class VirtualTableFactionNodeEffect(models.Model):
    node = models.ForeignKey(VirtualTableFactionNode, on_delete=models.CASCADE, related_name="effects")
    level_name = models.CharField("nom niveau", max_length=128)
    level_memo = models.TextField("mémo niveau (MJ only)", blank=True)
    level = models.SmallIntegerField("niveau")
    effect_name = models.CharField("nom capacité ou malus", max_length=128, blank=True)
    effect_description = models.TextField("description capacité ou malus", blank=True)
    effect = models.ForeignKey(FactionEffect, on_delete=models.CASCADE, null=True, related_name="node_effects")

class VirtualTableFactionEdge(models.Model):
    node_source = models.ForeignKey(VirtualTableFactionNode, on_delete=models.CASCADE, related_name="sources")
    node_target = models.ForeignKey(VirtualTableFactionNode, on_delete=models.CASCADE, related_name="targets")
    visible_player = models.BooleanField("relation visible aux joueurs", blank=True, default=False)
    level = models.SmallIntegerField("niveau de relation", default=0)
    visible_effect = models.BooleanField("effet niveau visible aux joueurs", blank=True, default=False)
    
    class Meta:
        unique_together = ("node_source", "node_target")

class VirtualTableFactionNodeVote(models.Model):
    node = models.ForeignKey(VirtualTableFactionNode, on_delete=models.CASCADE, related_name="votes")
    player = models.ForeignKey(VirtualTablePlayer, on_delete=models.CASCADE, related_name="affinities")
    affinity = models.SmallIntegerField("affinité", blank=True)
    
    class Meta:
        unique_together = ("node", "player")
    
class VirtualTableFactionSettings(models.Model):
    """
    This class allows to change some "global settings" on the faction
    feature. It handles the affinity system activation (enabled by
    default) and the player blacklisted for vote
    
    It's particularly useful when a user has a second character,
    like the Ascension or the Lion, and you don't want these characters
    to compromise the affinity PC faction has to others.
    """
    virtual_table = models.OneToOneField(VirtualTable, on_delete=models.CASCADE, related_name='faction_settings')
    enable_vote_system = models.BooleanField("activer système de vote", blank=True, default=True)
    vote_blacklist = models.ManyToManyField(VirtualTablePlayer, verbose_name="vote blacklist", blank=True)
    
    
class HelpingArticle(models.Model):
    index = models.PositiveSmallIntegerField("indice", blank=True, default=0)
    gm = models.BooleanField("MJ seulement ?", blank=True, default=False)
    title = models.CharField("titre", max_length=128)
    article = MartorField()
    
    def __str__(self):
        return self.title
    
