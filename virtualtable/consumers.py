from contextlib import nullcontext
import json
import subprocess
import logging
import uuid
from gettext import gettext

from django.core.exceptions import ObjectDoesNotExist
from django.db.models.query_utils import Q
from django.templatetags.static import static

from django.forms.models import model_to_dict
from django.db.models import F, Case, When, BooleanField
from django.template import loader

from django.urls import reverse
from django.utils import timezone

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer


from .models import VirtualTableAvalonExtensionVote, VirtualTableFactionNodeEffect, VirtualTableFactionSettings, VirtualTablePlayer, VirtualTableRoll, VirtualTable, VirtualTableFactionNode, VirtualTableFactionEdge, VirtualTableFactionNodeVote
from gm.models.character import MajorArcana, Faction, FactionDefaultEdge, FactionEffect

from markdown import markdown

logger = logging.getLogger(__name__)

class VirtualTableConsumer(WebsocketConsumer):
    def connect(self):
        self.vt_id = int(self.scope['url_route']['kwargs']['vt_id'])
        self.vt_group_name = 'virtual_table_%s' % self.vt_id
        
        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.vt_group_name,
            self.channel_name
        )
        # import pdb; pdb.set_trace()
        
        async_to_sync(self.channel_layer.group_send)(
            self.vt_group_name,
            {
                'type': 'vt_message',
                'params': {
                    "command": 'join',
                    "vtp": list(VirtualTablePlayer.objects.filter(player=self.scope['user'].id).values_list('id', flat=True)),
                    "name": self.scope['user'].first_name,
                }
            }
        )
        
        self.accept()

    def disconnect(self, close_code):
        # import pdb; pdb.set_trace()
        async_to_sync(self.channel_layer.group_send)(
            self.vt_group_name,
            {
                'type': 'vt_message',
                'params': {
                    "command": 'quit',
                    "vtp": list(VirtualTablePlayer.objects.filter(player=self.scope['user'].id).values_list('id', flat=True)),
                }
            }
        )
        async_to_sync(self.channel_layer.group_discard)(
            self.vt_group_name,
            self.channel_name
        )

    def receive(self, text_data):
        """
        When receiving a message, the websocket will do what is asked in the message.command
        and return the newly data, refreshed from database.
        
        text_data: valid json object with at least attributes:
            - command: update|status|hurt|toggle|combo|damage|destiny|config|avalon_vote|avalon_extension_toggle|avalon_reset_votes
            - vtp: id of the virtualtableplayer asking that command
            
            Additional attributes for update command:
                subcommand: edit|add|sub|reset
                attr: attribute code to update
                max: max value for this attribute (can be None for some subcommand)
                current: current value for this attribute (can be None for some subcommand)
                
            Additional attributes for status command:
                counter: counter of status command asked for this user. Used to avoid conflict with async tech.
                
            Additional attributes for hurt command:
                armour-piercing: Integer          Under a certain level of armour points, it ignores them. Default 0
                ignore-ff:    Boolean          Ignore force field. Default False if None.
                ignore-armour: Boolean          Ignore armour. If armour-piercing and ignore-armour are given, ignore-armour will be prioritary. Default False if None
                target:       Char             Target directly Hope Points, not health or armour. List between "hp" (health points),
                                                 "hop" (for hope points), "ap" (armour points) or "ep" (energy points). Default "hp"
                                                 If "ap", can't be combined with ignore-armour or armour-piercing (target will be prioritary).
                value:        Integer          Damage value to be calculated. Required
                vtp:          Array(Integer)   VirtualTablePlayer ids taking damages. They must belong to the VirtualTable.
            
            Additional attributes for toggle command:
                attr:  Char      Either rights or ignore_health_loss. Rights need a second attributes "value". Required
                value: Char      The new value (must be a valid value for VirtualTablePlayer.edit_level_choices)
                vtp:   Integer   VirtualTablePlayer id to change. It must belong to the VirtualTable.
            
            Additional attributes for combo command:
                dice_number:     Integer   Number of dice rolling for the combo test. Required
                overdrive_value: Integer   Overdrive and other automatic success for this roll. They will automatically be added if the roll is not a critical fail. Can be empty or 0.
                vtp:             Integer   VirtualTablePlayer id doing the combo roll.
            
            Additional attributes for damage command:
                dice_number: Integer   The number of dice launched for the damage roll. Can be 0 if the damage are only fixed without roll (could happen). If dice number equals 0 and bonus_value too, nothing is done.
                bonus_value: Integer   The bonus to add to the attack. They will automatically be added to the result, even if no dice are rolled. If dice number equals 0 and bonus_value too, nothing is done.
                vtp:         Integer   VirtualTablePlayer id doing the damage roll.
            
            Additional attributes for config command:
                classes_to_show: Char    Lists of css classes, separated by commas, needing to be shown on the interface.
                vtp:             Integer VirtualTablePlayer id having its configuration changed
            
            Additional attributes for destiny command:
                vtp:         Integer   VirtualTablePlayer id doing the destiny card draw.
            
            Additional attributes for avalon_vote command:
                new_ordered_extensions:   Array(Integer)  New avalon extensions ordered list id for this user
                
            Additional attributes for avalon_extension_toggle command:
                extension_id:   Integer  Avalon extension id to toggle (build or destroy) for this table
        """
        try:
            text_data_json = json.loads(text_data)
            command = text_data_json['command']
            command_id = uuid.uuid4()
            
            def restore_vtp_counters(vtp):
                
                dice_energy = vtp.lost_energy_counter // 6
                dice_hope = vtp.lost_hope_counter // 6
                logger.info('[%s] [RESTORE] VTP: %d restore %d d6 energy and %d d6 hope  ' % (command_id, vtp.id, dice_energy, dice_hope))
                
                if dice_energy > 0:
                    response = {
                        'damage': 0,
                        'dice': dice_energy,
                        'bonus': 0,
                        'detail': {
                            1: {
                                'class': 'dice-one',
                                'count': 0
                            },
                            2: {
                                'class': 'dice-two',
                                'count': 0
                            },
                            3: {
                                'class': 'dice-three',
                                'count': 0
                            },
                            4: {
                                'class': 'dice-four',
                                'count': 0
                            },
                            5: {
                                'class': 'dice-five',
                                'count': 0
                            },
                            6: {
                                'class': 'dice-six',
                                'count': 0
                            },
                        },
                    }
                    
                    try:
                        byte_output = subprocess.check_output(['dice', '-j', '%(dice)dd6' % {'dice': dice_energy}])
                    except subprocess.CalledProcessError as e:
                        logger.critical('[%s] [RESTORE] exec command failed, maybe binary is not installed? %s' % (command_id, str(e)))
                        raise e
                    except Exception as e:
                        logger.critical('[%s] [RESTORE] unknown exception on exec command %s' % (command_id, str(e)))
                        raise e
                    
                    result_object = json.loads(byte_output.decode())
                    
                    response['damage'] = int(result_object['scalar'])
                    
                    logger.debug('[%s] [RESTORE] restored: %d' % (command_id, response['damage']))
                    
                    logger.debug('[%s] [RESTORE] current_energy_points: %d' % (command_id, vtp.current_energy_points))
                    vtp.current_energy_points = vtp.current_energy_points + response['damage']
                    if vtp.current_energy_points < 0:
                        current_value = 0
                    if vtp.current_energy_points > vtp.max_energy_points:
                        vtp.current_energy_points = vtp.max_energy_points
                    
                    logger.debug('[%s] [RESTORE] new current_energy_points: %d' % (command_id, vtp.current_energy_points))

                    if not (result_object.get('values') is None):
                        for dice in result_object['values'][0]['values']:
                            response['detail'][dice['total']]['count'] = response['detail'][dice['total']]['count'] + 1
                    else:
                        for dice in result_object['instructions'][0]['diceval']:
                            response['detail'][dice['value']]['count'] = response['detail'][dice['value']]['count'] + 1
                    logger.info('[%s] [RESTORE] result (energy): %s' % (command_id, json.dumps(response)))
                    
                    roll = VirtualTableRoll()
                    roll.datetime = timezone.now()
                    roll.response_json = json.dumps(response)
                    roll.combo = False
                    roll.public = True
                    roll.virtual_table_player = vtp
                    
                    roll.save()
                    VirtualTableRoll.objects.filter(datetime__lte=timezone.now()-timezone.timedelta(hours=12)).delete()
                    
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "damage_result",
                                "public": True,
                                "vtp": model_to_dict(vtp),
                                "response": response,
                            }
                        }
                    )
                
                vtp.lost_energy_counter = 0
                
                if dice_hope > 0:
                    response = {
                        'damage': 0,
                        'dice': dice_hope,
                        'bonus': 0,
                        'detail': {
                            1: {
                                'class': 'dice-one',
                                'count': 0
                            },
                            2: {
                                'class': 'dice-two',
                                'count': 0
                            },
                            3: {
                                'class': 'dice-three',
                                'count': 0
                            },
                            4: {
                                'class': 'dice-four',
                                'count': 0
                            },
                            5: {
                                'class': 'dice-five',
                                'count': 0
                            },
                            6: {
                                'class': 'dice-six',
                                'count': 0
                            },
                        },
                    }
                    
                    try:
                        byte_output = subprocess.check_output(['dice', '-j', '%(dice)dd6' % {'dice': dice_hope}])
                    except subprocess.CalledProcessError as e:
                        logger.critical('[%s] [RESTORE] exec command failed, maybe binary is not installed? %s' % (command_id, str(e)))
                        raise e
                    except Exception as e:
                        logger.critical('[%s] [RESTORE] unknown exception on exec command %s' % (command_id, str(e)))
                        raise e
                    
                    result_object = json.loads(byte_output.decode())
                    
                    response['damage'] = int(result_object['scalar'])
                    
                    logger.debug('[%s] [RESTORE] restored: %d' % (command_id, response['damage']))
                    
                    logger.debug('[%s] [RESTORE] current_hope_points: %d' % (command_id, vtp.current_hope_points))
                    vtp.current_hope_points = vtp.current_hope_points + response['damage']
                    if vtp.current_hope_points < 0:
                        current_value = 0
                    if vtp.current_hope_points > vtp.max_hope_points:
                        vtp.current_hope_points = vtp.max_hope_points
                    
                    logger.debug('[%s] [RESTORE] new current_hope_points: %d' % (command_id, vtp.current_hope_points))

                    if not (result_object.get('values') is None):
                        for dice in result_object['values'][0]['values']:
                            response['detail'][dice['total']]['count'] = response['detail'][dice['total']]['count'] + 1
                    else:
                        for dice in result_object['instructions'][0]['diceval']:
                            response['detail'][dice['value']]['count'] = response['detail'][dice['value']]['count'] + 1
                    logger.info('[%s] [RESTORE] result (hope): %s' % (command_id, json.dumps(response)))
                    
                    roll = VirtualTableRoll()
                    roll.datetime = timezone.now()
                    roll.response_json = json.dumps(response)
                    roll.combo = False
                    roll.public = True
                    roll.virtual_table_player = vtp
                    
                    roll.save()
                    VirtualTableRoll.objects.filter(datetime__lte=timezone.now()-timezone.timedelta(hours=12)).delete()
                    
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "damage_result",
                                "public": True,
                                "vtp": model_to_dict(vtp),
                                "response": response,
                            }
                        }
                    )
                
                vtp.lost_hope_counter = 0
                
                vtp.save()
            
            if command == "update":
                subcommand = text_data_json['subcommand']
                attr_code = text_data_json['attr']
                vtp_id = text_data_json['vtp']
                
                if vtp_id is None:
                    raise Exception('Invalid parameters', 'invalid')
                
                vtp_id = int(vtp_id)
                
                vtp = VirtualTablePlayer.objects.get(pk=vtp_id)
                
                if vtp.virtual_table.id != self.vt_id:
                    # import pdb; pdb.set_trace()
                    raise Exception('Incoherent parameters', 'incoherent')
                
                if ((vtp.edit_level == vtp.EDIT_LVL_PLAYER
                  and vtp.player.id != self.scope['user'].id
                  and vtp.virtual_table.game_master.id != self.scope['user'].id)
                  or (vtp.edit_level == vtp.EDIT_LVL_GM
                  and vtp.virtual_table.game_master.id != self.scope['user'].id)):
                    raise Exception('Unauthorized modification for this user and this virtual table player', 'unauthorized_user_vtp')
                
                if subcommand == 'edit':
                    # import pdb; pdb.set_trace()
                    
                    try:
                        current_value = text_data_json['current']
                    except KeyError:
                        current_value = None
                    except Exception as e:
                        raise e
                        
                    try:
                        current_value_add = text_data_json['current_add']
                    except KeyError:
                        current_value_add = None
                    except Exception as e:
                        raise e
                    
                    try:
                        max_value = text_data_json['max']
                    except KeyError:
                        max_value = None
                    except Exception as e:
                        raise e
                    
                    try:
                        recoverable_value = text_data_json['recoverable']
                    except KeyError:
                        recoverable_value = None
                    except Exception as e:
                        raise e
                    
                    if max_value is not None:
                        max_value = int(max_value)
                    
                    if current_value is not None:
                        current_value = int(current_value)
                        
                        if current_value_add is not None:
                            current_value_add = int(current_value_add)
                            current_value = current_value + current_value_add
                            if current_value < 0:
                                current_value = 0
                    
                    if recoverable_value is not None:
                        recoverable_value = int(recoverable_value)
                    else:
                        recoverable_value = 0
                    
                    if attr_code == 'name':
                        try:
                            name = text_data_json['name']
                        except Exception as e:
                            raise e
                        
                        if name.strip() ==  '':
                            raise Exception('Empty name is not allowed', 'empty_name')
                        
                        vtp.name = name
                        vtp.save()
                        
                    elif attr_code == 'hp':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_health_points = current_value
                        vtp.max_health_points = max_value
                        vtp.save()
                        
                    elif attr_code == 'hn':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_health_nod = current_value
                        vtp.max_health_nod = max_value
                        vtp.save()
                        
                    elif attr_code == 'ap':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_armour_points = current_value
                        vtp.max_armour_points = max_value
                        vtp.save()
                        
                    elif attr_code == 'an':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_armour_nod = current_value
                        vtp.max_armour_nod = max_value
                        vtp.save()
                        
                    elif attr_code == 'ep':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_energy_points = current_value
                        vtp.max_energy_points = max_value
                        vtp.lost_energy_counter = recoverable_value
                        vtp.save()
                    
                    elif attr_code == 'en':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_energy_nod = current_value
                        vtp.max_energy_nod = max_value
                        vtp.save()
                    
                    elif attr_code == 'hop':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_hope_points = current_value
                        vtp.max_hope_points = max_value
                        vtp.lost_hope_counter = recoverable_value
                        vtp.save()
                    
                    elif attr_code == 'gs':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_grenade_stock = current_value
                        vtp.max_grenade_stock = max_value
                        vtp.save()
                    
                    elif attr_code == 'ff':
                        vtp.current_force_field = current_value
                        vtp.default_force_field = max_value
                        vtp.save()
                    
                    elif attr_code == 'ae':
                        vtp.current_aegis = current_value
                        vtp.default_aegis = max_value
                        vtp.save()
                    
                    elif attr_code == 'hep':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_heroism_points = current_value
                        vtp.max_heroism_points = max_value
                        vtp.save()
                    
                    elif attr_code == 'flux':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_flux_points = current_value
                        vtp.max_flux_points = max_value
                        vtp.save()
                    
                    elif attr_code == 'imp':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_impregnation_points = current_value
                        vtp.max_impregnation_points = max_value
                        vtp.save()
                    
                    elif attr_code == 'gp':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_gp = current_value
                        vtp.total_gp = max_value
                        vtp.save()
                    
                    elif attr_code == 'xp':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_xp = current_value
                        vtp.total_xp = max_value
                        vtp.save()
                    
                    elif attr_code == 'pard':
                        vtp.time_paradox_difficulty = current_value
                        vtp.save()
                    
                    elif attr_code == 'impd':
                        vtp.impregnation_difficulty = current_value
                        vtp.save()
                    
                    # import pdb; pdb.set_trace()
                else:
                    if subcommand == 'reset':
                        if attr_code == 'ff':
                            vtp.current_force_field = vtp.default_force_field
                            vtp.save()
                        elif attr_code == 'ae':
                            vtp.current_aegis = vtp.default_aegis
                            vtp.save()
                        else:
                            raise Exception('Not known subcommand for this attribute', 'unknown_attr_subcommand')
                    else:
                        if subcommand == 'add':
                            step = 1
                        elif subcommand == 'sub':
                            step = -1
                        else:
                            raise Exception('Not known subcommand for this attribute', 'unknown_attr_subcommand')
                        
                        # import pdb; pdb.set_trace()
                        
                        if attr_code == 'hp':
                            vtp.current_health_points = F('current_health_points') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_health_points > vtp.max_health_points:
                                vtp.current_health_points = vtp.max_health_points
                                vtp.save()
                            
                        elif attr_code == 'hn':
                            vtp.current_health_nod = F('current_health_nod') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_health_nod > vtp.max_health_nod:
                                vtp.current_health_nod = vtp.max_health_nod
                                vtp.save()
                            
                        elif attr_code == 'ap':
                            vtp.current_armour_points = F('current_armour_points') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_armour_points > vtp.max_armour_points:
                                vtp.current_armour_points = vtp.max_armour_points
                                vtp.save()
                            
                        elif attr_code == 'an':
                            vtp.current_armour_nod = F('current_armour_nod') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_armour_nod > vtp.max_armour_nod:
                                vtp.current_armour_nod = vtp.max_armour_nod
                                vtp.save()
                            
                        elif attr_code == 'ep':
                            vtp.current_energy_points = F('current_energy_points') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_energy_points > vtp.max_energy_points:
                                vtp.current_energy_points = vtp.max_energy_points
                                vtp.save()
                            
                        elif attr_code == 'en':
                            vtp.current_energy_nod = F('current_energy_nod') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_energy_nod > vtp.max_energy_nod:
                                vtp.current_energy_nod = vtp.max_energy_nod
                                vtp.save()
                            
                        elif attr_code == 'hop':
                            vtp.current_hope_points = F('current_hope_points') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_hope_points > vtp.max_hope_points:
                                vtp.current_hope_points = vtp.max_hope_points
                                vtp.save()
                            
                        elif attr_code == 'gs':
                            vtp.current_grenade_stock = F('current_grenade_stock') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_grenade_stock > vtp.max_grenade_stock:
                                vtp.current_grenade_stock = vtp.max_grenade_stock
                                vtp.save()
                            
                        elif attr_code == 'ff':
                            vtp.current_force_field = F('current_force_field') + step
                            vtp.save()
                            
                        elif attr_code == 'ae':
                            vtp.current_aegis = F('current_aegis') + step
                            vtp.save()
                            
                        elif attr_code == 'hep':
                            vtp.current_heroism_points = F('current_heroism_points') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_heroism_points > vtp.max_heroism_points:
                                vtp.current_heroism_points = vtp.max_heroism_points
                                vtp.save()
                            
                        elif attr_code == 'flux':
                            vtp.current_flux_points = F('current_flux_points') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_flux_points > vtp.max_flux_points:
                                vtp.current_flux_points = vtp.max_flux_points
                                vtp.save()
                            
                        elif attr_code == 'imp':
                            vtp.current_impregnation_points = F('current_impregnation_points') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_impregnation_points > vtp.max_impregnation_points:
                                vtp.current_impregnation_points = vtp.max_impregnation_points
                                vtp.save()
                            
                        elif attr_code == 'gp':
                            vtp.current_gp = F('current_gp') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_gp > vtp.total_gp:
                                vtp.current_gp = vtp.total_gp
                                vtp.save()
                            
                        elif attr_code == 'xp':
                            vtp.current_xp = F('current_xp') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_xp > vtp.total_xp:
                                vtp.current_xp = vtp.total_xp
                                vtp.save()
                            
                        elif attr_code == 'pard':
                            if vtp.time_paradox_difficulty is None:
                                vtp.time_paradox_difficulty = 1
                            else:
                                vtp.time_paradox_difficulty = F('time_paradox_difficulty') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.time_paradox_difficulty < 0:
                                vtp.time_paradox_difficulty = 0
                                vtp.save()
                            
                        elif attr_code == 'impd':
                            if vtp.impregnation_difficulty is None:
                                vtp.impregnation_difficulty = 1
                            else:
                                vtp.impregnation_difficulty = F('impregnation_difficulty') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.impregnation_difficulty < 0:
                                vtp.impregnation_difficulty = 0
                                vtp.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh",
                            "vtp_to_load": vtp.id
                        }
                    }
                )
            elif command == "status":
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_status',
                        'params': {
                            'user': self.scope['user'].id,
                            'counter': text_data_json['counter'],
                        }
                    }
                )
            elif command == "hurt":
                # import pdb; pdb.set_trace()
                
                logger.info('[%s] [HURT] JSON: %s ' % (command_id, text_data))
                
                vtp_ids = text_data_json['vtps']
                
                if len(vtp_ids) == 0:
                    logger.error('[%s] [HURT] no vtp found' % (command_id,))
                    raise Exception('Damage without targeted vtp are not possible', 'damage_no_vtp')
                
                vtps = VirtualTablePlayer.objects.filter(pk__in=vtp_ids, virtual_table=self.vt_id, virtual_table__game_master__id=self.scope['user'].id)
                
                if len(vtp_ids) != len(vtps):
                    logger.error('[%s] [HURT] GM %d tried to damage unknown players %s ' % (command_id, self.scope['user'].id, vtp_ids))
                    raise Exception("Damage can be sent only to vtp under this virtual table and by the game master of this table", "damage_vtp_unknown_to_vt")
                
                try:
                    ignore_ff = text_data_json['ignore-ff']
                except KeyError:
                    ignore_ff = False
                except Exception as e:
                    logger.critical('[%s] [HURT] unknown exception on ignore ff %s' % (command_id, str(e)))
                    raise e
                
                try:
                    ignore_armour = text_data_json['ignore-armour']
                except KeyError:
                    ignore_armour = False
                except Exception as e:
                    logger.critical('[%s] [HURT] unknown exception on ignore armour %s' % (command_id, str(e)))
                    raise e
                
                try:
                    aegis_active = text_data_json['aegis-active']
                except KeyError:
                    aegis_active = False
                except Exception as e:
                    logger.critical('[%s] [HURT] unknown exception on aegis active %s' % (command_id, str(e)))
                    raise e
                
                try:
                    armour_piercing = text_data_json['armour-piercing']
                except KeyError:
                    armour_piercing = 0
                except Exception as e:
                    logger.critical('[%s] [HURT] unknown exception on armour piercing %s' % (command_id, str(e)))
                    raise e
                
                try:
                    penetrating = text_data_json['penetrating']
                except KeyError:
                    penetrating = 0
                except Exception as e:
                    logger.critical('[%s] [HURT] unknown exception on penetrating %s' % (command_id, str(e)))
                    raise e
                
                try:
                    target = text_data_json['target']
                except KeyError:
                    target = 'hp'
                except Exception as e:
                    logger.critical('[%s] [HURT] unknown exception on target %s' % (command_id, str(e)))
                    raise e
                
                try:
                    recoverable = text_data_json['recoverable']
                except KeyError:
                    recoverable = False
                except Exception as e:
                    logger.critical('[%s] [HURT] unknown exception on recoverable %s' % (command_id, str(e)))
                    raise e
                
                try:
                    value = text_data_json['value']
                except KeyError:
                    raise Exception("No damage output given", "no_damage")
                except Exception as e:
                    logger.critical('[%s] [HURT] unknown exception on value %s' % (command_id, str(e)))
                    raise e
                
                if not isinstance(value, int):
                    logger.error('[%s] [HURT] value "%s" is not an integer ' % (command_id, value))
                    raise Exception("The damage value must be an integer", "damage_not_int")
                
                if not isinstance(armour_piercing, int):
                    armour_piercing = 0
                    
                if not isinstance(penetrating, int):
                    penetrating = 0
                
                for vtp in vtps:
                    damage = value
                    logger.debug('[%s] [HURT] [VTP: %d] object: %s ' % (command_id, vtp.id, model_to_dict(vtp)))
                    logger.debug('[%s] [HURT] [VTP: %d] initial damage: %d ' % (command_id, vtp.id, damage))
                    
                    logger.debug('[%s] [HURT] [VTP: %d] ignore force field? %s ' % (command_id, vtp.id, ignore_ff))
                    if not ignore_ff:
                        force_field = vtp.current_force_field
                        
                        if force_field is None:
                            force_field = vtp.default_force_field
                        
                        if force_field is None:
                            force_field = 0
                        
                        logger.debug('[%s] [HURT] [VTP: %d] final force field: %s ' % (command_id, vtp.id, force_field))
                        # if force field is lesser or equal to the penetrating score of the damage, it will be purely ignored.
                        if penetrating > 0 and force_field <= penetrating:
                            logger.debug('[%s] [HURT] [VTP: %d] force field <= penetrating[=%d] so force field is now 0' % (command_id, vtp.id, penetrating))
                            force_field = 0
                        
                        logger.debug('[%s] [HURT] [VTP: %d] max(damage[=%d] - force_field[=%d], 0) ' % (command_id, vtp.id, damage, force_field))
                        damage = max(damage - force_field, 0)
                        logger.debug('[%s] [HURT] [VTP: %d] new damage value: %d ' % (command_id, vtp.id, damage))
                    
                    logger.debug('[%s] [HURT] [VTP: %d] aegis active? %s ' % (command_id, vtp.id, aegis_active))
                    if aegis_active:
                        aegis = vtp.current_aegis
                        
                        if aegis is None:
                            aegis = vtp.default_aegis
                        
                        if aegis is None:
                            aegis = 0
                        
                        logger.debug('[%s] [HURT] [VTP: %d] final aegis: %s ' % (command_id, vtp.id, aegis))
                        logger.debug('[%s] [HURT] [VTP: %d] max(damage[=%d] - aegis[=%d], 0) ' % (command_id, vtp.id, damage, aegis))
                        damage = max(damage - aegis, 0)
                        logger.debug('[%s] [HURT] [VTP: %d] new damage value: %d ' % (command_id, vtp.id, damage))
                    
                    if target == 'ap':
                        current_armour = vtp.current_armour_points
                        logger.debug('[%s] [HURT] [VTP: %d] max(current_armour_points[=%d] - damage[=%d], 0) ' % (command_id, vtp.id, vtp.current_armour_points, damage))
                        vtp.current_armour_points = min(max(vtp.current_armour_points - damage, 0), vtp.max_armour_points) # if armour is fully destroyed, it must be set to 0. In case of negative damage to restore ap, you don't want to exceed max ap
                        logger.debug('[%s] [HURT] [VTP: %d] new armour value: %d ' % (command_id, vtp.id, vtp.current_armour_points))
                        
                        logger.debug('[%s] [HURT] [VTP: %d] ignore_health_loss_by_armour? %s ' % (command_id, vtp.id, vtp.ignore_health_loss_by_armour))
                        if not vtp.ignore_health_loss_by_armour:
                            logger.debug('[%s] [HURT] [VTP: %d] max(current_health_points[=%d] - ((current_armour[=%d] - current_armour_points[=%d]) // 5), 0) ' % (command_id, vtp.id, vtp.current_health_points, current_armour, vtp.current_armour_points))
                            vtp.current_health_points = min(max(vtp.current_health_points - ((current_armour - vtp.current_armour_points) // 5), 0), vtp.max_health_points) # in case of negative damage to restore health, you don't want to exceed max hp
                            logger.debug('[%s] [HURT] [VTP: %d] new health value: %d ' % (command_id, vtp.id, vtp.current_health_points))
                    else:
                        logger.debug('[%s] [HURT] [VTP: %d] ignore armour? %s ' % (command_id, vtp.id, ignore_armour))
                        if not ignore_armour:
                            # Armour take damage before the target
                            armour = vtp.current_armour_points
                            logger.debug('[%s] [HURT] [VTP: %d] initial armour: %d ' % (command_id, vtp.id, armour))
                            logger.debug('[%s] [HURT] [VTP: %d] armour_piercing: %d ' % (command_id, vtp.id, armour_piercing))
                            if armour_piercing > 0 and armour <= armour_piercing:
                                logger.debug('[%s] [HURT] [VTP: %d] effective pierce armour!' % (command_id, vtp.id))
                                # the pierce armour will be effective
                                armour_damage = 0
                                # if armour > armour_piercing:
                                #     logger.debug('[%s] [HURT] [VTP: %d] armour damaged despite pierce armour!' % (command_id, vtp.id))
                                #     armour_damage = armour - armour_piercing
                                # final_damage = damage - armour_damage
                                final_damage = damage
                            else:
                                if armour - damage < 0:
                                    armour_damage = armour
                                    final_damage = damage - armour_damage
                                else:
                                    armour_damage = damage
                                    final_damage = 0
                        else:
                            # Damage are directly done to the target
                            armour_damage = 0
                            final_damage = damage
                        
                        logger.debug('[%s] [HURT] [VTP: %d] armour_damage: %d - final_damage: %d' % (command_id, vtp.id, armour_damage, final_damage))
                        logger.debug('[%s] [HURT] [VTP: %d] ignore_health_loss_by_armour? %s ' % (command_id, vtp.id, vtp.ignore_health_loss_by_armour))
                        if not vtp.ignore_health_loss_by_armour:
                            logger.debug('[%s] [HURT] [VTP: %d] max(current_health_points[=%d] - ((armour_damage[=%d]) // 5), 0) ' % (command_id, vtp.id, vtp.current_health_points, armour_damage))
                            vtp.current_health_points = min(max(vtp.current_health_points - (armour_damage // 5), 0), vtp.max_health_points)
                            logger.debug('[%s] [HURT] [VTP: %d] new health value: %d ' % (command_id, vtp.id, vtp.current_health_points))
                        
                        if target == 'hp':
                            vtp.current_armour_points = min(vtp.current_armour_points - armour_damage, vtp.max_armour_points)
                            logger.debug('[%s] [HURT] [VTP: %d] max(current_health_points[=%d] - ((armour_damage[=%d]) // 5), 0) ' % (command_id, vtp.id, vtp.current_health_points, armour_damage))
                            vtp.current_health_points = min(max(vtp.current_health_points - final_damage, 0), vtp.max_health_points)
                            logger.debug('[%s] [HURT] [VTP: %d] new health value: %d ' % (command_id, vtp.id, vtp.current_health_points))
                        elif target == 'ep':
                            vtp.current_armour_points = min(vtp.current_armour_points - armour_damage, vtp.max_armour_points)
                            
                            logger.debug('[%s] [HURT] [VTP: %d] target energy recoverable? %s ' % (command_id, vtp.id, recoverable))
                            if recoverable:
                                if vtp.current_energy_points > final_damage:
                                    vtp.lost_energy_counter = max(vtp.lost_energy_counter + final_damage, 0)
                                else:
                                    vtp.lost_energy_counter += min(vtp.current_energy_points, vtp.max_energy_points)
                                logger.debug('[%s] [HURT] [VTP: %d] new energy lost counter: %d ' % (command_id, vtp.id, vtp.lost_energy_counter))
                            
                            vtp.current_energy_points = min(max(vtp.current_energy_points - final_damage, 0), vtp.max_energy_points)
                            logger.debug('[%s] [HURT] [VTP: %d] new energy value: %d ' % (command_id, vtp.id, vtp.current_energy_points))
                        elif target == 'hop':
                            vtp.current_armour_points = min(vtp.current_armour_points - armour_damage, vtp.max_armour_points)
                            
                            logger.debug('[%s] [HURT] [VTP: %d] target hope recoverable? %s ' % (command_id, vtp.id, recoverable))
                            if recoverable:
                                if vtp.current_hope_points > final_damage:
                                    vtp.lost_hope_counter = max(vtp.lost_hope_counter + final_damage, 0)
                                else:
                                    vtp.lost_hope_counter += min(vtp.current_hope_points, vtp.max_hope_points)
                                logger.debug('[%s] [HURT] [VTP: %d] new hope lost counter: %d ' % (command_id, vtp.id, vtp.lost_hope_counter))
                            
                            vtp.current_hope_points = min(max(vtp.current_hope_points - final_damage, 0), vtp.max_hope_points)
                            logger.debug('[%s] [HURT] [VTP: %d] new hope value: %d ' % (command_id, vtp.id, vtp.current_hope_points))
                        else:
                            raise Exception('Unknown target for damage', 'damage_unknown_target')
                    
                    vtp.save()
                    
                    # Send message to room group
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "refresh",
                                "vtp_to_load": vtp.id
                            }
                        }
                    )
            elif command == "toggle":
                attr = text_data_json['attr']
                vtp_id = text_data_json['vtp']
                
                if vtp_id is None:
                    raise Exception('Invalid parameters', 'invalid')
                
                vtp_id = int(vtp_id)
                
                vtp = VirtualTablePlayer.objects.get(pk=vtp_id)
                
                if vtp.virtual_table.id != self.vt_id:
                    # import pdb; pdb.set_trace()
                    raise Exception('Incoherent parameters', 'incoherent')
                
                if vtp.virtual_table.game_master.id != self.scope['user'].id:
                    raise Exception('You have no rights to change this data', 'unauthorized_toggle')
                
                if attr == 'rights':
                    try:
                        right = text_data_json['value']
                    except KeyError:
                        raise Exception('Missing access right', 'missing_access_right')
                    except Exception as e:
                        raise e
                    
                    if right not in (vtp.EDIT_LVL_ALL, vtp.EDIT_LVL_GM, vtp.EDIT_LVL_PLAYER):
                        raise ValueError('Unkown access right', 'unknown_access_right')
                    
                    vtp.edit_level = right
                elif attr == 'gest_advantage':
                    try:
                        gest_advantage = text_data_json['value']
                    except KeyError:
                        raise Exception('Missing gest advantage', 'missing_access_gest_advantage')
                    except Exception as e:
                        raise e
                    
                    if gest_advantage not in (vtp.GEST_ADVANTAGE_BALANCE, vtp.GEST_ADVANTAGE_LIGHT, vtp.GEST_ADVANTAGE_NONE):
                        raise ValueError('Unkown gest_advantage', 'unknown_access_gest_advantage')
                    
                    vtp.gest_advantage = gest_advantage
                elif attr == 'ignore_health_loss':
                    try:
                        value = text_data_json['value']
                    except KeyError:
                        value = False
                    except Exception as e:
                        raise e
                    
                    vtp.ignore_health_loss_by_armour = value
                else:
                    raise Exception("Not known commande for toggle", "unknown_toggle_command")
                
                vtp.save()
                vtp = VirtualTablePlayer.objects.annotate(
                    editable_by_user=Case(
                        When(edit_level=VirtualTablePlayer.EDIT_LVL_ALL, then=True),
                        When(edit_level=VirtualTablePlayer.EDIT_LVL_PLAYER, player=self.scope['user'], then=True),
                        When(virtual_table__game_master=self.scope['user'], then=True),
                        default=False,
                        output_field=BooleanField()
                    )
                ).get(pk=vtp.id)
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh",
                            "vtp_to_load": vtp.id
                        }
                    }
                )
            elif command == "combo":
                # import pdb; pdb.set_trace()
                
                vtp = None
                gm_roll = text_data_json['gm_roll']
                logger.info('[%s] [COMBO] JSON: %s ' % (command_id, text_data))
                
                reroll_failure = False
                
                if gm_roll:
                    vt = VirtualTable.objects.get(pk=self.vt_id)
                    if vt.game_master.id != self.scope['user'].id:
                        logger.error('[%s] [COMBO] %d tried a GM roll (%d) ' % (command_id, self.scope['user'].id, vt.game_master.id))
                        raise Exception('You can not do a gm roll if you are not the GM of the table', 'unauthorized_gm_combo_roll')
                else:
                    vtp_id = text_data_json['vtp']
                    
                    if vtp_id is None:
                        logger.error('[%s] [COMBO] no VTP found ' % command_id)
                        raise Exception('Invalid parameters', 'invalid')
                    
                    vtp_id = int(vtp_id)
                    
                    vtp = VirtualTablePlayer.objects.get(pk=vtp_id)
                    
                    if vtp.virtual_table.id != self.vt_id:
                        logger.error('[%s] [COMBO] VTP %d do not belong to the VT %d ' % (command_id, vtp.id, self.vt_id))
                        raise Exception('Incoherent parameters', 'incoherent')
                    
                    if vtp.gest_advantage == vtp.GEST_ADVANTAGE_BALANCE:
                        reroll_failure = True
                
                try:
                    dice_number = text_data_json['dice_number']
                except KeyError:
                    dice_number = 0
                except Exception as e:
                    logger.critical('[%s] [COMBO] unknown exception on dice number %s' % (command_id, str(e)))
                    raise e
                
                try:
                    overdrive_value = text_data_json['overdrive_value']
                except KeyError:
                    overdrive_value = 0
                except Exception as e:
                    logger.critical('[%s] [COMBO] unknown exception on overdrive value %s' % (command_id, str(e)))
                    raise e
                
                try:
                    support = text_data_json['support']
                except KeyError:
                    support = False
                except Exception as e:
                    logger.critical('[%s] [COMBO] unknown exception on support value %s' % (command_id, str(e)))
                    raise e
                
                if not isinstance(dice_number, int):
                    dice_number = 0
                
                if not isinstance(overdrive_value, int):
                    overdrive_value = 0
                
                if dice_number > 999:
                    dice_number = 999
                elif dice_number < 0:
                    dice_number = 0
                
                if overdrive_value > 999:
                    overdrive_value = 999
                elif overdrive_value < 0:
                    overdrive_value = 0
                    
                response = {
                    'success': 0,
                    'exploit': False,
                    'fail': False,
                    'dice': dice_number,
                    'od': overdrive_value,
                    'detail': {
                        2: {
                            'class': 'dice-two',
                            'count': 0
                        },
                        4: {
                            'class': 'dice-four',
                            'count': 0
                        },
                        6: {
                            'class': 'dice-six',
                            'count': 0
                        },
                        1: {
                            'class': 'dice-one',
                            'count': 0
                        },
                        3: {
                            'class': 'dice-three',
                            'count': 0
                        },
                        5: {
                            'class': 'dice-five',
                            'count': 0
                        }
                    },
                }
                
                if dice_number > 0:
                    try:
                        dice_type = 'd6'
                        
                        vt = VirtualTable.objects.get(pk=self.vt_id)
                        
                        if vt.destiny_state == 'curse':
                            dice_type = 'd[1-1]'
                        elif vt.destiny_state == 'bless':
                            dice_type = 'd[6-6]'
                        
                        dice_command = '%(dn)d%(dt)sc[%%2=0]i:[=0]{0}{i:[=%(dn)d]{+%(dn)dd6c[%%2=0]+%(od)d}{+%(od)d}}' % {'dn': dice_number, 'od': overdrive_value, 'dt': dice_type}
                        if support:
                            dice_command = '%(dn)d%(dt)sc[%%2=0]' % {'dn': dice_number, 'od': overdrive_value, 'dt': dice_type}
                        elif reroll_failure:
                            # command line: ${COMBO}d6;\$1c[%2=0];\$1c[%2=1];\$3i:[>0]{\$3d6}{0}c[%2=0];\$2+\$4;\$5i:[=${COMBO}]{${COMBO}d6c[%2=0]}{0};\$5i:[=0]{0}{\$6+\$5+${OD}}
                            # full explanation:
                            #                instruction                -                                        description                                        --         commentary
                            # ${COMBO}d6;                               - 1. roll COMBO d6                                                                          -- initial roll
                            # \$1c[%2=0];                               - 2. on instruction 1, count even                                                           -- INITIAL SUCCESS
                            # \$1c[%2=1];                               - 3. on instruction 1, count odd                                                            -- tmp
                            # \$3i:[>0]{\$3d6}{0}c[%2=0];               - 4. if instruction 3 > 0 then roll [instruction 3]D6, else roll 0 and anyway count even    -- BALANCE KNIGHT SUCCESS
                            # \$2+\$4;                                  - 5. add instruction 2 and instruction 4                                                    -- initial + balance
                            # \$5i:[=${COMBO}]{${COMBO}d6c[%2=0]}{0};   - 6. if instruction 5 = COMBO then roll COMBO d6, count even, else 0                        -- exploit roll
                            # \$5i:[=0]{0}{\$6+\$5+${OD}}               - 7. if instruction 5 = 0 then 0 (critical fail), else add instructions 6, 5 and OD         -- final count
                            dice_command = '%(dn)d%(dt)s;$1c[%%2=0];$1c[%%2=1];$3i:[>0]{$3%(dt)s}{0}c[%%2=0];$2+$4;$5i:[=%(dn)d]{%(dn)dd6c[%%2=0]}{0};$5i:[=0]{0}{$6+$5+%(od)d}' % {'dn': dice_number, 'od': overdrive_value, 'dt': dice_type}
                        
                        logger.debug(dice_command)
                        byte_output = subprocess.check_output(['dice', '-j', dice_command])
                    except subprocess.CalledProcessError as e:
                        logger.critical('[%s] [COMBO] exec command failed, maybe binary is not installed? %s' % (command_id, str(e)))
                        raise e
                    except Exception as e:
                        logger.critical('[%s] [COMBO] unknown exception on exec command %s' % (command_id, str(e)))
                        raise e
                    
                    result_object = json.loads(byte_output.decode())
                    logger.debug(result_object)
                    
                    last_scalar = result_object['scalar'].split(',')[-1]
                    response['success'] = int(last_scalar)
                    response['support'] = support
                    
                    if not support:
                        if response['success'] > 0 and response['success'] - overdrive_value >= dice_number:
                            response['exploit'] = True
                        
                        if response['success'] == 0:
                            response['fail'] = True
                    
                    first_roll = True
                    if not(result_object.get('values') is None):
                        for diceroll in result_object['values']:
                            for dice in diceroll['values']:
                                # Ignore an even dice if it's on the first roll of a non-support knight of balance... :)
                                if (reroll_failure
                                  and first_roll
                                  and dice['total'] % 2 == 1
                                  and not support):
                                    continue

                                response['detail'][dice['total']]['count'] = response['detail'][dice['total']]['count'] + 1

                            first_roll = False
                    else:
                        for diceroll in result_object['instructions']:
                            for dice in diceroll['diceval']:
                                # Ignore an even dice if it's on the first roll of a non-support knight of balance... :)
                                if (reroll_failure
                                        and first_roll
                                        and dice['value'] % 2 == 1
                                        and not support):
                                    continue

                                response['detail'][dice['value']]['count'] = response['detail'][dice['value']][
                                                                                 'count'] + 1

                            first_roll = False
                    
                    logger.info('[%s] [COMBO] result: %s' % (command_id, json.dumps(response)))
                    
                    roll = VirtualTableRoll()
                    roll.datetime = timezone.now()
                    roll.response_json = json.dumps(response)
                    roll.combo = True
                    roll.public = text_data_json['public']
                    
                    if gm_roll:
                        roll.virtual_table_gm = vt
                    else:
                        roll.virtual_table_player = vtp
                    
                    roll.save()
                    VirtualTableRoll.objects.filter(datetime__lte=timezone.now()-timezone.timedelta(hours=12)).delete()
                
                # Send message to room group
                if gm_roll:
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "combo_gm_result",
                                "public": text_data_json['public'],
                                "response": response,
                            }
                        }
                    )
                else:
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "combo_result",
                                "public": text_data_json['public'],
                                "vtp": model_to_dict(vtp),
                                "response": response,
                            }
                        }
                    )
            elif command == "damage":
                # import pdb; pdb.set_trace()
                
                vtp = None
                gm_roll = text_data_json['gm_roll']
                logger.info('[%s] [DAMAGE] JSON: %s ' % (command_id, text_data))
                
                force_low_result_to_4 = False
                if gm_roll:
                    vt = VirtualTable.objects.get(pk=self.vt_id)
                    if vt.game_master.id != self.scope['user'].id:
                        logger.error('[%s] [DAMAGE] %d tried a GM roll (%d) ' % (command_id, self.scope['user'].id, vt.game_master.id))
                        raise Exception('You can not do a gm roll if you are not the GM of the table', 'unauthorized_gm_combo_roll')
                else:
                    vtp_id = text_data_json['vtp']
                    
                    if vtp_id is None:
                        logger.error('[%s] [DAMAGE] no VTP found ' % command_id)
                        raise Exception('Invalid parameters', 'invalid')
                    
                    vtp_id = int(vtp_id)
                    
                    vtp = VirtualTablePlayer.objects.get(pk=vtp_id)
                    
                    if vtp.virtual_table.id != self.vt_id:
                        logger.error('[%s] [DAMAGE] VTP %d do not belong to the VT %d ' % (command_id, vtp.id, self.vt_id))
                        raise Exception('Incoherent parameters', 'incoherent')
                    
                    if vtp.gest_advantage == vtp.GEST_ADVANTAGE_LIGHT:
                        force_low_result_to_4 = True
                    
                try:
                    dice_number = text_data_json['dice_number']
                except KeyError:
                    dice_number = 0
                except Exception as e:
                    logger.critical('[%s] [DAMAGE] unknown exception on dice number %s' % (command_id, str(e)))
                    raise e
                
                try:
                    bonus_value = text_data_json['bonus_value']
                except KeyError:
                    bonus_value = 0
                except Exception as e:
                    logger.critical('[%s] [DAMAGE] unknown exception on bonus value %s' % (command_id, str(e)))
                    raise e
                
                if not isinstance(dice_number, int):
                    dice_number = 0
                
                if not isinstance(bonus_value, int):
                    bonus_value = 0
                
                if dice_number > 999:
                    dice_number = 999
                elif dice_number < 0:
                    dice_number = 0
                
                if bonus_value > 999:
                    bonus_value = 999
                elif bonus_value < -999:
                    bonus_value = -999
                
                response = {
                    'damage': bonus_value,
                    'dice': dice_number,
                    'bonus': bonus_value,
                    'detail': {
                        1: {
                            'class': 'dice-one',
                            'count': 0
                        },
                        2: {
                            'class': 'dice-two',
                            'count': 0
                        },
                        3: {
                            'class': 'dice-three',
                            'count': 0
                        },
                        4: {
                            'class': 'dice-four',
                            'count': 0
                        },
                        5: {
                            'class': 'dice-five',
                            'count': 0
                        },
                        6: {
                            'class': 'dice-six',
                            'count': 0
                        },
                    },
                }
                
                if dice_number > 0:
                    try:
                        dice_command = '%(dice)dd6+%(bonus)d' % {'dice': dice_number, 'bonus': bonus_value}
                        logger.debug(dice_command)
                        byte_output = subprocess.check_output(['dice', '-j', dice_command])
                    except subprocess.CalledProcessError as e:
                        logger.critical('[%s] [DAMAGE] exec command failed, maybe binary is not installed? %s' % (command_id, str(e)))
                        raise e
                    except Exception as e:
                        logger.critical('[%s] [DAMAGE] unknown exception on exec command %s' % (command_id, str(e)))
                        raise e
                    
                    result_object = json.loads(byte_output.decode())
                    logger.debug(result_object)
                    
                    response['damage'] = int(result_object['scalar'])
                    response['stronger_damage'] = int(result_object['scalar'])

                    if not (result_object.get('values') is None):
                        for dice in result_object['values'][0]['values']:
                            response['detail'][dice['total']]['count'] = response['detail'][dice['total']]['count'] + 1
                            val = int(dice['total'])
                            if force_low_result_to_4 and val < 4:
                                response['stronger_damage'] = response['stronger_damage'] + (4 - val)
                    else:
                        for dice in result_object['instructions'][0]['diceval']:
                            response['detail'][dice['value']]['count'] = response['detail'][dice['value']]['count'] + 1
                            val = int(dice['value'])
                            if force_low_result_to_4 and val < 4:
                                response['stronger_damage'] = response['stronger_damage'] + (4 - val)

                    logger.info('[%s] [DAMAGE] result: %s' % (command_id, json.dumps(response)))
                    
                    roll = VirtualTableRoll()
                    roll.datetime = timezone.now()
                    roll.response_json = json.dumps(response)
                    roll.combo = False
                    roll.public = text_data_json['public']
                    
                    if gm_roll:
                        roll.virtual_table_gm = vt
                    else:
                        roll.virtual_table_player = vtp
                    
                    roll.save()
                    VirtualTableRoll.objects.filter(datetime__lte=timezone.now()-timezone.timedelta(hours=12)).delete()
                
                # Send message to room group
                if gm_roll:
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "damage_gm_result",
                                "public": text_data_json['public'],
                                "response": response,
                            }
                        }
                    )
                else:
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "damage_result",
                                "public": text_data_json['public'],
                                "vtp": model_to_dict(vtp),
                                "response": response,
                            }
                        }
                    )
            elif command == "destiny_card":
                # import pdb; pdb.set_trace()
                
                vtp_id = text_data_json['vtp']
                logger.info('[%s] [DESTINY] JSON: %s ' % (command_id, text_data))
                
                if vtp_id is None:
                    raise Exception('Invalid parameters', 'invalid')
                
                vtp_id = int(vtp_id)
                
                vtp = VirtualTablePlayer.objects.get(pk=vtp_id)
                
                if vtp.virtual_table.id != self.vt_id:
                    raise Exception('Incoherent parameters', 'incoherent')
                    
                arcana = MajorArcana.objects.order_by('?').first()
                
                final_arcana = {
                    "number": arcana.number,
                    "roman_number": arcana.roman_number,
                    "name": arcana.name,
                    "destiny_effect": markdown(arcana.destiny_effect),
                }
                logger.info('[%s] [DESTINY] result: %d - %s' % (command_id, arcana.number, arcana.name))
                
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "destiny_result",
                            "arcana": final_arcana,
                            "vtp": model_to_dict(vtp),
                        }
                    }
                )
            elif command == "recover":
                vtp_ids = text_data_json['vtps']
                
                if len(vtp_ids) == 0:
                    raise Exception('Recover without targeted vtp are not possible', 'recover_no_vtp')
                
                vtps = VirtualTablePlayer.objects.filter(pk__in=vtp_ids, virtual_table=self.vt_id, virtual_table__game_master__id=self.scope['user'].id)
                
                if len(vtp_ids) != len(vtps):
                    raise Exception("Recover can be sent only to vtp under this virtual table and by the game master of this table", "recover_vtp_unknown_to_vt")
                
                for vtp in vtps:
                    restore_vtp_counters(vtp)
                    
                    # Send message to room group
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "refresh",
                                "vtp_to_load": vtp.id
                            }
                        }
                    )
            elif command == "end_battle":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                if vt.game_master.id != self.scope['user'].id:
                    logger.error('[%s] [END_BATTLE] %d tried to end battle ' % (command_id, self.scope['user'].id))
                    raise Exception('You can not end battle if you are not the GM of the table', 'unauthorized_gm_end_battle')
                
                for vtp in vt.players.all():
                    restore_vtp_counters(vtp)
                    
                    vtp.current_flux_points = 0
                    vtp.time_paradox_difficulty = 0
                    
                    vtp.save()
                    
                    # Send message to room group
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "refresh",
                                "vtp_to_load": vtp.id
                            }
                        }
                    )
            elif command == "end_mission":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                if vt.game_master.id != self.scope['user'].id:
                    logger.error('[%s] [END_MISSION] %d tried to end mission ' % (command_id, self.scope['user'].id))
                    raise Exception('You can not end mission if you are not the GM of the table', 'unauthorized_gm_end_mission')
                
                try:
                    rewards = text_data_json['rewards']
                except KeyError:
                    rewards = {}
                except Exception as e:
                    raise e
                
                try:
                    restore_reset = text_data_json['restore_reset']
                except KeyError:
                    restore_reset = []
                except Exception as e:
                    raise e
                
                for vtp in vt.players.all():
                    
                    if 'regen-recoverable-damage' in restore_reset:
                        restore_vtp_counters(vtp)
                    
                    if 'restore-health-points' in restore_reset:
                        vtp.current_health_points = vtp.max_health_points
                    if 'restore-armour-points' in restore_reset:
                        vtp.current_armour_points = vtp.max_armour_points
                    if 'restore-energy-points' in restore_reset:
                        vtp.current_energy_points = vtp.max_energy_points
                    if 'restore-health-nod' in restore_reset:
                        vtp.current_health_nod = vtp.max_health_nod
                    if 'restore-armour-nod' in restore_reset:
                        vtp.current_armour_nod = vtp.max_armour_nod
                    if 'restore-energy-nod' in restore_reset:
                        vtp.current_energy_nod = vtp.max_energy_nod
                    if 'restore-grenade-stock' in restore_reset:
                        vtp.current_grenade_stock = vtp.max_grenade_stock
                        
                    
                    if 'reset-flux-points' in restore_reset:
                        vtp.current_flux_points = 0
                    if 'reset-time-paradox-difficulty' in restore_reset:
                        vtp.time_paradox_difficulty = 0
                    if 'reset-impregnation-difficulty' in restore_reset:
                        vtp.impregnation_difficulty = 0
                    
                    # Managing GP and XP here
                    try:
                        vtp_reward = rewards[str(vtp.id)]
                        
                        try:
                            vtp.current_gp = vtp.current_gp + vtp_reward['gp']
                            vtp.total_gp = vtp.total_gp + vtp_reward['gp']
                        except KeyError:
                            # strange, but meh.
                            pass
                        except Exception as e:
                            raise e
                        
                        try:
                            vtp.current_xp = vtp.current_xp + vtp_reward['xp']
                            vtp.total_xp = vtp.total_xp + vtp_reward['xp']
                        except KeyError:
                            # strange, but meh.
                            pass
                        except Exception as e:
                            raise e
                        
                        try:
                            vtp.current_heroism_points = vtp.current_heroism_points + vtp_reward['hep']
                            if vtp.current_heroism_points > vtp.max_heroism_points:
                                vtp.current_heroism_points = vtp.max_heroism_points
                        except KeyError:
                            # strange, but meh.
                            pass
                        except Exception as e:
                            raise e
                            
                    except KeyError:
                        pass
                    except Exception as e:
                        raise e
                        
                    vtp.save()
                    
                    # Send message to room group
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "refresh",
                                "vtp_to_load": vtp.id
                            }
                        }
                    )
            elif command == "recap":
                vtp_ids = text_data_json['vtps']
                
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                for vtp in vt.players.all():
                    # import pdb; pdb.set_trace()
                    if vtp.id in vtp_ids: 
                        async_to_sync(self.channel_layer.group_send)(
                            self.vt_group_name,
                            {
                                'type': 'vt_message',
                                'params': {
                                    "command": "refresh",
                                    "vtp_to_load": vtp.id,
                                    "self_target": True,
                                    "origin": self.scope['user'].id
                                }
                            }
                        )
                        vtp_ids.remove(vtp.id)
                    else:
                        async_to_sync(self.channel_layer.group_send)(
                            self.vt_group_name,
                            {
                                'type': 'vt_message',
                                'params': {
                                    "command": 'new',
                                    "vtp_id": vtp.id,
                                    "self_target": True,
                                    "origin": self.scope['user'].id
                                }
                            }
                        )
                
                if len(vtp_ids) > 0:
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": 'del',
                                "vtp": vtp_ids,
                                "self_target": True,
                                "origin": self.scope['user'].id
                            }
                        }
                    )
            elif command == "config":
                classes_to_show = text_data_json['classes_to_show']
                vtp_id = text_data_json['vtp']
                
                if vtp_id is None:
                    raise Exception('Invalid parameters', 'invalid')
                
                vtp_id = int(vtp_id)
                
                vtp = VirtualTablePlayer.objects.get(pk=vtp_id)
                
                if vtp.virtual_table.id != self.vt_id:
                    # import pdb; pdb.set_trace()
                    raise Exception('Incoherent parameters', 'incoherent')
                
                if ((vtp.edit_level == vtp.EDIT_LVL_PLAYER
                  and vtp.player.id != self.scope['user'].id
                  and vtp.virtual_table.game_master.id != self.scope['user'].id)
                  or (vtp.edit_level == vtp.EDIT_LVL_GM
                  and vtp.virtual_table.game_master.id != self.scope['user'].id)):
                    raise Exception('You have no rights to change this data', 'unauthorized_config')
                
                vtp.classes_to_show = classes_to_show
                vtp.save()
                # import pdb; pdb.set_trace()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh",
                            "vtp_to_load": vtp.id
                        }
                    }
                )
                
                vtp_ids = text_data_json['vtps']
                
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                for vtp in vt.players.all():
                    # import pdb; pdb.set_trace()
                    if vtp.id in vtp_ids: 
                        async_to_sync(self.channel_layer.group_send)(
                            self.vt_group_name,
                            {
                                'type': 'vt_message',
                                'params': {
                                    "command": "refresh",
                                    "vtp_to_load": vtp.id,
                                    "self_target": True,
                                    "origin": self.scope['user'].id
                                }
                            }
                        )
                        vtp_ids.remove(vtp.id)
                    else:
                        async_to_sync(self.channel_layer.group_send)(
                            self.vt_group_name,
                            {
                                'type': 'vt_message',
                                'params': {
                                    "command": 'new',
                                    "vtp_id": vtp.id,
                                    "self_target": True,
                                    "origin": self.scope['user'].id
                                }
                            }
                        )
                
                if len(vtp_ids) > 0:
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": 'del',
                                "vtp": vtp_ids,
                                "self_target": True,
                                "origin": self.scope['user'].id
                            }
                        }
                    )
            elif command == "destiny_state":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                if vt.game_master.id != self.scope['user'].id:
                    logger.error('[%s] [DESTINY_STATE] %d tried to change ' % (command_id, self.scope['user'].id))
                    raise Exception('You can not change this value if you are not the GM of the table', 'unauthorized_gm_destiny_state')
                
                try:
                    val = text_data_json['val']
                except KeyError:
                    val = 'none'
                except Exception as e:
                    raise e
                
                vt.destiny_state = val
                
                vt.save()
                
            elif command == "avalon_vote":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                new_ordered_extensions = text_data_json['new_ordered_extensions']
                
                current_weight = len(new_ordered_extensions)
                relative_current_weight = vt.avalon.extensions.filter(built=False).count()
                
                for extension_id in new_ordered_extensions:
                    extension_id = int(extension_id)
                    avalon_extension = vt.avalon.extensions.get(pk=extension_id)
                    
                    try:
                        vote_to_update = VirtualTableAvalonExtensionVote.objects.get(virtual_table_avalon_extension=avalon_extension, user=self.scope['user'])
                    except ObjectDoesNotExist as e:
                        vote_to_update = VirtualTableAvalonExtensionVote()
                        vote_to_update.user = self.scope['user']
                        vote_to_update.virtual_table_avalon_extension = avalon_extension
                    except Exception as e:
                        raise e
                    
                    vote_to_update.weight = current_weight
                    current_weight = current_weight - 1
                    if not avalon_extension.built:
                        vote_to_update.relative_weight = relative_current_weight
                        relative_current_weight = relative_current_weight - 1
                    else:
                        vote_to_update.relative_weight = 0
                    
                    vote_to_update.save()
                    
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh_avalon",
                        }
                    }
                )
            elif command == "avalon_extension_toggle":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                if vt.game_master.id != self.scope['user'].id:
                    logger.error('[%s] [AVALON_TOGGLE] %d tried to toggle an avalon extension ' % (command_id, self.scope['user'].id))
                    raise Exception('You can not toggle avalon extension if you are not the GM of the table', 'unauthorized_gm_toggle_avalon_extension')
                
                extension_id = text_data_json['extension_id']
                
                extension_id = int(extension_id)
                avalon_extension = vt.avalon.extensions.get(pk=extension_id)
                avalon_extension.built = not avalon_extension.built
                avalon_extension.save()
                
                vote_user = None
                max_relative_weight = vt.avalon.extensions.filter(built=False).count()
                
                for vote in VirtualTableAvalonExtensionVote.objects.filter(virtual_table_avalon_extension__virtual_table_avalon=vt.avalon).order_by('user', '-weight').all():
                    if vote_user != vote.user:
                        vote_user = vote.user
                        relative_weight = max_relative_weight
                    
                    if not vote.virtual_table_avalon_extension.built:
                        vote.relative_weight = relative_weight
                        relative_weight = relative_weight - 1
                        vote.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh_avalon",
                        }
                    }
                )
            elif command == "avalon_reset_votes":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                if vt.game_master.id != self.scope['user'].id:
                    logger.error('[%s] [AVALON_VOTE_RESET] %d tried to reset avalon votes ' % (command_id, self.scope['user'].id))
                    raise Exception('You can not reset avalon votes if you are not the GM of the table', 'unauthorized_gm_avalon_vote_reset')
                
                VirtualTableAvalonExtensionVote.objects.filter(virtual_table_avalon_extension__virtual_table_avalon=vt.avalon).delete()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh_avalon",
                        }
                    }
                )
            elif command == "avalon_update_points":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                if vt.game_master.id != self.scope['user'].id:
                    logger.error('[%s] [AVALON_UPDATE_POINTS] %d tried to change avalon construction points ' % (command_id, self.scope['user'].id))
                    raise Exception('You can not update avalon total construction points if you are not the GM of the table', 'unauthorized_gm_avalon_update_points')
                
                points = text_data_json['points']
                
                if not isinstance(points, int):
                    logger.error('[%s] [AVALON_UPDATE_POINTS] construction points "%s" is not an integer, updated to 0.' % (command_id, points))
                    points = 0
                
                vt.avalon.total_construction_points = points
                vt.avalon.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh_avalon",
                        }
                    }
                )
            elif command == "factions_load":
                # Send message to room group
                force_pc_view = False
                try:
                    force_pc_view = text_data_json['force_pc_view']
                except KeyError:
                    pass
                    
                
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh_factions",
                            "origin": self.scope['user'].id,
                            "force_pc_view": force_pc_view,
                        }
                    }
                )
            elif command == "factions_import":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                if vt.game_master.id != self.scope['user'].id:
                    raise Exception("You can not toggle faction if you are not the GM of the table", 'factions_toggle_not_gm')
                
                new_factions = []
                nodes_by_faction = {}
                existing_pc_faction = vt.factions.filter(pc_faction=True).count()
                for faction in Faction.objects.all():
                    try:
                        node = VirtualTableFactionNode.objects.get(faction=faction, virtual_table=vt)
                        logger.debug('[%s] [FACTION] faction node exists: %s' % (command_id,faction.name))
                    except ObjectDoesNotExist as e:
                        logger.debug('[%s] [FACTION] faction node does not exist: %s' % (command_id,faction.name))
                        node = VirtualTableFactionNode()
                        node.virtual_table = vt
                        node.faction = faction
                        node.visible_player = faction.default_visible_player
                        node.public_description = faction.default_public_description
                        node.gm_description = faction.default_gm_description
                        node.logo_file = None
                        node.name = faction.name
                        if existing_pc_faction == 0:
                            node.pc_faction = faction.default_pc_faction
                        else:
                            node.pc_faction = False
                            
                        node.save()
                        new_factions.append(node)
                    except Exception as e:
                        raise e
                    
                    for faction_effect in faction.effects.all():
                        logger.debug('[%s] [FACTION] parsing effect: %d - %s' % (command_id, faction_effect.level, faction_effect.level_name))
                        try:
                            effect = node.effects.get(effect=faction_effect)
                            logger.debug('[%s] [FACTION] already existing effect for the node: %d - %s' % (command_id, effect.level, effect.level_name))
                        except ObjectDoesNotExist as e:
                            effect = VirtualTableFactionNodeEffect()
                            logger.debug('[%s] [FACTION] creating effect for this node' % command_id)
                            effect.node = node
                            effect.effect = faction_effect
                        except Exception as e:
                            raise e
                        
                        effect.level_name = faction_effect.level_name
                        effect.level_memo = faction_effect.level_memo
                        effect.level = faction_effect.level
                        effect.effect_name = faction_effect.effect_name
                        effect.effect_description = faction_effect.effect_description
                        
                        effect.save()
                        logger.debug('[%s] [FACTION] effect saved with pk %d' % (command_id, effect.pk))
                        
                    nodes_by_faction[faction] = node
                    
                for faction in new_factions:
                    for default_edge in FactionDefaultEdge.objects.filter(Q(node_source=faction.faction) | Q(node_target=faction.faction)).all():
                        if VirtualTableFactionEdge.objects.filter(node_source=nodes_by_faction[default_edge.node_source], node_target=nodes_by_faction[default_edge.node_target]).count() > 0:
                            logger.debug('[%s] [FACTION] already existing edge between %s and %s' % (command_id, default_edge.node_source, default_edge.node_target))
                            continue
                            
                        logger.debug('[%s] [FACTION] creating edge between %s and %s' % (command_id, default_edge.node_source, default_edge.node_target))
                        edge = VirtualTableFactionEdge()
                        edge.node_source = nodes_by_faction[default_edge.node_source]
                        edge.node_target = nodes_by_faction[default_edge.node_target]
                        edge.level = default_edge.level
                        edge.visible_effect = default_edge.get_effect_visibility()
                        edge.save()
                
                logger.debug('[%s] [FACTION] end of parse' % command_id)
                logger.debug('[%s] [FACTION] refresh faction to user %d' % (command_id, self.scope['user'].id))
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh_factions",
                            "origin": self.scope['user'].id,
                        }
                    }
                )
            elif command == "factions_toggle":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                if vt.game_master.id != self.scope['user'].id:
                    raise Exception("You can not toggle faction if you are not the GM of the table", 'factions_toggle_not_gm')
                
                faction_id = text_data_json['faction_id']
                
                if not isinstance(faction_id, int):
                    faction_id = 0
                
                if faction_id == 0:
                    raise Exception('Unknown faction to toggle', 'factions_toggle_unknown_faction')
                
                faction_node = VirtualTableFactionNode.objects.get(virtual_table=vt, pk=faction_id)
                
                faction_node.visible_player = not faction_node.visible_player
                faction_node.save()
                
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh_factions",
                            "origin": 0,
                        }
                    }
                )
            elif command == "faction_relations_manager":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                if vt.game_master.id != self.scope['user'].id:
                    raise Exception("You can not toggle faction if you are not the GM of the table", 'factions_toggle_not_gm')
                
                faction_id = text_data_json['faction_id']
                
                if not isinstance(faction_id, int):
                    faction_id = 0
                
                if faction_id == 0:
                    raise Exception('Unknown faction to toggle', 'factions_toggle_unknown_faction')
                
                faction_node = VirtualTableFactionNode.objects.get(virtual_table=vt, pk=faction_id)
                logger.debug('[%s] [FACREL] loading relation form for faction: %s' % (command_id,faction_node.name))
                
                template = loader.get_template('virtualtable/vt_faction_relations.html')
                
                factions = {}
                
                for faction in VirtualTableFactionNode.objects.exclude(virtual_table=vt,pk=faction_id).filter(virtual_table=vt).all():
                    logger.debug('[%s] [FACREL] preparing data for faction: %s' % (command_id,faction.name))
                    factions['f%d' % faction.id] = {
                        "id": faction.pk,
                        "name": faction.name,
                        "active": False,
                        "source_effects": [],
                        "target_effects": [],
                        "source_level": 0,
                        "target_level": 0,
                        "source_visible_player": False,
                        "target_visible_player": False,
                        "source_visible_effect": False,
                        "target_visible_effect": False,
                    }
                
                for source in faction_node.sources.all():
                    faction = factions['f%d' % source.node_target.id]
                    logger.debug('[%s] [FACREL] node %s is source for faction: %s' % (command_id, faction_node.name, source.node_target.name))
                    faction['active'] = True
                    faction['source_level'] = source.level
                    faction['source_visible_player'] = source.visible_player
                    faction['source_visible_effect'] = source.visible_effect
                    
                    if source.node_target.pc_faction:
                        logger.debug('[%s] [FACREL] the target is the players faction. Getting effects of %s' % (command_id, source.node_source.name))
                        faction['source_effects'] = source.node_source.effects.all()
                    
                    factions['f%d' % source.node_target.id] = faction
                    
                for target in faction_node.targets.all():
                    logger.debug('[%s] [FACREL] node %s is target for faction: %s' % (command_id, faction_node.name, target.node_source.name))
                    faction = factions['f%d' % target.node_source.id]
                    faction['active'] = True
                    faction['target_level'] = target.level
                    faction['target_visible_player'] = target.visible_player
                    faction['target_visible_effect'] = target.visible_effect
                    
                    if faction_node.pc_faction:
                        logger.debug('[%s] [FACREL] the source is the players faction. Getting effects of %s' % (command_id, target.node_source.name))
                        faction['target_effects'] = target.node_source.effects.all()
                    
                    factions['f%d' % target.node_source.id] = faction
                    
                
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "faction_relations",
                            "html": template.render(
                                {
                                    "node": faction_node,
                                    "factions": factions
                                }
                            ),
                            "name": faction_node.name,
                            "origin": self.scope['user'].id,
                        }
                    }
                )
            elif command == "faction_form":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                if vt.game_master.id != self.scope['user'].id:
                    raise Exception("You can not edit faction if you are not the GM of the table", 'factions_edit_not_gm')
                
                faction_id = text_data_json['faction_id']
                
                if not isinstance(faction_id, int):
                    faction_id = 0
                
                if faction_id == 0:
                    raise Exception('Unknown faction to edit', 'factions_edit_unknown_faction')
                
                faction_node = VirtualTableFactionNode.objects.get(virtual_table=vt, pk=faction_id)
                logger.debug('[%s] [FACFOR] loading relation form for faction: %s' % (command_id,faction_node.name))
                
                template = loader.get_template('virtualtable/vt_faction_form.html')
                logger.debug('[%s] [FACFOR] loaded template' % command_id)
                
                factions = Faction.objects.order_by('name').all()
                
                json_node = {
                    'name': faction_node.name,
                    'visible_player': faction_node.visible_player,
                    'faction': faction_node.faction.pk if faction_node.faction is not None else 0,
                    'discovered_secrets': faction_node.discovered_secrets,
                    'public_description': faction_node.public_description,
                    'gm_description': faction_node.gm_description,
                    'pc_faction': faction_node.pc_faction,
                    'effects': [],
                }
                
                for effect in faction_node.effects.order_by('level', 'level_name').all():
                    json_node['effects'].append({
                        'id': effect.pk,
                        'level_name': effect.level_name,
                        'level_memo': effect.level_memo,
                        'level': effect.level,
                        'effect_name': effect.effect_name,
                        'effect_description': effect.effect_description,
                        'effect': effect.effect.pk if effect.effect is not None else 0,
                    })
                
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "faction_form",
                            "html": template.render(
                                {
                                    "node": faction_node,
                                    "factions": factions
                                }
                            ),
                            "node": json_node,
                            "origin": self.scope['user'].id,
                        }
                    }
                )
            elif command == "faction_add_form":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                if vt.game_master.id != self.scope['user'].id:
                    raise Exception("You can not toggle faction if you are not the GM of the table", 'factions_toggle_not_gm')
                
                template = loader.get_template('virtualtable/vt_faction_form.html')
                logger.debug('[%s] [FACADD] loaded template' % command_id)
                
                factions = Faction.objects.order_by('name').all()
                
                json_node = {
                    'name': '',
                    'visible_player': False,
                    'faction': 0,
                    'discovered_secrets': '',
                    'public_description': '',
                    'gm_description': '',
                    'pc_faction': False,
                    'effects': [
                        {
                            'id': 0,
                            'level_name': '',
                            'level_memo': '',
                            'level': 1,
                            'effect_name': '',
                            'effect_description': '',
                            'effect': 0,
                        }
                    ],
                }
                
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "faction_form",
                            "html": template.render(
                                {
                                    "node": VirtualTableFactionNode(),
                                    "factions": factions
                                }
                            ),
                            "node": json_node,
                            "origin": self.scope['user'].id,
                        }
                    }
                )
            elif command == "faction_update_settings":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                if vt.game_master.id != self.scope['user'].id:
                    raise Exception("You can not update faction settings if you are not the GM of the table", 'factions_settings_not_gm')
                
                settings = text_data_json['settings']
                logger.debug('[%s] [STNGFAC] updating settings for virtual table %d' % (command_id, vt.id))
                
                try:
                    faction_settings = vt.faction_settings
                except ObjectDoesNotExist:
                    faction_settings = VirtualTableFactionSettings()
                    faction_settings.virtual_table = vt
                
                faction_settings.enable_vote_system = settings['vote_active']
                faction_settings.save()
                
                logger.debug('[%s] [STNGFAC] clearing blacklist' % (command_id))
                faction_settings.vote_blacklist.clear()
                
                for player_id in settings['vote_blacklist']:
                    logger.debug('[%s] [STNGFAC] trying to add player %d to blacklist' % (command_id, player_id))
                    try:
                        player = VirtualTablePlayer.objects.get(virtual_table=vt, pk=player_id)
                    except ObjectDoesNotExist:
                        logger.debug('[%s] [STNGFAC] player %d does not exists or does not belong to this table' % (command_id, player_id))
                        continue
                    
                    faction_settings.vote_blacklist.add(player)
                
                # Refresh all data
                if faction_settings.enable_vote_system:
                    try:
                        pc_faction = VirtualTableFactionNode.objects.get(pc_faction=True, virtual_table=vt)
                        logger.debug('[%s] [STNGFAC] recalculating relationships for faction %d' % (command_id, pc_faction.pk))
                        pc_faction.recalculate_relationships()
                    except ObjectDoesNotExist:
                        pass
            elif command == "faction_update_relations":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                if vt.game_master.id != self.scope['user'].id:
                    raise Exception("You can not update faction if you are not the GM of the table", 'factions_update_not_gm')
                
                faction = text_data_json['faction']
                logger.debug('[%s] [UPDFACT] updating faction id %d' % (command_id, faction['id']))
                
                faction_node = VirtualTableFactionNode.objects.get(virtual_table=vt, pk=faction['id'])
                logger.debug('[%s] [UPDFACT] got node %d: %s' % (command_id, faction_node.id, faction_node.name))
                
                faction_node.discovered_secrets = faction['discovered_secrets']
                
                faction_node.save()
                logger.debug('[%s] [UPDFACT] saved node %d' % (command_id, faction_node.id))
                
                for relation in faction['relations']:
                    logger.debug('[%s] [UPDFACT] adding/updating relation: %s' % (command_id, json.dumps(relation)))
                    
                    try:
                        if relation['type'] == 'source':
                            edge = VirtualTableFactionEdge.objects.get(node_source=faction_node, node_target=relation['target'])
                            logger.debug('[%s] [UPDFACT] got source to target edge: %d' % (command_id, edge.id))
                        elif relation['type'] == 'target':
                            edge = VirtualTableFactionEdge.objects.get(node_source=relation['source'], node_target=faction_node)
                            logger.debug('[%s] [UPDFACT] got target to source edge: %d' % (command_id, edge.id))
                        else:
                            logger.error('[%s] [UPDFACT] unknown type of edge: "%s"' % (command_id, relation['type']))
                            raise Exception('Unknown type of edge: "%s"' % relation['type'], 'unknown_edge_type')
                    except ObjectDoesNotExist as e:
                        edge = VirtualTableFactionEdge()
                        if relation['type'] == 'source':
                            edge.node_source = faction_node
                            edge.node_target = VirtualTableFactionNode.objects.get(virtual_table=vt, pk=relation['target'])
                            logger.debug('[%s] [UPDFACT] create source to target edge' % command_id)
                        elif relation['type'] == 'target':
                            edge.node_source = VirtualTableFactionNode.objects.get(virtual_table=vt, pk=relation['source'])
                            edge.node_target = faction_node
                            logger.debug('[%s] [UPDFACT] create target to source edge' % command_id)
                        else:
                            logger.error('[%s] [UPDFACT] unknown type of edge: "%s"' % (command_id, relation['type']))
                            raise Exception('Unknown type of edge: "%s"' % relation['type'], 'unknown_edge_type')
                    except Exception as e:
                        raise e
                    
                    edge.level = relation['level']
                    edge.visible_player = relation['visible']
                    edge.visible_effect = relation['effect']
                    
                    edge.save()
                    logger.debug('[%s] [UPDFACT] saved edge: %d' % (command_id, edge.id))
                
                for delete_id in faction['delete_relations']:
                    logger.debug('[%s] [UPDFACT] delete all relations with: %d' % (command_id, delete_id))
                    try:
                        edge = VirtualTableFactionEdge.objects.get(node_source=faction_node, node_target=delete_id)
                        logger.debug('[%s] [UPDFACT] got source to target edge: %d' % (command_id, edge.id))
                        edge.delete()
                        logger.debug('[%s] [UPDFACT] deleted.' % command_id)
                    except ObjectDoesNotExist as e:
                        logger.debug('[%s] [UPDFACT] no source to target relationship, nothing to do.' % command_id)
                    except Exception as e:
                        raise e
                        
                    try:
                        edge = VirtualTableFactionEdge.objects.get(node_source=delete_id, node_target=faction_node)
                        logger.debug('[%s] [UPDFACT] got target to source edge: %d' % (command_id, edge.id))
                        edge.delete()
                        logger.debug('[%s] [UPDFACT] deleted.' % command_id)
                    except ObjectDoesNotExist as e:
                        logger.debug('[%s] [UPDFACT] no target to source relationship, nothing to do.' % command_id)
                    except Exception as e:
                        raise e
                
                logger.debug('[%s] [UPDFACT] end of parse, refresh' % command_id)
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh_factions",
                            "origin": 0,
                        }
                    }
                )
            elif command == "faction_delete_node":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                if vt.game_master.id != self.scope['user'].id:
                    raise Exception("You can not delete faction if you are not the GM of the table", 'factions_update_not_gm')
                
                faction_id = text_data_json['faction_id']
                
                if not isinstance(faction_id, int):
                    faction_id = 0
                
                if faction_id == 0:
                    logger.error('[%s] [DELFAC] wrong id faction asked to delete: %s' % (command_id, faction_id))
                    raise Exception('Unknown faction to delete', 'factions_toggle_unknown_faction')
                
                try:
                    faction_node = VirtualTableFactionNode.objects.get(virtual_table=vt, pk=faction_id)
                    logger.debug('[%s] [DELFAC] got node %d: %s' % (command_id, faction_node.id, faction_node.name))
                except ObjectDoesNotExist as e:
                    logger.error('[%s] [DELFAC] asked to delete not existing faction: %d' % (command_id, faction_id))
                    raise e
                
                if faction_node.logo_file:
                    logger.debug('[%s] [DELFAC] custom logo file to delete first: %s' % (command_id, faction_node.logo_file.name))
                    faction_node.logo_file.storage.delete(faction_node.logo_file.name)
                    faction_node.logo_file.delete()
                
                faction_node.delete()
                
                logger.debug('[%s] [DELFAC] deletion completed' % command_id)
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh_factions",
                            "origin": 0,
                        }
                    }
                )
            elif command == "faction_info":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                faction_id = text_data_json['faction_id']
                game_master_badge = False
                
                force_pc_view = False
                try:
                    force_pc_view = text_data_json['force_pc_view']
                except KeyError:
                    pass
                
                game_master = vt.game_master.id == self.scope['user'].id and not force_pc_view
                
                if not isinstance(faction_id, int):
                    faction_id = 0
                
                if faction_id == 0:
                    raise Exception('Unknown faction to toggle', 'factions_toggle_unknown_faction')
                
                faction_node = VirtualTableFactionNode.objects.get(virtual_table=vt, pk=faction_id)
                
                effects = []
                votes = []
                relation_title = ""
                
                template_affinity = loader.get_template('virtualtable/vt_faction_affinity.html')
                affinity_html = ''
                template_vote_overview = loader.get_template('virtualtable/vt_faction_vote_overview.html')
                vote_overview_html = ''
                
                try:
                    faction_settings = vt.faction_settings
                    allowed_players = vt.players.exclude(pk__in=faction_settings.vote_blacklist.all()).all()
                    active_vote = faction_settings.enable_vote_system
                except ObjectDoesNotExist:
                    allowed_players = vt.players.all()
                    active_vote = True
                
                logger.debug('[%s] [INFACT] faction node: %s (id: %d)' % (command_id, faction_node.name, faction_node.id))
                
                if not faction_node.pc_faction:
                    try:
                        edge = faction_node.sources.get(node_target__faction__pk=1)
                        logger.debug('[%s] [INFACT] edge found %d' % (command_id, edge.id))
                        if game_master or edge.visible_effect:
                            if not edge.visible_effect:
                                game_master_badge = True
                            
                            logger.debug('[%s] [INFACT] effect visible' % command_id)
                            template = loader.get_template('virtualtable/vt_faction_effect.html')
                                        
                            if edge.level > 0:
                                logger.debug('[%s] [INFACT] positive edge level: %d' % (command_id, edge.level))
                                # For positive levels, we concatene effects. Maybe improve this later.
                                for effect in faction_node.effects.filter(level__gte=0, level__lte=edge.level).order_by('level').all():
                                    logger.debug('[%s] [INFACT] relation title: %s' % (command_id, effect.level_name))
                                    relation_title = effect.level_name
                                    logger.debug('[%s] [INFACT] effect used: %d' % (command_id, effect.id))
                                    
                                    if game_master or effect.effect_name != '' or effect.effect_description != '':
                                        logger.debug('[%s] [INFACT] either gm or something to display, adding effect' % command_id)
                                        # effects.append()
                                        
                                        effects.append(
                                            template.render(
                                                {
                                                    "effect": {
                                                        "title": effect.effect_name,
                                                        "desc": effect.effect_description,
                                                        "gm": game_master,
                                                        "memo": (effect.level_memo if game_master else False),
                                                    },
                                                }
                                            )
                                        )
                                        
                                    
                            elif edge.level < 0:
                                logger.debug('[%s] [INFACT] negative edge level: %d' % (command_id, edge.level))
                                for effect in faction_node.effects.filter(level__lte=0, level__gte=edge.level).order_by('level').all():
                                    logger.debug('[%s] [INFACT] relation title: %s' % (command_id, effect.level_name))
                                    relation_title = effect.level_name
                                    logger.debug('[%s] [INFACT] effect used: %d' % (command_id, effect.id))
                                    
                                    if game_master or effect.effect_name != '' or effect.effect_description != '':
                                        logger.debug('[%s] [INFACT] either gm or something to display, adding effect' % command_id)
                                        effects.append(
                                            template.render(
                                                {
                                                    "effect": {
                                                        "title": effect.effect_name,
                                                        "desc": effect.effect_description,
                                                        "gm": game_master,
                                                        "memo": (effect.level_memo if game_master else False),
                                                    },
                                                }
                                            )
                                        )
                                        # For negative levels, we don't concatene them, we get only the worst (displayable). Maybe improve this later.
                                        break
                        
                    except ObjectDoesNotExist as e:
                        logger.debug('[%s] [INFACT] edge not found' % command_id)
                    except Exception as e:
                        raise e
                    
                    if active_vote:
                        if game_master:
                            vote_avg = 0
                            try:
                                vote_avg = faction_node.targets.get(node_source__pc_faction=True).level
                            except ObjectDoesNotExist:
                                pass
                            vote_overview_html = template_vote_overview.render(
                                {
                                    "votes": (VirtualTableFactionNodeVote
                                        .objects
                                        .filter(player__in=allowed_players, node=faction_node)
                                        .select_related('player')
                                        .all()),
                                    "vote_avg": vote_avg,
                                }
                            )
                            logger.debug('[%s] [INFACT] vote overview html %s ' % (command_id, vote_overview_html))
                        else:
                            for vote in VirtualTableFactionNodeVote.objects.filter(player__player=self.scope['user'], player__in=allowed_players, node=faction_node).all():
                                votes.append({
                                    "player": vote.player.id,
                                    "affinity": vote.affinity,
                                })
                            
                            affinity_html = template_affinity.render(
                                {
                                    "players": VirtualTablePlayer.objects.filter(virtual_table=vt, pk__in=allowed_players, player=self.scope['user'])
                                }
                            )
                
                faction = {
                    "title": relation_title,
                    "desc": faction_node.public_description,
                    "secret": faction_node.discovered_secrets,
                    "effects": effects,
                    "votes": votes,
                    "vote_overview_div": vote_overview_html,
                    "affinity_div": affinity_html,
                    "gm_badge": game_master_badge,
                    "gm": game_master,
                }
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "faction_info",
                            "faction": faction,
                            "origin": self.scope['user'].id,
                        }
                    }
                )
            elif command == "faction_affinity":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                faction_id = text_data_json['faction_id']
                
                if not isinstance(faction_id, int):
                    faction_id = 0
                
                if faction_id == 0:
                    raise Exception('Unknown faction to change affinity on', 'faction_affinity_unknown_faction')
                
                player_id = text_data_json['player']
                
                if not isinstance(player_id, int):
                    player_id = 0
                
                if player_id == 0:
                    raise Exception('Unknown player to change affinity on', 'faction_affinity_unknown_player')
                
                affinity = text_data_json['affinity']
                if not isinstance(affinity, int):
                    affinity = 0
                
                try:
                    faction = VirtualTableFactionNode.objects.get(virtual_table=vt, pk=faction_id)
                except ObjectDoesNotExist:
                    raise Exception('The faction you try to update does not belong to this virtual table', 'faction_affinity_wrong_faction')
                
                try:
                    player = VirtualTablePlayer.objects.get(virtual_table=vt, pk=player_id, player=self.scope['user'])
                except ObjectDoesNotExist:
                    raise Exception('The player you try to update does not belong to you or to this virtual table', 'faction_affinity_wrong_player')
                
                try:
                    faction_settings = vt.faction_settings
                    active_vote = faction_settings.enable_vote_system
                    player_blacklisted = faction_settings.vote_blacklist.filter(pk=player.pk).count() > 0
                except ObjectDoesNotExist:
                    active_vote = True
                    player_blacklisted = False
                
                if not active_vote:
                    raise Exception('Trying to vote while the faction voting system is disabled', 'faction_affinity_vote_disabled')
                
                if player_blacklisted:
                    raise Exception('Trying to vote but this player is blacklisted', 'faction_affinity_player_blacklisted')
                    
                try:
                    player_vote = VirtualTableFactionNodeVote.objects.get(player=player, node=faction)
                except ObjectDoesNotExist:
                    player_vote = VirtualTableFactionNodeVote()
                    player_vote.player = player
                    player_vote.node = faction
                
                player_vote.affinity = affinity
                player_vote.save()
                
                # Calculate the new average
                final_affinity = 0
                count_allowed_players = VirtualTablePlayer.objects.filter(virtual_table=vt).count()
                
                if count_allowed_players == 0:
                    # We take only the currently-on-the-virtual-table, not-blacklisted players
                    for vote in VirtualTableFactionNodeVote.objects.filter(node=faction, player__virtual_table=vt).all():
                        final_affinity = final_affinity + vote.affinity
                        logger.debug('[%s] [VOTFAC] affinity %d, global_affinity %d' % (command_id, vote.affinity, final_affinity))
                        
                    affinity_avg = round(final_affinity / count_allowed_players)
                    logger.debug('[%s] [VOTFAC] affinity_avg %d' % (command_id, affinity_avg))
                    try:
                        pc_faction_edge = faction.targets.get(node_source__pc_faction=True)
                        logger.debug('[%s] [VOTFAC] updating edge id %d' % (command_id, pc_faction_edge.id))
                    except ObjectDoesNotExist:
                        pc_faction_edge = VirtualTableFactionEdge()
                        pc_faction_edge.node_target = faction
                        pc_faction_edge.node_source = VirtualTableFactionNode.objects.get(virtual_table=vt, pc_faction=True)
                        logger.debug('[%s] [VOTFAC] new edge' % command_id)
                else:
                    affinity_avg = 0
                pc_faction_edge.level = affinity_avg
                pc_faction_edge.save()
                logger.debug('[%s] [VOTFAC] edge saved: %d' % (command_id, pc_faction_edge.id))
                
            else:
                raise Exception('Not known command.', 'unknown_command')
            
            
            
        except Exception as e:
            # import pdb; pdb.set_trace()
            logger.exception("[%s] [GLOBAL] a global exception occured while executing the command (JSON: %s) " % (command_id, text_data))
            try:
                self.send(text_data=json.dumps({
                    'error': {
                        "message": e.args[0],
                        "code": e.args[1],
                    }
                }))
            except Exception as e:
                self.send(text_data=json.dumps({
                    'error': {
                        "message": str(e),
                        "code": "",
                    }
                }))
        
    # Receive message from room group
    def vt_message(self, event):
        params = event['params']
        # command_id = uuid.uuid4()
        
        if 'self_target' in params and params['self_target'] and params['origin'] != self.scope['user'].id:
            return
        
        if params['command'] == 'status':
            if params['origin'] != self.scope['user'].id:
                # we want to refresh only for the user who asked the refresh.
                return
        elif params['command'] == 'del':
            params['new_url'] = ''
            vt = VirtualTable.objects.get(pk=self.vt_id)
            if (len(VirtualTablePlayer.objects.filter(player=self.scope['user'],virtual_table=vt)) == 0 and
               vt.game_master != self.scope['user']):
                params['new_url'] = reverse('virtualtable:vt_list')
            else:
                safe_topics, unsafe_topics, unknown_topics = vt.get_separated_topics()
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": 'consent',
                            "safe_topics": safe_topics,
                            "unsafe_topics": unsafe_topics,
                            "unknown_topics": unknown_topics,
                        }
                    }
                )
        elif params['command'] == 'kick':
            params['new_url'] = reverse('virtualtable:vt_list')
        elif params['command'] == 'new':
            vtp = VirtualTablePlayer.objects.annotate(
                editable_by_user=Case(
                    When(edit_level=VirtualTablePlayer.EDIT_LVL_ALL, then=True),
                    When(edit_level=VirtualTablePlayer.EDIT_LVL_PLAYER, player=self.scope['user'], then=True),
                    When(virtual_table__game_master=self.scope['user'], then=True),
                    default=False,
                 
                    output_field=BooleanField()
                )
            ).get(pk=params['vtp_id'])
            
            template = loader.get_template('virtualtable/vt_detail_character.html')
            html = template.render({
                "character": vtp,
                "user": self.scope['user'],
            })
            params['html'] = html
            params['vtp'] = model_to_dict(vtp)
            params["editable_by_user"] = vtp.editable_by_user
            
            vt = VirtualTable.objects.get(pk=self.vt_id)
            safe_topics, unsafe_topics, unknown_topics = vt.get_separated_topics()
            async_to_sync(self.channel_layer.group_send)(
                self.vt_group_name,
                {
                    'type': 'vt_message',
                    'params': {
                        "command": 'consent',
                        "safe_topics": safe_topics,
                        "unsafe_topics": unsafe_topics,
                        "unknown_topics": unknown_topics,
                    }
                }
            )
        elif params['command'] == 'refresh':
            vtp = VirtualTablePlayer.objects.annotate(
                editable_by_user=Case(
                    When(edit_level=VirtualTablePlayer.EDIT_LVL_ALL, then=True),
                    When(edit_level=VirtualTablePlayer.EDIT_LVL_PLAYER, player=self.scope['user'], then=True),
                    When(virtual_table__game_master=self.scope['user'], then=True),
                    default=False,
                    
                    output_field=BooleanField()
                )
            ).get(pk=params['vtp_to_load'])
            params['vtp'] = model_to_dict(vtp)
            params["editable_by_user"] = vtp.editable_by_user
        elif params['command'] == 'refresh_avalon':
            template = loader.get_template('virtualtable/vt_detail_avalon.html')
            vt = VirtualTable.objects.get(pk=self.vt_id)
            vt.avalon.ordered_extensions_by_user = vt.avalon.get_extensions_by_user_vote(self.scope['user'])
            html = template.render({
                "user": self.scope['user'],
                "virtualtable": vt,
            })
            params['html'] = html
        elif params['command'] == 'refresh_factions':
            if params['origin'] != 0 and params['origin'] != self.scope['user'].id:
                # we want to refresh only for the user who asked the refresh.
                return
            force_pc_view = False
            try:
                force_pc_view = params['force_pc_view']
            except KeyError:
                pass
            
            params['elements'] = self.vt_faction_refresh(force_pc_view)
        elif params['command'] == 'faction_info':
            if params['origin'] != self.scope['user'].id:
                # we want to refresh only for the user who asked the refresh.
                return
        elif params['command'] == 'combo_result' or params['command'] == 'damage_result':
            vt = VirtualTable.objects.get(pk=self.vt_id)
            
            if vt is None:
                return
            
            if not params['public'] and vt.game_master.id != self.scope['user'].id and params['vtp']['player'] != self.scope['user'].id:
                return
        elif params['command'] == 'destiny_result':
            vt = VirtualTable.objects.get(pk=self.vt_id)
            
            if vt is None:
                return
        elif params['command'] == 'combo_gm_result' or params['command'] == 'damage_gm_result':
            vt = VirtualTable.objects.get(pk=self.vt_id)
            
            if vt is None:
                return
            
            if not params['public'] and (vt.game_master.id != self.scope['user'].id):
                return
        self.send(text_data=json.dumps(params))

    # Receive message from room group
    def vt_status(self, event):
        params = event['params']
        
        async_to_sync(self.channel_layer.group_send)(
            self.vt_group_name,
            {
                'type': 'vt_message',
                'params': {
                    'command': 'status',
                    'origin': params['user'],
                    'counter': params['counter'],
                    'vtp': list(VirtualTablePlayer.objects.filter(player=self.scope['user'].id).values_list('id', flat=True)),
                }
            }
        )
        
        self.send(text_data=json.dumps(params))
    
    def vt_faction_refresh(self, force_pc_view=False):
        """
        Get the factions refreshed from database to display. 
        """
        
        elements = []
        parsed_relation = {}
        already_done_edges = {}
        vt = VirtualTable.objects.get(pk=self.vt_id)
        
        for faction_node in VirtualTableFactionNode.objects.filter(virtual_table=self.vt_id).prefetch_related('sources', 'targets').all():
            if ((vt.game_master.id != self.scope['user'].id or force_pc_view)
                and not faction_node.visible_player):
                
                continue
            
            if faction_node.logo_file == None or faction_node.logo_file == '':
                if faction_node.faction == None:
                    full_filename = static('gm/img/faction_logo/noun-agent-1781609.png')
                else:
                    full_filename = static(''.join(['gm/img/faction_logo/', faction_node.faction.logo_file_name]))
            else:
                # Do something intelligent with media and image.
                full_filename = faction_node.logo_file.url
            
            classes = ''
            
            if vt.game_master.id == self.scope['user'].id and not force_pc_view:
                if faction_node.visible_player:
                    classes = 'visible'
                else:
                    classes = 'not-visible'
            
            elements.append({
                "group": "nodes",
                "classes": classes,
                "data": {
                    "id": faction_node.pk,
                    "name": faction_node.name,
                    "background": full_filename,
                    "pc_faction": faction_node.pc_faction,
                },
            })
            
            for faction_edge_source in faction_node.sources.all():
                if ((vt.game_master.id != self.scope['user'].id or force_pc_view)
                    and not (faction_edge_source.node_source.visible_player
                        and faction_edge_source.node_target.visible_player
                        and faction_edge_source.visible_player)):
                    continue
                
                if faction_edge_source.node_source.pk not in parsed_relation:
                    parsed_relation[faction_edge_source.node_source.pk] = {}
                
                parsed_relation[faction_edge_source.node_source.pk][faction_edge_source.node_target.pk] = [faction_edge_source]
            
            for faction_edge_target in faction_node.targets.all():
                if ((vt.game_master.id != self.scope['user'].id or force_pc_view)
                    and not (faction_edge_target.node_source.visible_player
                        and faction_edge_target.node_target.visible_player
                        and faction_edge_target.visible_player)):
                    continue
                
                if faction_edge_target.node_target.pk not in parsed_relation:
                    parsed_relation[faction_edge_target.node_target.pk] = {}
                
                if faction_edge_target.node_source.pk not in parsed_relation[faction_edge_target.node_target.pk]:
                    parsed_relation[faction_edge_target.node_target.pk][faction_edge_target.node_source.pk] = [None, faction_edge_target]
                else:
                    parsed_relation[faction_edge_target.node_target.pk][faction_edge_target.node_source.pk].append(faction_edge_target)
            
            for source_index in parsed_relation:
                for target_index in parsed_relation[source_index]:
                    # logger.debug('[%s] [FACTION] checking relation between %s and %s' % (command_id, source_index, target_index))
                    if '-'.join([str(source_index), str(target_index)]) in already_done_edges:
                        # logger.debug('[%s] [FACTION] already parsed' % command_id)
                        continue
                    
                    if '-'.join([str(target_index), str(source_index)]) in already_done_edges:
                        # logger.debug('[%s] [FACTION] already parsed' % command_id)
                        continue
                    
                    relationship_array = parsed_relation[source_index][target_index]
                    if len(relationship_array) == 1:
                        classes = "from%s-to0" % (relationship_array[0].level)
                    elif relationship_array[0] is None:
                        classes = "from0-to%s" % (relationship_array[1].level)
                    else:
                        classes = "from%s-to%s" % (relationship_array[0].level, relationship_array[1].level)
                    
                    elements.append({
                        "group": "edges",
                        "classes": classes,
                        "data": {
                            "source": source_index,
                            "target": target_index,
                            # "source_label": 'Neutre',
                            # "target_label": 'Neutre',
                        },
                    })
                    already_done_edges['-'.join([str(source_index), str(target_index)])] = True
        
        return elements
