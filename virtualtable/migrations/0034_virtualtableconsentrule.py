# Generated by Django 2.2.2 on 2022-04-13 12:52

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('virtualtable', '0033_virtualtableconsentanswer_virtualtableconsenttopic'),
    ]

    operations = [
        migrations.CreateModel(
            name='VirtualTableConsentRule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ignore_when_left', models.BooleanField(blank=True, default=False, help_text="En cochant cette case, vos réponses à ce questionnaire ne seront plus prises en compte une fois que vous ne serez plus sur la table. En pratique, cela signifie qu'il y a un risque pour que vos réponses puissent être devinées par les autres participants. Ne cochez cette case que si la table est constituée de personnes en qui vous avez confiance ou que vous pouvez aborder librement les sujets qui vous dérangent.", verbose_name='Ignorer mes réponses si aucun personnage')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='consent_rules', to=settings.AUTH_USER_MODEL)),
                ('virtual_table', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='consent_rules', to='virtualtable.VirtualTable')),
            ],
            options={
                'unique_together': {('virtual_table', 'user')},
            },
        ),
    ]
