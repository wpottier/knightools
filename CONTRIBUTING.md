# Contribuer au projet

Vous souhaitez contribuer au projet ? Voici un petit guide pour vous
permettre d'intégrer correctement vos modifications dans le workflow
général du projet.

Ce guide s'adresse à des personnes n'étant pas forcément familières avec
Git, pour aider à une utilisation classique de ce dernier. Certaines
commandes ont beaucoup d'options, il existe beaucoup de tutoriels 
interactifs pour apprendre les commandes git. Si malgré tout, certaines
commandes vous posent souci, n'hésitez pas à venir poser des questions
sur http://discord.renkineko.fr

Ce guide utilise les lignes de commandes git, permettant d'effectuer de
simples copier/coller en cas de besoin. Si vous utilisez Windows, vous
avez l'outil "Git bash" qui permet de simuler un terminal linux et donc
d'y mettre vos commandes.

## Divergence/Fork

Le projet fonctionne avec un système de merge request et de fork. Vous
devez donc dans un premier temps faire une divergence (fork). Cela
consiste simplement à faire une copie du projet à votre nom.
Pour cela, cliquez sur le badge "Créer une divergence" (fork) en haut de
l'interface gitlab, sur la page de votre dépôt.

Le projet sera copié avec, au lieu de Renkineko dans l'URL, votre nom
d'utilisateur.

## Cloner le projet

Si vous n'avez pas configuré de clé SSH, vous trouverez la procédure
sur la page correspondante dans Gitlab : Cliquez sur votre avatar en
haut à droite du site, puis dans Paramètres (ou Settings), dans
l'onglet sur la gauche choisissez Clefs SSH (ou SSH keys). Vous avez
un lien vers les guides en haut de la page.

Depuis le dépôt de votre divergence, copiez l'URL de clonage
(bouton Clone). Prenez celle qui utilise SSH et non pas celle qui
utilise HTTPS.

Ouvrez un terminal ou un git bash à l'emplacement désiré où vous voulez
mettre le projet, puis taper la commande suivante :

```
git clone url_de_votre_divergence
```

Un nouveau dossier va se créer, contenant le dépôt. Allez dans ce
dossier, puis tapez la commande :

```
git remote add upstream git@gitlab.com:Renkineko/knightools.git
```

Grâce à cette dernière commande, vous êtes branché à la fois sur votre
divergence ET sur le dépôt officiel.

## Configurer votre Git

Il y a trois choses à paramétrer pour pouvoir utiliser correctement Git :

* Votre nom d'utilisateur : `git config --global user.name votre_nom`
* Votre mail : `git config --global user.name votre_mail`
* La méthode de pull du projet : `git config pull.rebase true`

La dernière commande permet d'éviter des enchevêtrements de branches
dans tous les sens juste parce que vous avez tiré (pull) et que des
nouveaux commits étaient présents.

## Modifier les fichiers

Pour modifier les fichiers et les pousser (push) sur votre dépôt, vous
allez devoir maîtriser diverses commandes.

### Architecture et guidelines

Le projet est composé de 2 branches principales : dev et master.

* La branche `dev` sert pour tout ce qui est développement en cours. La
branche peut être instable, et il est déconseillé de l'utiliser pour
de l'exploitation. À terme, la branche dev devrait être testable sur
https://beta.knightools.fr, lorsque ce sera mis en place. Tant que la
sortie officielle n'est pas lancée, elle est présente directement sur
https://knightools.fr
* La branche `master` est la branche de sortie officielle : ce qui est
dans cette branche doit être stable car mis en exploitation. Aucun
commit direct n'est autorisé dans master, seul des merges requests
peuvent se faire dessus.

A ces deux branches principales s'ajoutent une multitude de petites
branches que vous êtes libres de créer ou non. Ces branches vont
permettre d'isoler les différents développements et seront ensuite
centralisés sur la branche `dev`.

Les commits doivent respecter les normes git :

* 50 caractères pour la première ligne (considérez cela comme un sujet
de mail), qui doivent donner les mots clés du sujet du commit.
* Si juste 50 caractères ne suffisent pas, sauter une ligne puis écrire
le détail du commit. Chaque ligne du détail ne doit pas dépasser 72
caractères.
* Si possible, écrire le commit en anglais (optionnel).

### Commandes git

Pour vous déplacer de branche en branche :

```
git checkout nom_de_la_branche
```

Lorsque vous créez une branche, vous partez de la branche d'où vous êtes,
vérifiez donc bien à chaque fois que vous êtes dans la branche dev avant
de créer votre branche.

Pour créer une branche :

```
git checkout -b nom_nouvelle_branche
```

Pour voir les branches existantes : 

```
git branch -a
```

Pour récupérer les commits du serveur distant :

```
git pull
```

Et pour récupérer ceux du dépôt officiel, il faut ajouter le code que
vous lui avez donné, ainsi que la branche :

```
git pull upstream dev
```

Attention : vous ne pouvez pas pull si votre zone de travail n'est pas
propre (comprendre : si vous avez des fichiers en cours de modification).

Une fois que vous avez modifié les fichiers, vous pouvez vérifier ce
qui a été modifié avec la commande :

```
git status
```

Si vous souhaitez voir ce que vous avez modifié exactement dans un
fichier :

```
git diff chemin_vers_le_fichier_telle_que_presente_dans_git_status
```

Une fois que vous êtes satisfait des modifications apportées, il faut
préparer l'espace de travail pour qu'il sache ce qu'il y aura dans votre
prochain commit. Pour cela :

```
git add chemin_vers_fichier1 chemin_vers_fichier2 ...
```

Si vous refaites `git status` vous verrez certains fichiers marqués
comme étant dans les modifications qui seront validées. Si vous avez
ajouté un fichier en trop :

```
git reset chemin_vers_fichier1 ...
```

Cela ne change pas les modifications que vous avez fait à l'intérieur,
mais il ne sera juste pas commit.

Enfin, une fois que vous vous sentez d'attaque :

```
git commit
```

Cela va ouvrir un éditeur par défaut (ou vous demander lequel ouvrir).
Les messages de commit du projets respectent la norme git classique,
se référer à la section correspondante un peu plus haut.

Une fois que vos commits sont terminés, vous pouvez pousser la branche
sur votre dépôt distant :

```
git push origin le_nom_de_votre_branche
```

Une fois cela fait, vous pouvez ouvrir un merge request de la branche
sur le dépôt upstream, en passant par l'interface gitlab : sélectionnez
votre branche nouvelle poussée, et vous aurez un bouton "Create merge
request" (ou son équivalent français). Suivez le guide, vous devez
demander la merge request sur la branche `dev` du dépôt officiel.

Lorsque votre merge request est acceptée, elle est présente dans la
branche `dev` du dépôt officiel (dont le nom est "upstream" ici).

Revenez sur votre branche originelle, tirez les commits du dépôt
upstream puis vous pouvez enfin les pousser chez vous :

```
git checkout dev
git pull upstream dev
git push origin dev
```

Si vous demandez à voir les commits précédents, vous devriez voir vos
commits :

```
git log
```

(tapez "q" pour sortir si vous restez dans cette liste)

Et voilà pour un cycle normal de contribution.

Si vous avez des questions, venez les poser sur le discord dont le lien
est donné en haut de ce guide.